import { FoundryShield } from '../../src/module/character/shield'

describe('FoundryShield', function () {
  const name = 'Wooden Shield'
  const attackMod = -2
  const parryMod = 3
  const initiativeMod = 2
  const itemData = {
    data: {
      name,
      data: {
        initiativeMod,
        weaponMod: {
          attack: attackMod,
          parry: parryMod,
        },
      },
    },
  } as unknown as Item

  it('should provide the name of the underlying shield', function () {
    const weapon = new FoundryShield(itemData)
    expect(weapon.name).toEqual(name)
  })

  it('should provide the initiative mod of the underlying shield data', function () {
    const weapon = new FoundryShield(itemData)
    expect(weapon.initiativeMod).toEqual(initiativeMod)
  })

  it('should provide the attack mod of the underlying shield data', function () {
    const weapon = new FoundryShield(itemData)
    expect(weapon.weaponMod.attack).toEqual(attackMod)
  })

  it('should provide the parry mod of the underlying shield data', function () {
    const weapon = new FoundryShield(itemData)
    expect(weapon.weaponMod.parry).toEqual(parryMod)
  })
})
