import each from 'jest-each'
import { when } from 'jest-when'
import { DsaFourOneActor } from '../../src/module/actor/actor'
import { FoundryCharacter } from '../../src/module/character/character'
import type {
  Armor,
  MeleeWeapon,
  Shield,
  Weapon,
} from '../../src/module/model/items'
import type {
  ManeuverType,
  ModifierDescriptor,
} from '../../src/module/model/modifier'
import type {
  Ability,
  BaseTalent,
  CombatTalent,
} from '../../src/module/model/properties'
import {
  AttackAction,
  ComputeInitiative,
  ComputeInitiativeFormula,
  DodgeAction,
  ParryAction,
} from '../../src/module/ruleset/rules/basic-combat'
import { RangedAttackAction } from '../../src/module/ruleset/rules/basic-ranged-combat'
import {
  ComputeArmorClass,
  ComputeAttack,
  ComputeDamageFormula,
  ComputeEffectiveEncumbarance,
  ComputeEncumbarance,
  ComputeParry,
  ComputeRangedAttack,
  RangeClass,
  SizeClass,
} from '../../src/module/ruleset/rules/derived-combat-attributes'
import { ComputeManeuverList } from '../../src/module/ruleset/rules/maneuvers/basic-maneuver'
import { createMockRuleset } from '../ruleset/rules/helpers'

describe('FoundryCharacter', function () {
  const name = 'Testname'
  const baseAttack = 12
  const baseParry = 12
  const baseRangedAttack = 12
  const baseInitiative = 9
  const dodge = 12
  const strength = 14
  const items = new Map()
  const talentSID = 'talent-swords'
  const talentName = 'Swords'
  const spellSID = 'spell-ignifaxius'
  const spellName = 'Ignifaxius'
  items.set('abc', {
    data: {
      name: talentName,
      type: 'combatTalent',
      data: {
        sid: talentSID,
      },
    },
  })

  items.set('123', {
    data: {
      name: spellName,
      type: 'spell',
      data: {
        sid: spellSID,
      },
    },
  })
  const abilityName = 'Wuchtschlag'
  const abilitySID = 'ability-wuchtschlag'
  items.set('xyz', {
    data: {
      name: abilityName,
      type: 'specialAbility',
      data: {
        sid: abilitySID,
      },
    },
  })

  const armorName = 'Kettenhemd'
  const armorClass = 3
  const armorEncumbarance = 2
  items.set('armor-kettenhemd', {
    data: {
      name: armorName,
      type: 'armor',
      data: {
        armorClass,
        encumbarance: armorEncumbarance,
        equiped: true,
      },
    },
  })

  items.set('another-armor-kettenhemd', {
    data: {
      name: 'another' + armorName,
      type: 'armor',
      data: {
        armorClass: armorClass + 3,
        encumbarance: armorEncumbarance + 2,
        equiped: false,
      },
    },
  })

  const combatStateData = {
    isArmed: true,
    primaryHand: {} as unknown as Weapon,
    secondaryHand: {} as unknown as Weapon | Shield,
    unarmedTalent: {} as unknown as CombatTalent,
  }

  const actor = {
    combatState: combatStateData,

    data: {
      name,
      type: 'character',
      data: {
        base: {
          basicAttributes: {
            strength: {
              value: strength,
            },
          },
          combatAttributes: {
            active: {
              baseAttack: {
                value: baseAttack,
              },
              baseParry: {
                value: baseParry,
              },
              baseRangedAttack: {
                value: baseRangedAttack,
              },
              baseInitiative: {
                value: baseInitiative,
              },
              dodge: {
                value: dodge,
              },
            },
          },
        },
      },
    },
    items,
  } as unknown as DsaFourOneActor

  const ruleset = createMockRuleset()

  it('should provide the name of the given actor', function () {
    const character = new FoundryCharacter(actor, ruleset)
    expect(character.name).toEqual(name)
  })

  it('should provide the base attack of the given actor', function () {
    const character = new FoundryCharacter(actor, ruleset)
    expect(character.baseAttack).toEqual(baseAttack)
  })

  it('should provide the base parry of the given actor', function () {
    const character = new FoundryCharacter(actor, ruleset)
    expect(character.baseParry).toEqual(baseParry)
  })

  it('should provide the base ranged attack of the given actor', function () {
    const character = new FoundryCharacter(actor, ruleset)
    expect(character.baseRangedAttack).toEqual(baseRangedAttack)
  })

  it('should provide the base initiative of the given actor', function () {
    const character = new FoundryCharacter(actor, ruleset)
    expect(character.baseInitiative).toEqual(baseInitiative)
  })

  it('should provide the strength attribute of the given actor', function () {
    const character = new FoundryCharacter(actor, ruleset)
    expect(character.attribute('strength').value).toEqual(strength)
  })

  it('should provide the characters attack value for a weapon', function () {
    const character = new FoundryCharacter(actor, ruleset)
    const weapon = {} as Weapon

    const value = 12
    when(ruleset.compute)
      .calledWith(ComputeAttack, expect.objectContaining({ weapon, character }))
      .mockReturnValue({ value })

    const result = character.attackValue({ weapon })
    expect(result).toEqual(value)
  })

  it('should provide the characters parry value for a weapon', function () {
    const character = new FoundryCharacter(actor, ruleset)
    const weapon = {} as Weapon

    const value = 13
    when(ruleset.compute)
      .calledWith(ComputeParry, expect.objectContaining({ weapon, character }))
      .mockReturnValue({ value })

    const result = character.parryValue({ weapon })
    expect(result).toEqual(value)
  })

  it('should provide the characters ranged attack value for a weapon', function () {
    const character = new FoundryCharacter(actor, ruleset)
    const weapon = {} as Weapon

    const value = 19

    when(ruleset.compute)
      .calledWith(
        ComputeRangedAttack,
        expect.objectContaining({ weapon, character })
      )
      .mockReturnValue({ value })

    const result = character.rangedAttackValue({ weapon })
    expect(result).toEqual(value)
  })

  it('should provide the characters damage formula for a weapon', function () {
    const character = new FoundryCharacter(actor, ruleset)
    const weapon = {} as Weapon
    const formula = '1d6+3'

    when(ruleset.compute)
      .calledWith(
        ComputeDamageFormula,
        expect.objectContaining({ weapon, character })
      )
      .mockReturnValue({
        baseDamage: null,
        bonusDamage: 0,
        multiplier: 0,
        formula,
      })

    const result = character.damage(weapon)
    expect(result).toEqual(formula)
  })

  it('should provide the characters dodge value', function () {
    const character = new FoundryCharacter(actor, ruleset)

    const result = character.dodgeValue()
    expect(result).toEqual(dodge)
  })

  it('should provide the characters equiped armor items as a list', function () {
    const character = new FoundryCharacter(actor, ruleset)
    const expected: Armor[] = [
      {
        name: armorName,
        armorClass: armorClass,
        encumbarance: armorEncumbarance,
      },
    ]

    const result = character.armorItems
    expect(result).toEqual(expected)
  })

  it('should provide the characters encumbarance value', function () {
    const character = new FoundryCharacter(actor, ruleset)

    const value = 3
    when(ruleset.compute)
      .calledWith(ComputeEncumbarance, expect.objectContaining({ character }))
      .mockReturnValue(value)

    const result = character.encumbarance
    expect(result).toEqual(value)
  })

  each([
    ['BE', 2],
    ['BE–1', 1],
    ['BE–3', 0],
    ['BE+1', 3],
    ['BEx2', 4],
  ]).it(
    'should be able to compute the effective encumbarance based on a formula',
    function (formula: string, expectedEncumbarance: number) {
      const character = new FoundryCharacter(actor, ruleset)

      when(ruleset.compute)
        .calledWith(
          ComputeEffectiveEncumbarance,
          expect.objectContaining({ character, formula })
        )
        .mockReturnValue(expectedEncumbarance)

      const result = character.effectiveEncumbarance(formula)

      expect(result).toEqual(expectedEncumbarance)
    }
  )

  it('should provide the characters armor class value', function () {
    const character = new FoundryCharacter(actor, ruleset)

    const value = 3
    when(ruleset.compute)
      .calledWith(ComputeArmorClass, expect.objectContaining({ character }))
      .mockReturnValue(value)

    const result = character.armorClass
    expect(result).toEqual(value)
  })

  it('should provide a talent by its sid', function () {
    const character = new FoundryCharacter(actor, ruleset)

    expect(character.talent(talentSID)?.name).toEqual(talentName)
  })

  it('should provide a spell by its sid', function () {
    const character = new FoundryCharacter(actor, ruleset)

    expect(character.spell(spellSID)?.name).toEqual(spellName)
  })

  it('should provide a ability by its sid', function () {
    const character = new FoundryCharacter(actor, ruleset)

    expect(character.ability(abilitySID)?.name).toEqual(abilityName)
    expect(character.ability(abilitySID)?.identifier).toEqual(abilitySID)
  })

  it('should be able to check if a ability is present', function () {
    const character = new FoundryCharacter(actor, ruleset)
    const ability: Ability = {
      name: 'Wuchtschlag',
      identifier: 'ability-wuchtschlag',
    }

    expect(character.has(ability)).toEqual(true)
  })

  it('should be able to check if a talent is present', function () {
    const character = new FoundryCharacter(actor, ruleset)
    const talent: BaseTalent = {
      name: 'Schwerter',
      identifier: 'talent-swords',
      value: 0,
    }

    expect(character.has(talent)).toEqual(true)
  })

  const baseCombatActions = ['attack', 'parry', 'rangedAttack']
  const actionIdentifiers = {
    attack: AttackAction,
    parry: ParryAction,
    rangedAttack: RangedAttackAction,
  }

  each(baseCombatActions).it(
    'should execute a base combat action',
    function (actionName: string) {
      const character = new FoundryCharacter(actor, ruleset)
      const weapon = {} as Weapon
      const actionIdentifier = actionIdentifiers[actionName]

      character[actionName]({ weapon })
      expect(ruleset.execute).toHaveBeenCalledWith(
        actionIdentifier,
        expect.objectContaining({ weapon, character })
      )
    }
  )

  each(baseCombatActions).it(
    'should execute a base combat action with modifiers',
    function (actionName: string) {
      const character = new FoundryCharacter(actor, ruleset)
      const weapon = {} as Weapon
      const actionIdentifier = actionIdentifiers[actionName]

      const modifiers: ModifierDescriptor[] = [
        {
          name: 'test',
          mod: 4,
          modifierType: 'other',
        },
      ]

      character[actionName]({ weapon, modifiers })
      expect(ruleset.execute).toHaveBeenCalledWith(
        actionIdentifier,
        expect.objectContaining({ weapon, character, modifiers })
      )
    }
  )

  each(baseCombatActions.filter((action) => action !== 'rangedAttack')).it(
    'should execute a base combat action with a shield',
    function (actionName: string) {
      const character = new FoundryCharacter(actor, ruleset)
      const weapon = {} as Weapon
      const shield = {} as Shield
      const actionIdentifier = actionIdentifiers[actionName]

      character[actionName]({ weapon, shield })
      expect(ruleset.execute).toHaveBeenCalledWith(
        actionIdentifier,
        expect.objectContaining({ weapon, character, shield })
      )
    }
  )

  it('should execute a ranged attack action with size and range class', function () {
    const character = new FoundryCharacter(actor, ruleset)
    const weapon = {} as Weapon
    const sizeClass = SizeClass.Tiny
    const rangeClass = RangeClass.Near

    character.rangedAttack({ weapon, sizeClass, rangeClass })
    expect(ruleset.execute).toHaveBeenCalledWith(
      RangedAttackAction,
      expect.objectContaining({ weapon, character, sizeClass, rangeClass })
    )
  })

  it('should execute a dodge action', function () {
    const character = new FoundryCharacter(actor, ruleset)

    character.dodge()
    expect(ruleset.execute).toHaveBeenCalledWith(
      DodgeAction,
      expect.objectContaining({ character })
    )
  })

  each(['offensive', 'defensive']).it(
    'should provide the available maneuvers for the characters of the given maneuver type',
    function (maneuverType: ManeuverType) {
      const character = new FoundryCharacter(actor, ruleset)
      const maneuvers: ModifierDescriptor[] = [
        {
          name: 'TestManeuver',
          modifierType: 'maneuver',
          mod: 0,
        },
      ]

      when(ruleset.compute)
        .calledWith(
          ComputeManeuverList,
          expect.objectContaining({ character, maneuverType })
        )
        .mockReturnValue({ maneuvers })

      const result = character.maneuverList(maneuverType)
      expect(result).toEqual(maneuvers)
    }
  )

  it('should be able to roll initiative if unarmed', function () {
    combatStateData.isArmed = false

    const expected = '1d6 + 9'

    const character = new FoundryCharacter(actor, ruleset)
    when(ruleset.compute)
      .calledWith(
        ComputeInitiativeFormula,
        expect.objectContaining({ character })
      )
      .mockReturnValue(expected)

    const result = character.initiative()
    expect(result).toEqual(expected)
  })

  it('should be able to roll initiative if unarmed and uses an unarmed combat talent', function () {
    combatStateData.isArmed = false
    combatStateData.unarmedTalent = {
      name: 'Raufen',
      isArmed: false,
    } as unknown as CombatTalent
    combatStateData.primaryHand = {
      name: 'Dolch',
      class: 'weapon',
    } as unknown as MeleeWeapon

    const expected = '1d6 + 9'

    const character = new FoundryCharacter(actor, ruleset)
    when(ruleset.compute)
      .calledWith(
        ComputeInitiativeFormula,
        expect.objectContaining({
          character,
          talent: combatStateData.unarmedTalent,
          weapon: undefined,
          shield: undefined,
        })
      )
      .mockReturnValue(expected)

    const result = character.initiative()
    expect(result).toEqual(expected)
  })

  it('should be able to roll initiative if armed and uses a weapon', function () {
    combatStateData.isArmed = true
    combatStateData.unarmedTalent = {
      name: 'Raufen',
      isArmed: false,
    } as unknown as CombatTalent
    combatStateData.primaryHand = {
      name: 'Dolch',
      class: 'weapon',
    } as unknown as MeleeWeapon

    const expected = '1d6 + 9'

    const character = new FoundryCharacter(actor, ruleset)
    when(ruleset.compute)
      .calledWith(
        ComputeInitiativeFormula,
        expect.objectContaining({
          character,
          talent: undefined,
          weapon: combatStateData.primaryHand,
          shield: undefined,
        })
      )
      .mockReturnValue(expected)

    const result = character.initiative()
    expect(result).toEqual(expected)
  })

  it('should be able to roll initiative if armed and uses a weapon and a shield', function () {
    combatStateData.isArmed = true
    combatStateData.unarmedTalent = {
      name: 'Raufen',
      isArmed: false,
    } as unknown as CombatTalent
    combatStateData.primaryHand = {
      name: 'Dolch',
      class: 'weapon',
    } as unknown as MeleeWeapon
    combatStateData.secondaryHand = {
      name: 'Schild',
      class: 'shield',
    } as unknown as Shield

    const expected = '1d6 + 9'

    const character = new FoundryCharacter(actor, ruleset)
    when(ruleset.compute)
      .calledWith(
        ComputeInitiativeFormula,
        expect.objectContaining({
          character,
          talent: undefined,
          weapon: combatStateData.primaryHand,
          shield: combatStateData.secondaryHand,
        })
      )
      .mockReturnValue(expected)

    const result = character.initiative()
    expect(result).toEqual(expected)
  })

  afterEach(() => {
    jest.clearAllMocks()
    ruleset.execute.mockReset()
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    ruleset.compute.__whenMock__ = undefined
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    ruleset.execute.__whenMock__ = undefined
  })
})
