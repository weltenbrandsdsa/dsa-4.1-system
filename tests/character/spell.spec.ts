import { FoundrySpell } from '../../src/module/character/skill'
import type { Character } from '../../src/module/model/character'
import type { TestAttributes } from '../../src/module/model/properties'
import { createMockRuleset } from '../ruleset/rules/helpers'

describe('FoundrySpell', function () {
  const character = {} as Character
  const ruleset = createMockRuleset()

  const spellValue = 12
  const spellName = 'Ignifaxius'
  const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
  const itemData = {
    data: {
      name: spellName,
      data: {
        value: spellValue,
        test: {
          firstAttribute: testAttributes[0],
          secondAttribute: testAttributes[1],
          thirdAttribute: testAttributes[2],
        },
      },
    },
  } as unknown as Item
  const spell = new FoundrySpell(itemData, character, ruleset)

  it('should have its given name', function () {
    expect(spell.name).toEqual(spellName)
  })

  it('should be of skill type spell', function () {
    expect(spell.skillType).toEqual('spell')
  })

  it('should return the correct value of its related data entity', function () {
    expect(spell.value).toEqual(spellValue)
  })

  it('should provide a list of test attributes', function () {
    expect(spell.testAttributes[0]).toEqual(testAttributes[0])
    expect(spell.testAttributes[1]).toEqual(testAttributes[1])
    expect(spell.testAttributes[2]).toEqual(testAttributes[2])
  })
})
