import {
  FoundryMeleeWeapon,
  FoundryRangedWeapon,
} from '../../src/module/character/weapon'

describe('FoundryMeleeWeapon', function () {
  const talentSID = 'Sword'
  const initiativeMod = 3
  const name = 'SuperSword'
  const damageFormula = '1d6+3'
  const strengthMod = {
    threshold: 12,
    hitPointStep: 2,
  }
  const weaponMod = {
    attack: 1,
    parry: -1,
  }
  const itemData = {
    data: {
      name,
      data: {
        talent: talentSID,
        initiativeMod,
        damage: damageFormula,
        strengthMod,
        weaponMod,
      },
    },
  } as unknown as Item

  it('should provide the name of the underlying wepaon', function () {
    const weapon = new FoundryMeleeWeapon(itemData)
    expect(weapon.name).toEqual(name)
  })

  it('should provide the talent name of the underlying wepaon data', function () {
    const weapon = new FoundryMeleeWeapon(itemData)
    expect(weapon.talent).toEqual(talentSID)
  })

  it('should provide the damage formula of the underlying wepaon data', function () {
    const weapon = new FoundryMeleeWeapon(itemData)
    expect(weapon.damage).toEqual(damageFormula)
  })

  it('should provide the initiative mod of the underlying wepaon data', function () {
    const weapon = new FoundryMeleeWeapon(itemData)
    expect(weapon.initiativeMod).toEqual(initiativeMod)
  })

  it('should provide the initiative mod of the underlying wepaon data', function () {
    const weapon = new FoundryMeleeWeapon(itemData)
    expect(weapon.strengthMod).toEqual(strengthMod)
  })

  it('should provide the initiative mod of the underlying wepaon data', function () {
    const weapon = new FoundryMeleeWeapon(itemData)
    expect(weapon.weaponMod).toEqual(weaponMod)
  })
})

describe('FoundryRangedWeapon', function () {
  const talentSID = 'Bow'
  const name = 'Longbow'
  const damageFormula = '1d6+5'
  const itemData = {
    data: {
      name,
      data: {
        talent: talentSID,
        damage: damageFormula,
      },
    },
  } as unknown as Item

  it('should provide the name of the underlying wepaon', function () {
    const weapon = new FoundryRangedWeapon(itemData)
    expect(weapon.name).toEqual(name)
  })

  it('should provide the talent name of the underlying wepaon data', function () {
    const weapon = new FoundryRangedWeapon(itemData)
    expect(weapon.talent).toEqual(talentSID)
  })

  it('should provide the damage formula of the underlying wepaon data', function () {
    const weapon = new FoundryRangedWeapon(itemData)
    expect(weapon.damage).toEqual(damageFormula)
  })
})
