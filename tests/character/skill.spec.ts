import { FoundrySkill } from '../../src/module/character/skill'
import type { Character } from '../../src/module/model/character'
import type { TestAttributes } from '../../src/module/model/properties'
import { createMockRuleset } from '../ruleset/rules/helpers'

describe('FoundrySkill', function () {
  const character = {} as Character
  const ruleset = createMockRuleset()

  it('should have a static creator for can take spell data', function () {
    const spellValue = 12
    const spellName = 'Ignifaxius'
    const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
    const spellData = {
      data: {
        name: spellName,
        type: 'spell',
        data: {
          value: spellValue,
          test: {
            firstAttribute: testAttributes[0],
            secondAttribute: testAttributes[1],
            thirdAttribute: testAttributes[2],
          },
        },
      },
    } as unknown as Item

    const spell = FoundrySkill.create(spellData, character, ruleset)
    expect(spell?.skillType).toEqual('spell')
  })

  it('should have a static creator for can take talent data', function () {
    const talentValue = 12
    const talentName = 'Ignifaxius'
    const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
    const talentData = {
      data: {
        name: talentName,
        type: 'talent',
        data: {
          value: talentValue,
          test: {
            firstAttribute: testAttributes[0],
            secondAttribute: testAttributes[1],
            thirdAttribute: testAttributes[2],
          },
        },
      },
    } as unknown as Item

    const talent = FoundrySkill.create(talentData, character, ruleset)
    expect(talent?.skillType).toEqual('talent')
  })
})
