import { FoundryAttribute } from '../../src/module/character/attribute'
import { RollAttribute } from '../../src/module/ruleset/rules/basic-roll-mechanic'
import { createMockRuleset } from '../ruleset/rules/helpers'

describe('FoundryAttribute', function () {
  const attributeValue = 12
  const actor = {
    data: {
      data: {
        base: {
          basicAttributes: {
            courage: {
              value: attributeValue,
            },
          },
        },
      },
    },
  } as unknown as Actor
  const attributeName = 'courage'
  const ruleset = createMockRuleset()

  const attribute = new FoundryAttribute(actor, attributeName, ruleset)

  it('should be have its given name', function () {
    expect(attribute.name).toEqual(attributeName)
  })

  it('should return the correct value of its related data attribute', function () {
    expect(attribute.value).toEqual(attributeValue)
  })

  it('should give to correct key to the actor data', function () {
    expect(attribute.key).toEqual(
      `data.base.basicAttributes.${attributeName}.value`
    )
  })
})

describe('FoundryAttribute', function () {
  const attributeValue = 12
  const actor = {
    data: {
      data: {
        base: {
          basicAttributes: {
            courage: {
              value: attributeValue,
            },
          },
        },
      },
    },
  } as unknown as Actor
  const attributeName = 'courage'

  const ruleset = createMockRuleset()

  const attribute = new FoundryAttribute(actor, attributeName, ruleset)

  it('should execute the roll attribute action on its given ruleset', function () {
    const rollOptions = {
      targetValue: attributeValue,
    }
    attribute.roll()
    expect(ruleset.execute).toHaveBeenCalledWith(
      RollAttribute,
      expect.objectContaining(rollOptions)
    )
  })
})
