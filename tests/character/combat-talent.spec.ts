import { FoundryCombatTalent } from '../../src/module/character/combat-talent'
import type { Character } from '../../src/module/model/character'
import { when } from 'jest-when'

import {
  ComputeAttack,
  ComputeParry,
  ComputeRangedAttack,
} from '../../src/module/ruleset/rules/derived-combat-attributes'
import { createMockRuleset } from '../ruleset/rules/helpers'

describe('FoundryCombatTalent', function () {
  const baseAttack = 8
  const baseParry = 9
  const baseRangedAttack = 10
  const character = {
    baseAttack,
    baseParry,
    baseRangedAttack,
  } as Character

  const talentValue = 12
  const talentName = 'Swords'
  const attackMod = 4
  const parryMod = 5
  const rangedAttackMod = 7
  const itemData = {
    data: {
      name: talentName,
      data: {
        value: talentValue,
        combat: {
          attack: attackMod,
          parry: parryMod,
          rangedAttack: rangedAttackMod,
        },
      },
    },
  } as unknown as Item
  const ruleset = createMockRuleset()
  const combatTalent = new FoundryCombatTalent(itemData, character, ruleset)

  it('should have its given name', function () {
    expect(combatTalent.name).toEqual(talentName)
  })

  it('should have the attack modifier of the given talent', function () {
    expect(combatTalent.attackMod).toEqual(attackMod)
  })

  it('should have the parry modifier of the given talent', function () {
    expect(combatTalent.parryMod).toEqual(parryMod)
  })

  it('should have the ranged attack modifier of the given talent', function () {
    expect(combatTalent.rangedAttackMod).toEqual(rangedAttackMod)
  })

  it('should return the correct value of its related data entity', function () {
    expect(combatTalent.value).toEqual(talentValue)
  })

  it('should use rule api to compute the attack value', function () {
    const expectedAttack = 15

    when(ruleset.compute)
      .calledWith(ComputeAttack, {
        character,
        talent: combatTalent,
      })
      .mockReturnValue({ value: expectedAttack })
    expect(combatTalent.attack).toEqual(expectedAttack)
  })

  it('should use rule api to compute the parry value', function () {
    const expectedParry = 15

    when(ruleset.compute)
      .calledWith(ComputeParry, {
        character,
        talent: combatTalent,
      })
      .mockReturnValue({ value: expectedParry })
    expect(combatTalent.parry).toEqual(expectedParry)
  })

  it('should use rule api to compute the ranged attack value', function () {
    const expectedRangedAttack = 15

    when(ruleset.compute)
      .calledWith(ComputeRangedAttack, {
        character,
        talent: combatTalent,
      })
      .mockReturnValue({ value: expectedRangedAttack })

    expect(combatTalent.rangedAttack).toEqual(expectedRangedAttack)
  })
})
