import SkillRoll from '../../src/ui/skill-roll/SkillRoll.svelte'
import { render, fireEvent } from '@testing-library/svelte'
import userEvent from '@testing-library/user-event'

import { Character } from '../../src/module/model/character'
import { Skill } from '../../src/module/model/properties'

describe('SkillRollDialog', function () {
  const callback = jest.fn()
  const character = {} as Character
  const testAttributes = ['courage', 'courage', 'courage']
  const skill = {
    testAttributes,
  } as Skill
  const foundryApp = {
    close: jest.fn(),
    setPosition: jest.fn(),
  }

  const customModifier = {
    name: 'custom',
    mod: 0,
    modifierType: 'other',
  }

  beforeEach(() => {
    callback.mockReset()
  })

  test('Submit without changes', async function () {
    const { getByTestId } = render(SkillRoll, {
      callback,
      character,
      skill,
      foundryApp,
    })
    const submitButton = getByTestId('submit-button')
    await fireEvent.click(submitButton)
    expect(callback).toHaveBeenCalledWith([customModifier], 0, testAttributes)
    expect(foundryApp.close).toHaveBeenCalled()
  })

  test('Submit with other test attributes', async function () {
    const { getByTestId } = render(SkillRoll, {
      callback,
      character,
      skill,
      foundryApp,
    })
    const submitButton = getByTestId('submit-button')
    const selectFirstTestAttribute = getByTestId('test-attribute-0')
    await userEvent.selectOptions(selectFirstTestAttribute, 'strength')

    await fireEvent.click(submitButton)
    const expectedTestAttributes = ['strength', 'courage', 'courage']
    expect(callback).toHaveBeenCalledWith(
      [customModifier],
      0,
      expectedTestAttributes
    )
    expect(foundryApp.close).toHaveBeenCalled()
  })

  test('Submit with changed modifier', async function () {
    const { getByTestId } = render(SkillRoll, {
      callback,
      character,
      skill,
      foundryApp,
    })
    const submitButton = getByTestId('submit-button')
    const modifierInput = getByTestId('modifier-input')
    const expectedModifier = 5
    await userEvent.type(modifierInput, expectedModifier.toString())

    await fireEvent.click(submitButton)
    const expectedCustomModifier = {
      name: 'custom',
      mod: expectedModifier,
      modifierType: 'other',
    }
    expect(callback).toHaveBeenCalledWith(
      [expectedCustomModifier],
      expectedModifier,
      testAttributes
    )
    expect(foundryApp.close).toHaveBeenCalled()
  })

  test('Hide effective encumbarance for other skills', async function () {
    const skill = {
      skillType: 'spell',
      testAttributes,
    }

    const { queryByTestId } = render(SkillRoll, {
      callback,
      character,
      skill,
      foundryApp,
    })
    const effectiveEncumbranceCheckbox = queryByTestId(
      'effective-encumbarance-checkbox'
    )
    expect(effectiveEncumbranceCheckbox).not.toBeInTheDocument()
  })

  test('Submit with effective encumbarance', async function () {
    const effectiveEncumbrance = 5
    const skill = {
      skillType: 'talent',
      effectiveEncumbarance: {
        formula: 'BE',
      },
      testAttributes,
    }
    const character = {
      effectiveEncumbarance: jest.fn().mockReturnValue(effectiveEncumbrance),
    } as unknown as Character

    const { getByTestId } = render(SkillRoll, {
      callback,
      character,
      skill,
      foundryApp,
    })
    const submitButton = getByTestId('submit-button')
    const effectiveEncumbranceCheckbox = getByTestId(
      'effective-encumbarance-checkbox'
    )
    await userEvent.click(effectiveEncumbranceCheckbox)

    await fireEvent.click(submitButton)
    const encumbaranceModifier = {
      name: 'effectiveEncumbarance',
      mod: effectiveEncumbrance,
      modifierType: 'other',
    }
    expect(callback).toHaveBeenCalledWith(
      [customModifier, encumbaranceModifier],
      effectiveEncumbrance,
      testAttributes
    )
    expect(foundryApp.close).toHaveBeenCalled()
  })
})
