import Attribute from '../src/ui/Attribute.svelte'
import { render, fireEvent } from '@testing-library/svelte'

describe('ExampleSvelteTest', function () {
  test('test2', async function () {
    const rollAttribute = jest.fn()
    const { getByTestId } = render(Attribute, {
      attribute: {
        value: 10,
        name: 'TEst',
        key: '',
        roll: rollAttribute,
      },
    })
    const attributeRoll = getByTestId('attribute-roll')
    await fireEvent.click(attributeRoll)
    expect(rollAttribute).toHaveBeenCalled()
  })
})
