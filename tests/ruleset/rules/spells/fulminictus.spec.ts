import {
  Fulminictus,
  FulminictusRule,
} from '../../../../src/module/ruleset/rules/spells/fulminictus'
import { TestAction } from '../../test-classes'

import {
  SkillActionData,
  SpellAction,
} from '../../../../src/module/ruleset/rules/basic-skill'
import type { Character } from '../../../../src/module/model/character'
import { createTestRuleset } from '../helpers'

describe('Fulminictus', function () {
  const ruleset = createTestRuleset()

  const spellActionResult = {} as any
  const executeHook = jest.fn().mockReturnValue(spellActionResult)
  const skillRollAction = new TestAction(SpellAction, executeHook)
  ruleset.registerAction(skillRollAction)

  ruleset.add(FulminictusRule)
  ruleset.compileRules()

  const character = {} as Character

  it('should add damage formula on success', async function () {
    const options: SkillActionData = {
      character,
      skill: Fulminictus,
    }
    spellActionResult.success = true
    spellActionResult.options = {}
    const rollResult = 5
    spellActionResult.roll = {
      total: rollResult,
    }
    const expectedDamage = '2d6 + 5'

    const result = await ruleset.execute(SpellAction, options)

    expect(result.damage).toEqual(expectedDamage)
  })
})
