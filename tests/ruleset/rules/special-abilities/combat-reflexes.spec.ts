import { when } from 'jest-when'
import {
  CombatReflexes,
  CombatReflexesRule,
} from '../../../../src/module/ruleset/rules/special-abilities/combat-reflexes'

import type { Character } from '../../../../src/module/model/character'
import { Computation } from '../../../../src/module/ruleset/rule-components'
import { createTestRuleset } from '../helpers'
import { ComputeInitiative } from '../../../../src/module/ruleset/rules/basic-combat'

describe('Combat reflexes', function () {
  const ruleset = createTestRuleset()
  const initiativeValue = 10

  const executeHook = jest.fn().mockReturnValue(initiativeValue)
  ruleset.registerComputation(new Computation(ComputeInitiative, executeHook))

  ruleset.add(CombatReflexesRule)
  ruleset.compileRules()

  const character = {} as Character

  it('should add 4 to the initiative if the character has combat reflexes', async function () {
    character.has = jest.fn()

    const options = {
      character,
    }

    when(character.has).calledWith(CombatReflexes).mockReturnValue(true)

    const result = ruleset.compute(ComputeInitiative, options)

    expect(result).toEqual(14)
  })

  it('should not change the initiative if the character has no combat reflexes', async function () {
    character.has = jest.fn()

    const options = {
      character,
    }

    when(character.has).calledWith(CombatReflexes).mockReturnValue(false)

    const result = ruleset.compute(ComputeInitiative, options)

    expect(result).toEqual(10)
  })
})
