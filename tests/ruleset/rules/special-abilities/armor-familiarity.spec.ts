import { when } from 'jest-when'
import {
  ArmorFamiliarityI,
  ArmorFamiliarityII,
  ArmorFamiliarityIII,
  ArmorFamiliarityRule,
} from '../../../../src/module/ruleset/rules/special-abilities/armor-familiarity'

import type { Character } from '../../../../src/module/model/character'
import { ComputeEncumbarance } from '../../../../src/module/ruleset/rules/derived-combat-attributes'
import { Computation } from '../../../../src/module/ruleset/rule-components'
import { createTestRuleset } from '../helpers'
import { ComputeInitiative } from '../../../../src/module/ruleset/rules/basic-combat'
import each from 'jest-each'
import { Ability } from '../../../../src/module/model/properties'

describe('Armor familiarity', function () {
  const ruleset = createTestRuleset()

  const executeHook = jest.fn()
  ruleset.registerComputation(new Computation(ComputeEncumbarance, executeHook))
  ruleset.registerComputation(new Computation(ComputeInitiative, executeHook))

  ruleset.add(ArmorFamiliarityRule)
  ruleset.compileRules()

  const character = {} as Character

  it('should not reduce encumbarance if character possess no armor familiarity', async function () {
    character.has = jest.fn()
    const encumbarance = 2
    executeHook.mockReturnValue(encumbarance)

    character.armorItems = [
      {
        name: 'Kettenhemd',
        armorClass: 2,
        encumbarance,
      },
    ]
    const expectedEncumbarance = 2

    const options = {
      character,
    }

    when(character.has).calledWith(ArmorFamiliarityI).mockReturnValue(false)

    const result = await ruleset.compute(ComputeEncumbarance, options)

    expect(result).toEqual(expectedEncumbarance)
  })

  it('should reduce encumbarance if character possess armor familiarity 1 and the wears the correpsponding armor', async function () {
    character.has = jest.fn()
    const encumbarance = 2
    executeHook.mockReturnValue(encumbarance)

    character.armorItems = [
      {
        name: 'Kettenhemd',
        armorClass: 2,
        encumbarance,
      },
    ]
    const expectedEncumbarance = 1

    const options = {
      character,
    }

    when(character.has).calledWith(ArmorFamiliarityI).mockReturnValue(true)

    const result = await ruleset.compute(ComputeEncumbarance, options)

    expect(result).toEqual(expectedEncumbarance)
  })

  each([ArmorFamiliarityI, ArmorFamiliarityII, ArmorFamiliarityIII]).it(
    'should not reduce encumbarance below zero for any armor familiarity',
    async function (ability: Ability) {
      character.has = jest.fn()
      const encumbarance = 0
      executeHook.mockReturnValue(encumbarance)

      const options = {
        character,
      }

      when(character.has).calledWith(ability).mockReturnValue(true)

      const result = await ruleset.compute(ComputeEncumbarance, options)

      expect(result).toBeGreaterThanOrEqual(0)
    }
  )

  it('should reduce encumbarance if character possess armor familiarity 1 and 2', async function () {
    character.has = jest.fn()
    const encumbarance = 2
    executeHook.mockReturnValue(encumbarance)

    character.armorItems = [
      {
        name: 'Kettenhemd',
        armorClass: 2,
        encumbarance,
      },
    ]
    const expectedEncumbarance = 1

    const options = {
      character,
    }

    when(character.has).calledWith(ArmorFamiliarityII).mockReturnValue(true)

    const result = await ruleset.compute(ComputeEncumbarance, options)

    expect(result).toEqual(expectedEncumbarance)
  })

  //   it('should not reduce encumbarance if character possess armor familiarity 1 and the wears not the correpsponding armor', async function () {
  //     const mightyStrike = MightyStrikeManeuver
  //     mightyStrike.mod = 5
  //     const options = {
  //       character,
  //       modifiers: [mightyStrike],
  //     }
  //     computationResult.options = options
  //     character.has = jest.fn()

  //     when(character.has).calledWith(MightyStrike).mockReturnValue(true)

  //     const result = await ruleset.compute(ComputeDamageFormula, options)

  //     expect(result.bonusDamage).toEqual(mightyStrike.mod)
  //   })

  it('should reduce encumbarance by 2 if character possess armor familiarity 3', async function () {
    character.has = jest.fn()
    const encumbarance = 4
    executeHook.mockReturnValue(encumbarance)

    character.armorItems = [
      {
        name: 'Kettenhemd',
        armorClass: 2,
        encumbarance,
      },
    ]
    const expectedEncumbarance = 2

    const options = {
      character,
    }

    when(character.has).calledWith(ArmorFamiliarityI).mockReturnValue(true)
    when(character.has).calledWith(ArmorFamiliarityII).mockReturnValue(true)
    when(character.has).calledWith(ArmorFamiliarityIII).mockReturnValue(true)

    const result = await ruleset.compute(ComputeEncumbarance, options)

    expect(result).toEqual(expectedEncumbarance)
  })

  it('should reduce initiative malus if character possess armor familiarity 3', async function () {
    character.has = jest.fn()
    const baseInitiative = 10
    character.encumbarance = 5
    executeHook.mockReturnValue(baseInitiative - character.encumbarance)

    const expectedInitiative = 8

    const options = {
      character,
    }

    when(character.has).calledWith(ArmorFamiliarityIII).mockReturnValue(true)

    const result = await ruleset.compute(ComputeInitiative, options)

    expect(result).toEqual(expectedInitiative)
  })
})
