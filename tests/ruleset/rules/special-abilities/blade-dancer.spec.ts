import { when } from 'jest-when'
import {
  BladeDancer,
  BladeDancerRule,
} from '../../../../src/module/ruleset/rules/special-abilities/blade-dancer'

import type { Character } from '../../../../src/module/model/character'
import {} from '../../../../src/module/ruleset/rules/derived-combat-attributes'
import { Computation } from '../../../../src/module/ruleset/rule-components'
import { createTestRuleset } from '../helpers'
import { ComputeInitiativeFormula } from '../../../../src/module/ruleset/rules/basic-combat'

describe('Blade dancer', function () {
  const ruleset = createTestRuleset()

  const executeHook = jest.fn()
  ruleset.registerComputation(
    new Computation(ComputeInitiativeFormula, executeHook)
  )

  ruleset.add(BladeDancerRule)
  ruleset.compileRules()

  const character = {} as Character

  it('should add an additional dice to the initiative formula', async function () {
    character.has = jest.fn()

    const expectedDices = 2

    const options = {
      character,
    }

    when(character.has).calledWith(BladeDancer).mockReturnValue(true)

    ruleset.compute(ComputeInitiativeFormula, options)

    expect(executeHook).toBeCalledWith(
      expect.objectContaining({
        dices: expectedDices,
      })
    )
  })
})
