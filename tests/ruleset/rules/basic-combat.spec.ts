import each from 'jest-each'
import {
  ActionIdentifier,
  Computation,
} from '../../../src/module/ruleset/rule-components'
import {
  ComputeInitiativeFormula,
  AttackAction,
  ParryAction,
  DodgeAction,
  BasicCombatRule,
  CombatActionData,
  CombatActionResult,
} from '../../../src/module/ruleset/rules/basic-combat'
import type { Character } from '../../../src/module/model/character'
import { when } from 'jest-when'
import type {
  Weapon,
  MeleeWeapon,
  Shield,
} from '../../../src/module/model/items'
import {
  RollAttributeToChatEffect,
  RollCombatAttribute,
} from '../../../src/module/ruleset/rules/basic-roll-mechanic'
import { EffectSpy, TestAction } from '../test-classes'
import { ComputeDamageFormula } from '../../../src/module/ruleset/rules/derived-combat-attributes'
import { createTestRuleset } from './helpers'

describe('BasicCombatRule', function () {
  let character = {} as Character

  const ruleset = createTestRuleset()

  const executeHook = jest.fn()
  ruleset.registerComputation(
    new Computation(ComputeDamageFormula, executeHook)
  )

  ruleset.add(BasicCombatRule)
  ruleset.compileRules()

  const effectSpy = new EffectSpy(RollAttributeToChatEffect)
  ruleset.registerEffect(effectSpy)

  const basicCombatActions = ['attack', 'parry', 'dodge']
  const actionIdentifiers = {
    attack: AttackAction,
    parry: ParryAction,
    dodge: DodgeAction,
  }
  const combatValues = {
    attack: 12,
    parry: 13,
    dodge: 14,
  }

  each(basicCombatActions).it(
    'should provide basic attack, parry and dodge actions',
    async function (actionName) {
      const executeHook = jest.fn().mockReturnValue({})
      const rollAction = new TestAction(RollCombatAttribute, executeHook)
      ruleset.registerAction(rollAction)

      const weapon = {} as Weapon
      character[`${actionName}Value`] = jest.fn()
      when(character[`${actionName}Value`])
        .calledWith(expect.objectContaining({ weapon }))
        .mockReturnValue(combatValues[actionName])

      const identifier: ActionIdentifier<CombatActionData, CombatActionResult> =
        actionIdentifiers[actionName]
      const result = await ruleset.execute(identifier, {
        character,
        weapon,
      })

      expect(result.options.action).toEqual(identifier)
      expect(executeHook).toHaveBeenCalledWith(
        expect.objectContaining({
          targetValue: combatValues[actionName],
          mod: 0,
        })
      )
    }
  )

  each([
    [5, '1d6 + 5.05'],
    [11, '1d6 + 11.11'],
  ]).it(
    'should provide a computation for the initiative',
    function (baseInitiative: number, expectedResult: string) {
      const character = {} as Character
      character.baseInitiative = baseInitiative
      const result = ruleset.compute(ComputeInitiativeFormula, {
        character,
      })

      expect(result.formula).toEqual(expectedResult)
    }
  )

  it('should compute the initiative based on a reference to the base initiative', function () {
    const character = {} as Character
    character.baseInitiative = 10
    const result = ruleset.compute(ComputeInitiativeFormula, {
      character,
    })

    expect(result.formula).toEqual('1d6 + 10.10')
    character.baseInitiative = 11
    expect(result.formula).toEqual('1d6 + 11.11')
  })

  it('should should the base roll formula in the initiative data', function () {
    const character = {} as Character
    const result = ruleset.compute(ComputeInitiativeFormula, {
      character,
    })

    expect(result.rollFormula).toEqual('1d6')
  })

  each([
    [3, 10, 13.1],
    [-2, 10, 8.1],
    [1, 9, 10.09],
  ]).it(
    'should provide an initiative formula that can compute a result with a given modifier',
    function (modifier: number, baseInitiative: number, expected: number) {
      const character = {} as Character
      character.baseInitiative = baseInitiative
      const result = ruleset.compute(ComputeInitiativeFormula, {
        character,
      })

      expect(result.initiative(modifier)).toEqual(expected)
    }
  )

  it('should provide a computation for the initiative which uses the weapon mod', function () {
    const baseInitiative = 11
    character.baseInitiative = baseInitiative

    const initiativeMod = 3
    const weapon = {
      type: 'melee',
      initiativeMod,
    } as MeleeWeapon
    const result = ruleset.compute(ComputeInitiativeFormula, {
      character,
      weapon,
    })

    const expectedResult = '1d6 + 14.11'

    expect(result.formula).toEqual(expectedResult)
  })

  it('should provide a computation for the initiative which reduces it by the encumbarance', function () {
    const baseInitiative = 11
    character.baseInitiative = baseInitiative

    const encumbarance = 3
    character.encumbarance = encumbarance

    const result = ruleset.compute(ComputeInitiativeFormula, {
      character,
    })

    const expectedResult = '1d6 + 8.11'

    expect(result.formula).toEqual(expectedResult)
  })

  it('should provide a computation for the initiative which uses the shield mod', function () {
    const shield = {} as Shield

    const baseInitiative = 11
    character.baseInitiative = baseInitiative

    const initiativeMod = 2
    shield.initiativeMod = initiativeMod
    const result = ruleset.compute(ComputeInitiativeFormula, {
      character,
      shield,
    })

    const expectedResult = '1d6 + 13.11'

    expect(result.formula).toEqual(expectedResult)
  })

  // each(basicCombatActions).it(
  //   'should generate a chat message for the associated roll',
  //   async function (actionName) {
  //     const mockAction = mock<Action>()
  //     const rollAction = instance(mockAction)
  //     when(mockAction.identifier).thenReturn(RollCombatAttribute)

  //     const mockEffect = mock<Effect>()
  //     const chatEffect = instance(mockEffect)
  //     when(mockEffect.identifier).thenReturn(RollAttributeToChatEffect)
  //     ruleset.registerEffect(chatEffect)

  //     when(mockAction.execute(anything())).thenReturn(
  //       Promise.resolve({} as any)
  //     )
  //     ruleset.registerAction(rollAction)

  //     const mockWeapon = mock<Weapon>()
  //     const weapon = instance(mockWeapon)
  //     when(mockCharacter[`${actionName}Value`](weapon)).thenReturn(
  //       combatValues[actionName]
  //     )
  //     const identifier = actionIdentifiers[actionName]
  //     await ruleset.execute(identifier, {
  //       character,
  //       weapon,
  //     })

  //     verify(mockEffect.apply(anything())).called()
  //   }
  // )

  it('should generate a chat message for the associated roll', async function () {
    const actionName = 'attack'
    const weaponDamage = '1d6 + 3'
    executeHook.mockImplementation((_options) => weaponDamage)

    const rollExecuteHook = jest.fn().mockReturnValue({
      success: true,
    })
    const rollAction = new TestAction(RollCombatAttribute, rollExecuteHook)
    ruleset.registerAction(rollAction)

    const weapon = {} as Weapon
    character[`${actionName}Value`] = jest.fn()
    when(character[`${actionName}Value`])
      .calledWith(weapon)
      .mockReturnValue(combatValues[actionName])

    character.damage = jest.fn()
    when(character.damage).calledWith(weapon).mockReturnValue(weaponDamage)
    const identifier = actionIdentifiers[actionName]
    await ruleset.execute(identifier, {
      character,
      weapon,
    })

    expect(effectSpy.result.damage).toEqual(weaponDamage)
  })

  afterEach(() => {
    character = {} as Character
  })
})
