import { when } from 'jest-when'
import type { Character } from '../../../src/module/model/character'
import {
  TestAction,
  TestEffect,
  TestEffectListenedResult,
} from '../test-classes'
import {
  RollSkill,
  RollSkillToChatEffect,
} from '../../../src/module/ruleset/rules/basic-roll-mechanic'
import {
  BasicSkillRule,
  TalentAction,
} from '../../../src/module/ruleset/rules/basic-skill'
import type {
  Attribute,
  AttributeName,
  SkillDescriptor,
  Spell,
  Talent,
  TestAttributes,
} from '../../../src/module/model/properties'
import { SpellAction } from '../../../src/module/ruleset/rules/basic-skill'
import { createTestRuleset } from './helpers'

describe('BasicSkillRule', function () {
  const character = {} as Character

  const ruleset = createTestRuleset()

  const executeHook = jest.fn().mockReturnValue({})
  const skillRollAction = new TestAction(RollSkill, executeHook)
  ruleset.registerAction(skillRollAction)

  ruleset.registerEffect<TestEffectListenedResult, TestEffect>(
    new TestEffect(RollSkillToChatEffect)
  )

  ruleset.add(BasicSkillRule)
  ruleset.compileRules()

  const talentDescriptor: SkillDescriptor = {
    name: 'Swimming',
    identifier: 'talent-swimming',
    skillType: 'talent',
  }
  const spellDescriptor: SkillDescriptor = {
    name: 'Ignifaxius',
    identifier: 'spell-ignifaxius',
    skillType: 'spell',
  }

  const attributeRollMock = jest.fn()
  const courage = {
    name: 'courage',
    value: 10,
    key: '',
    roll: attributeRollMock,
  } as Attribute

  const cleverness = {
    name: 'cleverness',
    value: 11,
    key: '',
    roll: attributeRollMock,
  } as Attribute

  const agility = {
    name: 'agility',
    value: 12,
    key: '',
    roll: attributeRollMock,
  } as Attribute

  const talent = {} as Talent
  const spell = {} as Spell

  type TestAttributeData = { name: AttributeName; value: number }
  const testAttributeData: [
    TestAttributeData,
    TestAttributeData,
    TestAttributeData
  ] = [
    {
      name: courage.name,
      value: courage.value,
    },
    {
      name: cleverness.name,
      value: cleverness.value,
    },
    {
      name: agility.name,
      value: agility.value,
    },
  ]
  const skillValue = 8
  const testAttributes: TestAttributes = testAttributeData.map(
    (attributeData) => attributeData.name as AttributeName
  ) as TestAttributes

  talent.value = skillValue
  talent.testAttributes = testAttributes
  spell.value = skillValue
  spell.testAttributes = testAttributes
  character.talent = jest.fn()
  when(character.talent)
    .calledWith(talentDescriptor.identifier)
    .mockReturnValue(talent)
  character.spell = jest.fn()
  when(character.spell)
    .calledWith(spellDescriptor.identifier)
    .mockReturnValue(spell)
  character.attribute = jest.fn()
  when(character.attribute).calledWith(courage.name).mockReturnValue(courage)
  when(character.attribute)
    .calledWith(cleverness.name)
    .mockReturnValue(cleverness)
  when(character.attribute).calledWith(agility.name).mockReturnValue(agility)

  it('should provide basic talent roll action', async function () {
    const mod = 5
    await ruleset.execute(TalentAction, {
      character,
      mod,
      skill: talentDescriptor,
    })

    expect(executeHook).toBeCalledWith(
      expect.objectContaining({
        skillName: talentDescriptor.name,
        skillType: talentDescriptor.skillType,
        skillValue,
        mod,
        testAttributeData,
      })
    )
  })

  it('should provide basic spell roll action', async function () {
    const mod = 5
    await ruleset.execute(SpellAction, {
      character,
      mod,
      skill: spellDescriptor,
    })

    expect(executeHook).toBeCalledWith(
      expect.objectContaining({
        skillName: spellDescriptor.name,
        skillType: spellDescriptor.skillType,
        skillValue,
        mod,
        testAttributeData,
      })
    )
  })
})
