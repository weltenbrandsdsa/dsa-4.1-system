import each from 'jest-each'

import { Computation } from '../../../src/module/ruleset/rule-components'
import {
  ComputeRangedAttack,
  RangeClass,
  SizeClass,
} from '../../../src/module/ruleset/rules/derived-combat-attributes'
import {
  BasicRangedCombatRule,
  RangedAttackAction,
  RangedCombatActionData,
} from '../../../src/module/ruleset/rules/basic-ranged-combat'
import { when } from 'jest-when'
import type { Character } from '../../../src/module/model/character'
import type { Weapon } from '../../../src/module/model/items'
import {
  RollAttributeToChatEffect,
  RollCombatAttribute,
} from '../../../src/module/ruleset/rules/basic-roll-mechanic'
import { createTestRuleset } from './helpers'
import { EffectSpy, TestAction } from '../test-classes'

describe('BasicRangedCombatRule', function () {
  const character = {} as Character

  const ruleset = createTestRuleset()

  const identifier = RangedAttackAction
  const computeHook = jest.fn().mockReturnValue({})
  ruleset.registerComputation(new Computation(ComputeRangedAttack, computeHook))
  ruleset.add(BasicRangedCombatRule)
  ruleset.compileRules()
  const rangedAttackValue = 12
  const executeHook = jest.fn().mockReturnValue({})
  const rollAction = new TestAction(RollCombatAttribute, executeHook)
  ruleset.registerAction(rollAction)
  const effectSpy = new EffectSpy(RollAttributeToChatEffect)
  ruleset.registerEffect(effectSpy)

  const weapon = {} as Weapon

  character.rangedAttackValue = jest.fn()
  when(character.rangedAttackValue)
    .calledWith(expect.objectContaining({ weapon }))
    .mockReturnValue(rangedAttackValue)

  let options: RangedCombatActionData

  beforeEach(function () {
    executeHook.mockClear()
    options = {
      character,
      weapon,
      rangeClass: RangeClass.Near,
      sizeClass: SizeClass.Big,
    }
  })

  it('should provide basic ranged attack actions', async function () {
    const result = await ruleset.execute(identifier, options)

    expect(result.options.action).toEqual(identifier)
    expect(executeHook).toHaveBeenCalledWith(
      expect.objectContaining({
        targetValue: rangedAttackValue,
        mod: 0,
      })
    )
  })

  it('should generate a chat message for the associated roll', async function () {
    executeHook.mockReturnValueOnce({
      success: true,
    })

    const weaponDamage = '1d6 + 3'
    const weapon = {} as Weapon
    character.rangedAttackValue = jest.fn()
    when(character.rangedAttackValue)
      .calledWith(expect.objectContaining({ weapon }))
      .mockReturnValue(rangedAttackValue)

    character.damage = jest.fn()
    when(character.damage)
      .calledWith(expect.objectContaining({ weapon }))
      .mockReturnValue(weaponDamage)
    await ruleset.execute(identifier, options)

    // expect(effectSpy.result.damage).toEqual(weaponDamage)
  })

  const rangeModMap = {
    [RangeClass.VeryNear]: -2,
    [RangeClass.Near]: 0,
    [RangeClass.Medium]: 4,
    [RangeClass.Far]: 8,
    [RangeClass.VeryFar]: 12,
  }

  const ranges = Object.keys(rangeModMap)

  each(ranges).test(
    'should provide a computation for the range modificator to the actor',
    function (range) {
      const result = ruleset.compute(ComputeRangedAttack, {
        ...options,
        rangeClass: range,
      })
      expect(result.mod).toEqual(rangeModMap[range])
    }
  )

  each(ranges).test(
    'should apply the correct modificator based on range class',
    async function (range) {
      await ruleset.execute(RangedAttackAction, {
        ...options,
        rangeClass: range,
      })

      expect(executeHook).toHaveBeenCalledWith(
        expect.objectContaining({
          targetValue: rangedAttackValue,
          mod: rangeModMap[range],
        })
      )
    }
  )

  const sizeModMap = {
    [SizeClass.Tiny]: 8,
    [SizeClass.VerySmall]: 6,
    [SizeClass.Small]: 4,
    [SizeClass.Medium]: 2,
    [SizeClass.Big]: 0,
    [SizeClass.VeryBig]: -2,
  }

  const sizes = Object.keys(sizeModMap)

  each(sizes).test(
    'should provide a computation for the size modificator to the actor',
    function (size) {
      const result = ruleset.compute(ComputeRangedAttack, {
        ...options,
        sizeClass: size,
      })

      expect(result.mod).toEqual(sizeModMap[size])
    }
  )

  each(sizes).test(
    'should apply the correct modificator based on target size',
    async function (size) {
      await ruleset.execute(RangedAttackAction, {
        ...options,
        sizeClass: size,
      })

      expect(executeHook).toHaveBeenCalledWith(
        expect.objectContaining({
          targetValue: rangedAttackValue,
          mod: sizeModMap[size],
        })
      )
    }
  )
})
