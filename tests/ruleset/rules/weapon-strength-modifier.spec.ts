import {
  ComputeDamageFormula,
  createDamageFormula,
} from '../../../src/module/ruleset/rules/derived-combat-attributes'
import { WeaponStrengthModifierRule } from '../../../src/module/ruleset/rules/weapon-strength-modifier'
import type { Attribute } from '../../../src/module/model/properties'
import each from 'jest-each'
import { when } from 'jest-when'
import type { Character } from '../../../src/module/model/character'
import type { MeleeWeapon } from '../../../src/module/model/items'
import { createTestRuleset } from './helpers'
import { Computation } from '../../../src/module/ruleset/rule-components'

describe('WeaponStrengthModifier', function () {
  const character = {} as Character

  const ruleset = createTestRuleset()

  const computeDamage = jest.fn()
  ruleset.registerComputation(
    new Computation(ComputeDamageFormula, computeDamage)
  )

  ruleset.add(WeaponStrengthModifierRule)
  ruleset.compileRules()

  const weapon = {} as MeleeWeapon

  const strengthAttribute = {} as Attribute

  const strength = 15
  strengthAttribute.value = strength
  character.attribute = jest.fn()
  when(character.attribute)
    .calledWith('strength')
    .mockReturnValue(strengthAttribute)

  const baseDamage = '1d6 + 3'
  weapon.damage = baseDamage
  weapon.type = 'melee'

  beforeEach(() => {
    computeDamage.mockReturnValue(createDamageFormula(baseDamage))
  })

  each([
    [13, 2, '1d6 + 3 + 1'],
    [14, 2, '1d6 + 3'],
    [16, 2, '1d6 + 3'],
    [17, 2, '1d6 + 3 + -1'],
  ]).it(
    'should add weapon strength modifier to damage',
    function (threshold, hitPointStep, expectedDamage) {
      const strengthMod = {
        threshold,
        hitPointStep,
      }
      weapon.strengthMod = strengthMod

      const result = ruleset.compute(ComputeDamageFormula, {
        character,
        weapon,
      })

      expect(result.formula).toEqual(expectedDamage)
    }
  )
})
