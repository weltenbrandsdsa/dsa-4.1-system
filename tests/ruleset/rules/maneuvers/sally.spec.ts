import { when } from 'jest-when'

import {
  SallyRule,
  SallyManeuver,
  Sally,
} from '../../../../src/module/ruleset/rules/maneuvers/sally'

import type { Character } from '../../../../src/module/model/character'
import { Computation } from '../../../../src/module/ruleset/rule-components'
import { createTestRuleset } from '../helpers'
import { ComputeManeuverList } from '../../../../src/module/ruleset/rules/maneuvers/basic-maneuver'
import each from 'jest-each'
import type { ManeuverType } from '../../../../src/module/model/modifier'

describe('Sally', function () {
  const ruleset = createTestRuleset()

  ruleset.registerComputation(
    new Computation(
      ComputeManeuverList,
      jest.fn().mockImplementation(() => ({ maneuvers: [] }))
    )
  )

  ruleset.add(SallyRule)
  ruleset.compileRules()

  const character = {} as Character

  each([
    [false, 'offensive', false],
    [false, 'defensive', false],
    [false, 'defensive', true],
    [true, 'offensive', true],
  ]).it(
    'should add sally only if the character has the ability and the list is for offensive maneuvers',
    function (
      expected: boolean,
      maneuverType: ManeuverType,
      hasAbility: boolean
    ) {
      character.has = jest.fn()
      when(character.has).calledWith(Sally).mockReturnValue(hasAbility)

      const result = ruleset.compute(ComputeManeuverList, {
        character,
        maneuverType,
      })

      expect(result.maneuvers.includes(SallyManeuver)).toEqual(expected)
    }
  )
})
