import { when } from 'jest-when'
import {
  MightyStrike,
  MightyStrikeManeuver,
  MightyStrikeRule,
} from '../../../../src/module/ruleset/rules/maneuvers/mighty-strike'

import type { Character } from '../../../../src/module/model/character'
import { ComputeDamageFormula } from '../../../../src/module/ruleset/rules/derived-combat-attributes'
import { Computation } from '../../../../src/module/ruleset/rule-components'
import { createTestRuleset } from '../helpers'
import { ComputeManeuverList } from '../../../../src/module/ruleset/rules/maneuvers/basic-maneuver'

describe('Mighty Strike', function () {
  const ruleset = createTestRuleset()

  const computationResult = {} as any

  const executeHook = jest.fn().mockReturnValue(computationResult)
  ruleset.registerComputation(
    new Computation(ComputeDamageFormula, executeHook)
  )
  ruleset.registerComputation(
    new Computation(
      ComputeManeuverList,
      jest.fn().mockImplementation(() => ({ maneuvers: [] }))
    )
  )

  ruleset.add(MightyStrikeRule)
  ruleset.compileRules()

  const character = {} as Character

  it('should compute bonus damage on success if character possess mighty strike', async function () {
    const mightyStrike = MightyStrikeManeuver
    mightyStrike.mod = 5
    const options = {
      character,
      modifiers: [mightyStrike],
    }
    computationResult.options = options
    character.has = jest.fn()

    when(character.has).calledWith(MightyStrike).mockReturnValue(true)

    const result = await ruleset.compute(ComputeDamageFormula, options)

    expect(result.bonusDamage).toEqual(mightyStrike.mod)
  })

  it('should only give half bonus damage if character does not possess mighty strike', async function () {
    const mightyStrike = MightyStrikeManeuver
    mightyStrike.mod = 5
    const expectedBonusDamage = 3
    const options = {
      character,
      modifiers: [mightyStrike],
      weapon: undefined,
    }
    computationResult.options = options
    when(character.has).calledWith(MightyStrike).mockReturnValue(false)

    const result = await ruleset.compute(ComputeDamageFormula, options)

    expect(result.bonusDamage).toEqual(expectedBonusDamage)
  })

  it('should add mighty strike to the list of offensive maneuvers', function () {
    const result = ruleset.compute(ComputeManeuverList, {
      character,
      maneuverType: 'offensive',
    })

    expect(result.maneuvers.includes(MightyStrikeManeuver)).toEqual(true)
  })

  it('should not add mighty strike to the list of defensive maneuvers', function () {
    const result = ruleset.compute(ComputeManeuverList, {
      character,
      maneuverType: 'defensive',
    })

    expect(result.maneuvers.includes(MightyStrikeManeuver)).toEqual(false)
  })
})
