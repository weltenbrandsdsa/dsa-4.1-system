import {
  FeintRule,
  FeintManeuver,
} from '../../../../src/module/ruleset/rules/maneuvers/feint'

import type { Character } from '../../../../src/module/model/character'
import { Computation } from '../../../../src/module/ruleset/rule-components'
import { createTestRuleset } from '../helpers'
import { ComputeManeuverList } from '../../../../src/module/ruleset/rules/maneuvers/basic-maneuver'

describe('Feint', function () {
  const ruleset = createTestRuleset()

  ruleset.registerComputation(
    new Computation(
      ComputeManeuverList,
      jest.fn().mockImplementation(() => ({ maneuvers: [] }))
    )
  )

  ruleset.add(FeintRule)
  ruleset.compileRules()

  const character = {} as Character

  it('should add feint to the list of offensive maneuvers', function () {
    const result = ruleset.compute(ComputeManeuverList, {
      character,
      maneuverType: 'offensive',
    })

    expect(result.maneuvers.includes(FeintManeuver)).toEqual(true)
  })

  it('should not add feint to the list of defensive maneuvers', function () {
    const result = ruleset.compute(ComputeManeuverList, {
      character,
      maneuverType: 'defensive',
    })

    expect(result.maneuvers.includes(FeintManeuver)).toEqual(false)
  })
})
