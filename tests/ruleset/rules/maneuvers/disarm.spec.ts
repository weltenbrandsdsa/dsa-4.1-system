import { when } from 'jest-when'

import {
  DisarmRule,
  DisarmManeuver,
  Disarm,
} from '../../../../src/module/ruleset/rules/maneuvers/disarm'

import type { Character } from '../../../../src/module/model/character'
import { Computation } from '../../../../src/module/ruleset/rule-components'
import { createTestRuleset } from '../helpers'
import { ComputeManeuverList } from '../../../../src/module/ruleset/rules/maneuvers/basic-maneuver'
import each from 'jest-each'
import type { ManeuverType } from '../../../../src/module/model/modifier'

describe('Disarm', function () {
  const ruleset = createTestRuleset()

  ruleset.registerComputation(
    new Computation(
      ComputeManeuverList,
      jest.fn().mockImplementation(() => ({ maneuvers: [] }))
    )
  )

  ruleset.add(DisarmRule)
  ruleset.compileRules()

  const character = {} as Character

  each([
    [false, 'offensive', false],
    [false, 'defensive', false],
    [true, 'defensive', true],
    [false, 'offensive', true],
  ]).it(
    'should add disarm only if the character has the ability and the list is for defensive maneuvers',
    function (
      expected: boolean,
      maneuverType: ManeuverType,
      hasAbility: boolean
    ) {
      character.has = jest.fn()
      when(character.has).calledWith(Disarm).mockReturnValue(hasAbility)

      const result = ruleset.compute(ComputeManeuverList, {
        character,
        maneuverType,
      })

      expect(result.maneuvers.includes(DisarmManeuver)).toEqual(expected)
    }
  )
})
