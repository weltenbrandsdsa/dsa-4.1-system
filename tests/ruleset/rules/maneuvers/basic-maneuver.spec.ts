import {
  BasicManeuverRule,
  ComputeManeuverList,
} from '../../../../src/module/ruleset/rules/maneuvers/basic-maneuver'

import {
  AttackAction,
  CombatActionData,
  CombatActionResult,
  DodgeAction,
  ParryAction,
} from '../../../../src/module/ruleset/rules/basic-combat'
import type { Character } from '../../../../src/module/model/character'
import { TestAction } from '../../test-classes'
import each from 'jest-each'
import { createTestRuleset } from '../helpers'
import type { ActionIdentifier } from '../../../../src/module/ruleset/rule-components'
import { RangedAttackAction } from '../../../../src/module/ruleset/rules/basic-ranged-combat'
import type {
  ManeuverType,
  ModifierDescriptor,
} from '../../../../src/module/model/modifier'
import { Maneuver } from '../../../../src/module/character/maneuver'

describe('Basic Maneuver', function () {
  const ruleset = createTestRuleset()

  const executionResult = {} as any

  const executeHook = jest.fn().mockReturnValue(executionResult)

  ruleset.registerAction(new TestAction(AttackAction, executeHook))
  ruleset.registerAction(new TestAction(ParryAction, executeHook))
  ruleset.registerAction(new TestAction(DodgeAction, executeHook))
  ruleset.registerAction(new TestAction(RangedAttackAction, executeHook))

  ruleset.add(BasicManeuverRule)
  ruleset.compileRules()

  const character = {} as Character

  each([AttackAction, ParryAction]).it(
    'should add penality of failed maneuver to action result',
    async function (
      action: ActionIdentifier<CombatActionData, CombatActionResult>
    ) {
      const mightyStrike = new Maneuver('TestManeuver', 'offensive')
      mightyStrike.mod = 5
      const options = {
        character,
        modifiers: [mightyStrike],
        talent: undefined,
        weapon: undefined,
        mod: 0,
      }
      executionResult.options = options
      executionResult.success = false

      const expectedPenality = 5

      const result = await ruleset.execute(action, options)

      expect(result.penality).toEqual(expectedPenality)
    }
  )

  each([AttackAction, ParryAction, DodgeAction, RangedAttackAction]).it(
    'should add penality of failed maneuver to action result',
    async function (
      action: ActionIdentifier<CombatActionData, CombatActionResult>
    ) {
      const modifiers: ModifierDescriptor[] = [
        {
          name: 'TestManeuver',
          mod: 3,
          modifierType: 'maneuver',
        },

        {
          name: 'Test Modifier',
          mod: 4,
          modifierType: 'other',
        },
      ]
      const options = {
        character,
        modifiers,
        talent: undefined,
        weapon: undefined,
        mod: 0,
      }
      executionResult.options = options
      executionResult.success = false

      const expectedTotalMod = 7

      await ruleset.execute(action, options)

      expect(executeHook).toHaveBeenCalledWith(
        expect.objectContaining({
          mod: expectedTotalMod,
        })
      )
    }
  )

  each(['offensive', 'defensive']).it(
    'should be able to compute a list of available maneuvers',
    function (maneuverType: ManeuverType) {
      const result = ruleset.compute(ComputeManeuverList, {
        character,
        maneuverType,
      })
      expect(result.maneuvers).toEqual([])
    }
  )

  afterEach(() => {
    jest.clearAllMocks()
  })
})
