import { when } from 'jest-when'

import {
  HammerStrikeRule,
  HammerStrikeManeuver,
  HammerStrike,
} from '../../../../src/module/ruleset/rules/maneuvers/hammer-strike'

import type { Character } from '../../../../src/module/model/character'
import { Computation } from '../../../../src/module/ruleset/rule-components'
import { createTestRuleset } from '../helpers'
import { ComputeManeuverList } from '../../../../src/module/ruleset/rules/maneuvers/basic-maneuver'
import each from 'jest-each'
import type { ManeuverType } from '../../../../src/module/model/modifier'

describe('Hammer-Strike', function () {
  const ruleset = createTestRuleset()

  ruleset.registerComputation(
    new Computation(
      ComputeManeuverList,
      jest.fn().mockImplementation(() => ({ maneuvers: [] }))
    )
  )

  ruleset.add(HammerStrikeRule)
  ruleset.compileRules()

  const character = {} as Character

  it('should have a minimal modifier of 8', function () {
    expect(HammerStrikeManeuver.minMod).toEqual(8)
  })

  each([
    [false, 'offensive', false],
    [false, 'defensive', false],
    [false, 'defensive', true],
    [true, 'offensive', true],
  ]).it(
    'should add hammer strike only if the character has the ability and the list is for offensive maneuvers',
    function (
      expected: boolean,
      maneuverType: ManeuverType,
      hasAbility: boolean
    ) {
      character.has = jest.fn()
      when(character.has).calledWith(HammerStrike).mockReturnValue(hasAbility)

      const result = ruleset.compute(ComputeManeuverList, {
        character,
        maneuverType,
      })

      expect(result.maneuvers.includes(HammerStrikeManeuver)).toEqual(expected)
    }
  )
})
