import type { RuleConfigManager } from '../../../src/module/ruleset/rule'
import { Ruleset } from '../../../src/module/ruleset/ruleset'

export function createTestRuleset(): Ruleset {
  const configManager: jest.Mocked<RuleConfigManager> = {
    register: jest.fn(),
    get: jest.fn(),
  }
  configManager.get.mockReturnValue(true)
  const ruleset: Ruleset = new Ruleset(configManager)
  return ruleset
}

export function createMockRuleset(): jest.Mocked<Ruleset> {
  return ({
    execute: jest.fn(),
    compute: jest.fn(),
    registerAction: jest.fn(),
  } as unknown) as jest.Mocked<Ruleset>
}
