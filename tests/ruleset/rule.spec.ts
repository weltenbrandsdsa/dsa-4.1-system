import each from 'jest-each'
import { when } from 'jest-when'
import type { RuleDescriptor } from '../../src/module/ruleset/rule'
import { DescribeRule } from '../../src/module/ruleset/rule'
import { CreateRule } from '../../src/module/ruleset/rule'
import { Rule, RuleConfigManager } from '../../src/module/ruleset/rule'
import { CreateActionIdentifier } from '../../src/module/ruleset/rule-components'
import type { Ruleset } from '../../src/module/ruleset/ruleset'
import { createMockRuleset } from './rules/helpers'
import { TestAction } from './test-classes'

describe('Rule', function () {
  const ruleId = 'testRule'

  it('should be disabled by default', function () {
    const rule = new Rule(ruleId)
    expect(rule.isEnabled).toEqual(false)
  })

  each([true, false]).it(
    'can be enabled on load',
    function (isEnabled: boolean) {
      const rule = new Rule(ruleId, {
        enabled: isEnabled,
      })
      expect(rule.isEnabled).toEqual(isEnabled)
    }
  )

  each([true, false]).it(
    'should be describable and loadable into a ruleset if enabled',
    function (isEnabled: boolean) {
      const rule = new Rule(ruleId, {
        enabled: isEnabled,
      })
      const ruleset = createMockRuleset()

      rule.describe((ruleset: Ruleset) => {
        ruleset.registerAction(new TestAction(CreateActionIdentifier('test')))
      })

      rule.loadInto(ruleset)

      if (isEnabled) {
        expect(ruleset.registerAction).toHaveBeenCalled()
      } else {
        expect(ruleset.registerAction).toHaveBeenCalledTimes(0)
      }
    }
  )

  each([
    [true, true],
    [true, false],
    [false, true],
    [false, false],
  ]).it(
    'should be able to create a config in the config manager',
    function (isChangeable: boolean, isEnabled: boolean) {
      const systemId = 'dsa-4.1'
      const rule = new Rule(ruleId, {
        changeable: isChangeable,
        enabled: isEnabled,
      })
      const configManager = ({
        register: jest.fn(),
      } as unknown) as jest.Mocked<RuleConfigManager>
      rule.createConfig(configManager)

      expect(configManager.register).toHaveBeenCalledWith(
        systemId,
        ruleId,
        expect.objectContaining({
          name: `${systemId}.settings.${ruleId}`,
          hint: `${systemId}.settings.${ruleId}Hint`,
          scope: 'world',
          config: isChangeable,
          type: Boolean,
          default: isEnabled,
        })
      )
    }
  )

  each([true, false]).it(
    'should be able to load its config from the config manager',
    function (isEnabled) {
      const systemId = 'dsa-4.1'
      const rule = new Rule(ruleId)

      const configManager = ({
        get: jest.fn(),
      } as unknown) as jest.Mocked<RuleConfigManager>

      when(configManager.get)
        .calledWith(systemId, ruleId)
        .mockReturnValue(isEnabled)
      rule.loadConfig(configManager)
      expect(rule.isEnabled).toEqual(isEnabled)
    }
  )
})

describe('Rule Creator', function () {
  const ruleId = 'testRule'
  const systemId = 'dsa-4.1'

  it('should create a rule based on description', function () {
    const isChangeable = false
    const isEnabled = true
    const loaderCallback = jest.fn()
    const ruleDescription: RuleDescriptor = {
      ruleId,
      config: {
        enabled: isEnabled,
        changeable: isChangeable,
      },
      loader: (_ruleset: Ruleset) => {
        loaderCallback()
      },
    }
    const rule: Rule = CreateRule(ruleDescription)

    const configManager = ({
      register: jest.fn(),
    } as unknown) as jest.Mocked<RuleConfigManager>

    rule.createConfig(configManager)
    rule.loadInto({} as Ruleset)

    expect(configManager.register).toHaveBeenCalledWith(
      systemId,
      ruleId,
      expect.objectContaining({
        name: `${systemId}.settings.${ruleId}`,
        hint: `${systemId}.settings.${ruleId}Hint`,
        scope: 'world',
        config: isChangeable,
        type: Boolean,
        default: isEnabled,
      })
    )

    expect(loaderCallback).toHaveBeenCalled()
  })
})

describe('Rule Describer', function () {
  const ruleId = 'testRule'

  it('should create a rule description', function () {
    const ruleConfig = {
      enabled: true,
      changeable: false,
    }
    const loaderCallback = jest.fn()
    const ruleLoader = (_ruleset: Ruleset) => {
      loaderCallback()
    }
    const ruleDescription = DescribeRule(ruleId, ruleConfig, ruleLoader)

    expect(ruleDescription.ruleId).toEqual(ruleId)
    expect(ruleDescription.config).toEqual(ruleConfig)
    ruleDescription.loader({} as Ruleset)
    expect(loaderCallback).toHaveBeenCalled()
  })
})
