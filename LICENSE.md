Copyright © 2020 by Ulisses Spiele GmbH, Waldems. THE DARK EYE, AVENTURIA, DERE, MYRANOR, RIESLAND, THARUN, and UTHURIA are trademarks of Ulisses Spiele. All rights reserved.
This publication is protected under the copyright laws of the United States of America. No part of this publication may be reproduced, stored in a retrieval system, or transmitted, in any form or by any means, whether electronic,mechanical, photocopy, recording, or otherwise, without prior written consent by Ulisses Spiele GmbH, Waldems.

DAS SCHWARZE AUGE, AVENTURIEN, DERE, MYRANOR, RIESLAND, THARUN, UTHURIA und THE DARK EYE sind eingetragene Marken der Ulisses Spiele GmbH, Waldems.
Copyright © 2020 by Ulisses Spiele GmbH. Alle Rechte vorbehalten.
Titel und Inhalte dieses Werkes sind urheberrechtlich geschützt. Der Nachdruck, auch auszugsweise, die Bearbeitung, Verarbeitung, Verbreitung und Vervielfältigung des Werkes in jedweder Form, insbesondere die Vervielfältigung auf photomechanischem, elektronischem oder ähnlichem Weg, sind nur mit schriftlicher Genehmigung der Ulisses Spiele GmbH, Waldems, gestattet.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
