module.exports = {
  transform: {
    '^.+\\.svelte$': [
      'svelte-jester',
      {
        preprocess: true,
      },
    ],
    '^.+\\.ts$': 'ts-jest',
  },
  moduleFileExtensions: ['js', 'ts', 'svelte'],
  preset: 'ts-jest',
  setupFilesAfterEnv: ['jest-extended', '<rootDir>/jest-setup.ts'],
}
