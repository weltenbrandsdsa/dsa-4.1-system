import { getGame } from './module/utils'

export async function migrateSystem(): Promise<void> {
  const game = getGame()
  if (!game.user?.isGM) return
  await migrateSkillToSpecialAbility()
}

async function migrateSkillToSpecialAbility() {
  const game = getGame()
  if (game !== undefined) {
    if (game.items) {
      await migrateSkillToSpecialAbilityInContext(game.items)
    }
    for (const actor of game.actors ?? []) {
      await migrateSkillToSpecialAbilityInContext(actor.items, actor)
    }
    for (const scene of game.scenes || []) {
      for (const token of scene.data.tokens || []) {
        await migrateSkillToSpecialAbilityInContext(
          token.actor.items,
          token.actor
        )
      }
    }
  }
}

async function migrateSkillToSpecialAbilityInContext(items: any, parent?: any) {
  const createData =
    items
      ?.filter((item) => item.data.type === 'skill')
      .map((item) => {
        const newData = duplicate(item.data)
        delete newData._id
        newData.type = 'specialAbility'
        return newData as any
      }) || []
  const deleteIDs =
    items
      ?.filter((item) => item.data.type === 'skill')
      .map((item) => item.id || '') || []
  if (deleteIDs.length > 0 || createData.length > 0) {
    if (parent === undefined) {
      await Item.createDocuments(createData)
      await Item.deleteDocuments(deleteIDs)
    } else {
      parent.createEmbeddedDocuments('Item', createData)
      parent.deleteEmbeddedDocuments('Item', deleteIDs)
    }
  }
}
