import { DocumentModificationOptions } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/abstract/document.mjs'
import { CombatantDataProperties } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/combatantData'
import { BaseUser } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/documents.mjs'
import { PropertiesToSource } from '@league-of-foundry-developers/foundry-vtt-types/src/types/helperTypes'
import { InitiativeFormula } from './ruleset/rules/basic-combat'

declare global {
  interface DocumentClassConfig {
    Combatant: typeof DsaCombatant
  }

  interface FlagConfig {
    Combatant: {
      'dsa-4.1': {
        lastInitiative: number | null
      }
    }
  }
}

export class DsaCombatant extends Combatant {
  private initiativeFormula: InitiativeFormula | undefined

  async _preUpdate(
    changed: DeepPartial<PropertiesToSource<CombatantDataProperties>>,
    options: DocumentModificationOptions,
    user: BaseUser
  ): Promise<void> {
    this.updateInitiativeFormula()

    const initiative = hasProperty(changed, 'initiative')
      ? this.computeInitiative(changed.initiative || null)
      : this.initiative

    setProperty(changed, 'flags.dsa-4.1.lastInitiative', initiative)

    await super._preUpdate(changed, options, user)
  }

  _getInitiativeFormula(): string {
    this.updateInitiativeFormula()
    return this.initiativeFormula?.rollFormula || ''
  }

  private computeInitiative(modifier: number | null): number | null {
    if (this.initiativeFormula === undefined) {
      this.updateInitiativeFormula()
    }

    let result = modifier
      ? this.initiativeFormula?.initiative(modifier) || null
      : null

    if (result === null) {
      result = this.getFlag('dsa-4.1', 'lastInitiative') || null
    }

    return result
  }

  get initiative(): number | null {
    const modifier = Number.isNumeric(this.data.initiative)
      ? Number(this.data.initiative)
      : null

    return this.computeInitiative(modifier)
  }

  updateInitiativeFormula(): void {
    if (this.actor) {
      this.initiativeFormula = this.actor.initiative()
    }
  }
}

function updateCombat(combatant: DsaCombatant) {
  const combat = combatant.parent
  if (combat && combat.isOwner) {
    if (combat) {
      combat.update({ combatants: combat.setupTurns() })
    }
  }
}

Hooks.on('updateToken', (token, data) => {
  const initiativeRelevantData = [
    'actorData.data.base.combatAttributes.active.baseInitiative',
    'actorData.data.base.equiped',
    'actorData.effects',
  ]
  if (
    initiativeRelevantData.some((dataEntry: string) =>
      hasProperty(data, dataEntry)
    )
  ) {
    token.combatant.updateInitiativeFormula()
    updateCombat(token.combatant)
  }
})
