import type {
  Attribute,
  AttributeName,
  BaseProperty,
  Ability,
  BaseTalent,
  Spell,
} from '../model/properties'
import type { Armor, RollFormula, Shield, Weapon } from '../model/items'
import type {
  BaseCombatOptions,
  Character,
  MeleeCombatOptions,
  RangedCombatOptions,
} from '../model/character'
import { FoundryAttribute } from './attribute'
import type { Ruleset } from '../ruleset/ruleset'
import { FoundryCombatTalent } from './combat-talent'
import {
  ComputeArmorClass,
  ComputeAttack,
  ComputeDamageFormula,
  ComputeEffectiveEncumbarance,
  ComputeEncumbarance,
  ComputeParry,
  ComputeRangedAttack,
} from '../ruleset/rules/derived-combat-attributes'
import { FoundrySpell, FoundryTalent } from './skill'
import {
  AttackAction,
  CombatActionData,
  CombatActionResult,
  ComputeInitiativeFormula,
  DodgeAction,
  InitiativeFormula,
  ParryAction,
} from '../ruleset/rules/basic-combat'
import { RangedAttackAction } from '../ruleset/rules/basic-ranged-combat'
import { ComputeManeuverList } from '../ruleset/rules/maneuvers/basic-maneuver'
import type { ManeuverDescriptor, ManeuverType } from '../model/modifier'
import { getGame } from '../utils'
import { ActionIdentifier } from '../ruleset/rule-components'
import { DsaFourOneActor } from '../actor/actor'

export class FoundryCharacter implements Character {
  private ruleset: Ruleset
  actor: DsaFourOneActor

  constructor(actor: DsaFourOneActor, ruleset?: Ruleset) {
    if (ruleset) {
      this.ruleset = ruleset
    } else {
      this.ruleset = getGame().ruleset
    }
    this.actor = actor
  }

  get name(): string {
    return this.actor.data.name
  }

  attribute(name: AttributeName): Attribute {
    return new FoundryAttribute(this.actor, name, this.ruleset)
  }

  get baseAttack(): number {
    return this.actor.data.data.base.combatAttributes.active.baseAttack.value
  }

  get baseParry(): number {
    return this.actor.data.data.base.combatAttributes.active.baseParry.value
  }

  get baseRangedAttack(): number {
    return this.actor.data.data.base.combatAttributes.active.baseRangedAttack
      .value
  }

  get baseInitiative(): number {
    return this.actor.data.data.base.combatAttributes.active.baseInitiative
      .value
  }

  talent(sid: string): BaseTalent | undefined {
    const talent = [...this.actor.items.values()].find(
      (item) => item.data.data.sid === sid
    )
    // const talent = this.actor.items.find(
    //   (item) => item.data.data.sid === sid
    // )
    if (talent) {
      if (talent.data.type === 'combatTalent') {
        return new FoundryCombatTalent(talent, this, this.ruleset)
      }
      if (talent.data.type === 'talent') {
        return new FoundryTalent(talent, this, this.ruleset)
      }
    }
    return undefined
  }

  spell(sid: string): Spell | undefined {
    const spell = [...this.actor.items.values()].find(
      (item) => item.data.data.sid === sid
    )
    // const talent = this.actor.items.find(
    //   (item) => item.data.data.sid === sid
    // )
    if (spell) {
      return new FoundrySpell(spell, this, this.ruleset)
    }
    return undefined
  }

  attackValue(options: MeleeCombatOptions): number {
    return this.ruleset.compute(ComputeAttack, {
      character: this,
      ...options,
    }).value
  }

  parryValue(options: MeleeCombatOptions): number {
    return this.ruleset.compute(ComputeParry, {
      character: this,
      ...options,
    }).value
  }

  rangedAttackValue(options: RangedCombatOptions): number {
    return this.ruleset.compute(ComputeRangedAttack, {
      character: this,
      ...options,
    }).value
  }

  dodgeValue(): number {
    return this.actor.data.data.base.combatAttributes.active.dodge.value
  }

  get encumbarance(): number {
    return this.ruleset.compute(ComputeEncumbarance, {
      character: this,
    })
  }

  effectiveEncumbarance(formula: string): number {
    return this.ruleset.compute(ComputeEffectiveEncumbarance, {
      character: this,
      formula,
    })
  }

  get armorClass(): number {
    return this.ruleset.compute(ComputeArmorClass, {
      character: this,
    })
  }

  damage(weapon: Weapon): RollFormula {
    return this.ruleset.compute(ComputeDamageFormula, {
      character: this,
      weapon,
    }).formula
  }

  initiative(): InitiativeFormula {
    const combatState = this.actor.combatState
    const unarmedTalent = combatState.isArmed
      ? undefined
      : combatState.unarmedTalent
    const weapon = combatState.isArmed ? combatState.primaryHand : undefined
    const shield =
      combatState.isArmed && combatState.secondaryHand?.class === 'shield'
        ? combatState.secondaryHand
        : undefined
    return this.ruleset.compute(ComputeInitiativeFormula, {
      character: this,
      talent: unarmedTalent,
      weapon,
      shield,
    })
  }

  get abilities(): Ability[] {
    return [...this.actor.items.values()]
      .filter((item) => item.data.type === 'skill')
      .map((item) => ({
        name: item.name || '',
        identifier: `ability-${
          item.name?.toLowerCase().replace(' ', '') || ''
        }`,
      }))
  }

  ability(sid: string): Ability | undefined {
    const ability = [...this.actor.items.values()].find(
      (item) => item.data.data.sid === sid
    )
    // const talent = this.actor.items.find(
    //   (item) => item.data.data.sid === sid
    // )
    if (ability) {
      return {
        name: ability.data.name,
        identifier: ability.data.data.sid,
      }
    } else {
      return undefined
    }
  }

  has(property: BaseProperty): boolean {
    return [...this.actor.items.values()].some(
      (item) => item.data.data.sid === property.identifier
    )
  }

  private baseCombatAction<CombatOptions extends BaseCombatOptions>(
    action: 'attack' | 'parry' | 'ranged' | 'dodge',
    options?: CombatOptions
  ): void {
    const actionIdentifier: ActionIdentifier<
      CombatActionData,
      CombatActionResult
    > = {
      attack: AttackAction,
      parry: ParryAction,
      ranged: RangedAttackAction,
      dodge: DodgeAction,
    }[action]

    this.ruleset.execute(actionIdentifier, {
      character: this,
      ...options,
    })
  }

  attack(options: MeleeCombatOptions): void {
    this.baseCombatAction('attack', options)
  }

  rangedAttack(options?: RangedCombatOptions): void {
    this.baseCombatAction('ranged', options)
  }

  parry(options?: MeleeCombatOptions): void {
    this.baseCombatAction('parry', options)
  }

  dodge(options?: BaseCombatOptions): void {
    this.baseCombatAction('dodge', options)
  }

  maneuverList(
    maneuverType: ManeuverType,
    isArmed?: boolean
  ): ManeuverDescriptor[] {
    return this.ruleset.compute(ComputeManeuverList, {
      character: this,
      maneuverType,
      isArmed,
    }).maneuvers
  }

  get armorItems(): Armor[] {
    const armorItemsData = [...this.actor.items.values()].filter(
      (item) => item.data.type === 'armor' && item.data.data.equiped
    )
    return armorItemsData.map((item) => ({
      name: item.data.name,
      armorClass: item.data.data.armorClass,
      encumbarance: item.data.data.encumbarance,
    }))
  }
}
