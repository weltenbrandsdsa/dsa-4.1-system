import type { Shield, WeaponMod } from '../model/items'

export class FoundryShield implements Shield {
  class: 'shield' = 'shield'

  private item: Item
  constructor(item: Item) {
    this.item = item
  }

  get name(): string {
    return this.item.data.name
  }

  get initiativeMod(): number {
    return this.item.data.data.initiativeMod
  }

  get weaponMod(): WeaponMod {
    return {
      attack: this.item.data.data.weaponMod.attack,
      parry: this.item.data.data.weaponMod.parry,
    }
  }
}
