import type { ManeuverType, ModifierType } from '../model/modifier'
import type { ManeuverDescriptor } from '../model/modifier'

interface ManeuverOptions {
  minMod?: number
}

export class Maneuver implements ManeuverDescriptor {
  public name: string
  public type: ManeuverType
  public modifierType: ModifierType = 'maneuver'
  public mod: number
  public readonly minMod: number

  constructor(name: string, type: ManeuverType, options: ManeuverOptions = {}) {
    this.name = name
    this.type = type
    this.mod = 1
    this.minMod = options.minMod || 1
  }
}
