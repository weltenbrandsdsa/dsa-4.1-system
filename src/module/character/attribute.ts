import type { Attribute, AttributeName } from '../model/properties'
import { RollAttribute } from '../ruleset/rules/basic-roll-mechanic'
import type { Ruleset } from '../ruleset/ruleset'

export class FoundryAttribute implements Attribute {
  private actor: Actor
  private ruleset: Ruleset
  name: AttributeName

  constructor(actor: Actor, name: AttributeName, ruleset?: Ruleset) {
    this.actor = actor
    this.name = name
    if (ruleset) {
      this.ruleset = ruleset
    } else {
      this.ruleset = (<any>game).ruleset
    }
  }

  get key(): string {
    return `data.base.basicAttributes.${this.name}.value`
  }

  get value(): number {
    return this.actor.data.data.base.basicAttributes[this.name].value
  }

  roll(): void {
    this.ruleset.execute(RollAttribute, {
      attributeName: this.name,
      targetValue: this.value,
    })
  }
}
