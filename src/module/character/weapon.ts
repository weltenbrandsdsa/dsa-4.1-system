import type {
  RollFormula,
  StrengthMod,
  MeleeWeapon,
  WeaponMod,
  Weapon,
  WeaponType,
  RangedWeapon,
} from '../model/items'

export abstract class FoundryWeapon implements Weapon {
  class: 'weapon' = 'weapon'

  protected item: Item
  public abstract type: WeaponType

  constructor(item: Item) {
    this.item = item
  }

  get name(): string {
    return this.item.data.name
  }

  get talent(): string {
    return this.item.data.data.talent
  }

  get damage(): RollFormula {
    return this.item.data.data.damage
  }

  static create(item: Item): Weapon | undefined {
    switch (item.type) {
      case 'meleeWeapon':
        return new FoundryMeleeWeapon(item)
      case 'rangedWeapon':
        return new FoundryRangedWeapon(item)
    }
  }
}

export class FoundryMeleeWeapon extends FoundryWeapon implements MeleeWeapon {
  public type: 'melee' = 'melee'

  get initiativeMod(): number {
    return this.item.data.data.initiativeMod
  }

  get weaponMod(): WeaponMod {
    return this.item.data.data.weaponMod
  }

  get strengthMod(): StrengthMod {
    return this.item.data.data.strengthMod
  }
}

export class FoundryRangedWeapon extends FoundryWeapon implements RangedWeapon {
  public type: 'ranged' = 'ranged'
}
