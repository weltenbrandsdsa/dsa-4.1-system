import type { Character } from '../model/character'
import type { CombatTalent } from '../model/properties'
import type { ComputationIdentifier } from '../ruleset/rule-components'
import {
  CombatComputationData,
  CombatComputationResult,
  ComputeAttack,
  ComputeParry,
  ComputeRangedAttack,
} from '../ruleset/rules/derived-combat-attributes'
import type { Ruleset } from '../ruleset/ruleset'
import { getGame } from '../utils'

export class FoundryCombatTalent implements CombatTalent {
  private item: Item
  private ruleset: Ruleset
  private character: Character

  constructor(item: Item, character: Character, ruleset?: Ruleset) {
    this.item = item
    if (ruleset) {
      this.ruleset = ruleset
    } else {
      this.ruleset = getGame().ruleset
    }
    this.character = character
  }

  get name(): string {
    return this.item.data.name
  }

  get identifier(): string {
    return this.item.data.data.sid
  }

  get value(): number {
    return this.item.data.data.value
  }

  private computeValue(
    identifier: ComputationIdentifier<
      CombatComputationData,
      CombatComputationResult
    >
  ): number {
    return this.ruleset.compute(identifier, {
      character: this.character,
      talent: this,
    }).value
  }

  get attack(): number {
    return this.computeValue(ComputeAttack)
  }

  get parry(): number {
    return this.computeValue(ComputeParry)
  }

  get rangedAttack(): number {
    return this.computeValue(ComputeRangedAttack)
  }

  get attackMod(): number {
    return this.item.data.data.combat.attack
  }

  get parryMod(): number {
    return this.item.data.data.combat.parry
  }

  get rangedAttackMod(): number {
    return this.item.data.data.combat.rangedAttack
  }

  get isUnarmed(): boolean {
    return ['talent-raufen', 'talent-ringen'].includes(this.item.data.data.sid)
  }
}
