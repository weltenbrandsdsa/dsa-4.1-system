import type { Character } from '../model/character'
import type {
  EffectiveEncumbarance,
  Skill,
  SkillDescriptor,
  SkillType,
  Spell,
  Talent,
  TestAttributes,
} from '../model/properties'
import type { ActionIdentifier } from '../ruleset/rule-components'
import {
  SkillActionData,
  SkillActionResult,
  SpellAction,
  TalentAction,
} from '../ruleset/rules/basic-skill'
import type { Ruleset } from '../ruleset/ruleset'
import { getGame } from '../utils'

interface SkillRollOptions {
  mod?: number
}

export abstract class FoundrySkill implements Skill {
  protected item: Item
  private ruleset: Ruleset
  private character: Character
  public skillType: SkillType
  protected abstract skillAction: ActionIdentifier<
    SkillActionData,
    SkillActionResult
  >

  constructor(item: Item, character: Character, ruleset?: Ruleset) {
    this.item = item
    if (ruleset) {
      this.ruleset = ruleset
    } else {
      this.ruleset = getGame().ruleset
    }

    this.character = character
  }

  get name(): string {
    return this.item.data.name
  }

  get identifier(): string {
    return this.item.data.data.sid
  }

  get value(): number {
    return this.item.data.data.value
  }

  get testAttributes(): TestAttributes {
    return [
      this.item.data.data.test.firstAttribute,
      this.item.data.data.test.secondAttribute,
      this.item.data.data.test.thirdAttribute,
    ]
  }

  private get skillDescriptor(): SkillDescriptor {
    return {
      name: this.name,
      identifier: this.identifier,
      skillType: this.skillType,
    }
  }

  roll(options: SkillRollOptions): void {
    this.ruleset.execute(this.skillAction, {
      character: this.character,
      ...options,
      skill: this.skillDescriptor,
    })
  }

  static create(
    item: Item,
    character: Character,
    ruleset?: Ruleset
  ): Skill | undefined {
    switch (item.data.type) {
      case 'spell':
        return new FoundrySpell(item, character, ruleset)
      case 'talent':
        return new FoundryTalent(item, character, ruleset)
    }
  }
}

export function isTalent(skill: Skill): skill is Talent {
  return skill.skillType === 'talent'
}

export class FoundryTalent extends FoundrySkill implements Talent {
  public skillType: 'talent' = 'talent'
  protected skillAction: ActionIdentifier<SkillActionData, SkillActionResult> =
    TalentAction

  constructor(item: Item, character: Character, ruleset?: Ruleset) {
    super(item, character, ruleset)
  }

  get effectiveEncumbarance(): EffectiveEncumbarance {
    return this.item.data.data.effectiveEncumbarance
  }
}

export class FoundrySpell extends FoundrySkill implements Spell {
  public skillType: 'spell' = 'spell'
  protected skillAction: ActionIdentifier<SkillActionData, SkillActionResult> =
    SpellAction

  constructor(item: Item, character: Character, ruleset?: Ruleset) {
    super(item, character, ruleset)
  }
}
