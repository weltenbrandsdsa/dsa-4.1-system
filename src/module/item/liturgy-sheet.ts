import {
  LiturgyCastType,
  LiturgyTargetClass,
  LiturgyType,
  RuleBook,
} from '../enums'
import { labeledEnumValues } from '../i18n'
import { createOpenPDFPageListener } from '../pdfoundry-integration'
import { showOptionsDialog } from './sheet-helper'
import { ItemWithVariantsSheet } from './item-with-variants-sheet'

export class LiturgySheet extends ItemWithVariantsSheet {
  static get variantCompendium() {
    return 'world.liturgyvariants'
  }

  static get variantItemType() {
    return 'liturgyVariant'
  }

  static get variantHTMLSelector() {
    return '.liturgy-variants'
  }

  activateListeners(html) {
    super.activateListeners(html)

    html.find('.target-classes-list .item-create').click(() => {
      showOptionsDialog(LiturgyTargetClass, this.item, 'targetClasses')
    })

    html
      .find('.open-ll-page')
      .click(createOpenPDFPageListener(RuleBook.LiberLiturgium))
  }

  async getData() {
    const data = await super.getData()

    data.data.targetClasses = labeledEnumValues(LiturgyTargetClass).filter(
      (target_class) => data.data.targetClasses.includes(target_class.value)
    )

    data.data.types = labeledEnumValues(LiturgyType)
    data.data.castTypes = labeledEnumValues(LiturgyCastType)

    return data
  }
}

ItemWithVariantsSheet.registerSheet('liturgie', LiturgySheet)
