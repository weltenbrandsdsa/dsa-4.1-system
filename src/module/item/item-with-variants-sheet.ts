import { getGame } from '../utils'
import { DsaItemSheet } from './item-sheet'

export class ItemWithVariantsSheet extends DsaItemSheet {
  ['constructor']: typeof ItemWithVariantsSheet

  static _registeredSheets = {}

  static registerSheet(itemType, sheetCls) {
    this._registeredSheets[itemType] = sheetCls
  }

  static sheetByType(itemType) {
    return this._registeredSheets[itemType]
  }

  static get variantCompendium(): string {
    throw 'Not implemented!'
  }

  static get variantItemType(): string {
    throw 'Not implemented!'
  }

  static get variantHTMLSelector(): string {
    throw 'Not implemented!'
  }

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      dragDrop: [{ dragSelector: '.item-list .item', dropSelector: null }],
    })
  }

  activateListeners(html) {
    super.activateListeners(html)

    html
      .find(`${this.constructor.variantHTMLSelector} .item-create`)
      .click(async () => {
        const data = {
          data: {
            name: 'New Variant',
            type: this.constructor.variantItemType,
            img: 'icons/svg/mystery-man.svg',
          },
        }
        const variant = await this._importOrCreate(data)
        addVariant(this.item, variant)
        if (variant.sheet) {
          variant.sheet.render(true)
        }
      })

    html.find('.item-edit').click(async (event) => {
      const itemName = event.currentTarget.name
      const item = this.item.isOwned
        ? this.item.actor?.items.get(itemName)
        : await findVariant(itemName, this.constructor.variantCompendium)

      item?.sheet?.render(true)
    })
  }

  async _importOrCreate(data) {
    let item
    const game = getGame()

    if (this.item.isOwned) {
      item = await this.item.actor?.importItem(data)
    } else {
      if (data.data) {
        const pack = game.packs.get(this.constructor.variantCompendium)
        if (!pack) {
          ui.notifications?.error(
            'Compendium ' +
              this.constructor.variantCompendium +
              ' does not exist!'
          )
          return
        }
        item = await pack.createEntity(data.data, {})
      } else {
        item = game.items?.get(data.id)
      }
    }

    return item
  }

  async _onDrop(event) {
    let data

    try {
      data = JSON.parse(event.dataTransfer.getData('text/plain'))
      if (data.type !== 'Item') return
    } catch (err) {
      return false
    }

    return this._importOrCreate(data)
      .then((item) => addVariant(this.item, item))
      .then(() => this.render(true))
      .catch((err) => console.error(err.message))
  }

  async getData() {
    const data = await super.getData()

    data.data.variant_data = await this.getVariantsData(data.data.variants)

    return data
  }

  async getVariantsData(variants) {
    const variantsData = await Promise.all(
      variants.map(async (variant) => [
        variant,
        this.item.isOwned
          ? this.item.actor?.items.get(variant)
          : await findVariant(variant, this.constructor.variantCompendium),
      ])
    )

    return Object.fromEntries(variantsData as any)
  }
}

async function findVariant(variant, variantCompendium) {
  const game = getGame()
  const globalItem = game.items?.get(variant)

  if (!globalItem) {
    const pack = game.packs.get(variantCompendium)
    return await pack?.get(variant)
  }

  return globalItem
}

Hooks.on('createOwnedItem', async (actor, itemData) => {
  if (itemData.data.variants) {
    const variants = itemData.data.variants
    const ownedVariants: any[] = []

    for (const variant of variants) {
      const sheetCls = ItemWithVariantsSheet.sheetByType(itemData.type)
      const item = await findVariant(variant, sheetCls.variantCompendium)
      if (item) {
        const ownedItem = await actor.createOwnedItem(item.data)
        ownedVariants.push(ownedItem.id)
      }
    }

    const item = actor.items.get(itemData.id)
    item.update({ 'data.variants': ownedVariants })
  }
})

function addVariant(item, variant) {
  return item.update({
    'data.variants': [...new Set(item.data.data.variants).add(variant.id)],
  })
}
