import { getGame } from '../utils'

Hooks.on('createActor', async (actor, options) => {
  if (actor.data.type === 'character' && options.renderSheet) {
    const game = getGame()
    for (const packName of ['talent', 'combattalent']) {
      const pack = await game.packs?.get(`dsa-4.1.${packName}`)
      const talentIndex = await pack?.getDocuments()
      if (talentIndex) {
        const baseTalents = talentIndex.filter(
          (item) => item.data.data.type == 'basic'
        )
        const test = baseTalents.map((item) => {
          delete item.data.id
          return item.data
        })
        await actor.createEmbeddedDocuments('Item', test)
      }
    }
  }
})
