import { DsaItemSheet } from './item-sheet'
import {
  TalentType,
  TalentCategory,
  Attribute,
  CombatTalentCategory,
  EffectiveEncumbaranceType,
} from '../enums'
import { labeledEnumValues } from '../i18n'

export class TalentSheet extends DsaItemSheet {
  async getData() {
    const data = await super.getData()

    data.data.talentTypes = labeledEnumValues(TalentType)
    data.data.talentCategories = labeledEnumValues(TalentCategory).filter(
      (category) =>
        category.value != TalentCategory.Combat &&
        category.value != TalentCategory.Language
    )

    data.data.effectiveEncumbaranceTypes = labeledEnumValues(
      EffectiveEncumbaranceType
    )

    return data
  }
}

export class LinguisticTalentSheet extends DsaItemSheet {
  get template() {
    return 'systems/dsa-4.1/templates/items/linguisticTalent-sheet.html'
  }
}

function enrichLinguisticItem(document) {
  if (document.type === 'language' || document.type === 'scripture') {
    const updateData = {
      data: {
        type: TalentType.Special,
        category: TalentCategory.Language,
        test: {},
      },
    }

    switch (document.type) {
      case 'language':
        updateData.data.test = {
          firstAttribute: Attribute.Cleverness,
          secondAttribute: Attribute.Intuition,
          thirdAttribute: Attribute.Charisma,
        }
        break
      case 'scripture':
        updateData.data.test = {
          firstAttribute: Attribute.Cleverness,
          secondAttribute: Attribute.Cleverness,
          thirdAttribute: Attribute.Dexterity,
        }
        break
    }
    document.data.update(updateData)
  }
}

Hooks.on('preCreateItem', (document) => {
  enrichLinguisticItem(document)
})

Hooks.on('preCreateOwnedItem', (actor, document) => {
  enrichLinguisticItem(document)
})

export class CombatTalentSheet extends TalentSheet {
  async getData() {
    const data = await super.getData()
    data.data.combatTalentCategories = labeledEnumValues(CombatTalentCategory)

    return data
  }
}
