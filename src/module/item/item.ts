Hooks.on('createItem', async (item) => {
  if (item.data.data.sid === '') {
    const itemData = item.data
    await itemData.update({
      data: {
        sid: item.id,
      },
    })
  }
})

Hooks.on('preCreateOwnedItem', (actor, itemData) => {
  if (itemData.data?.isUniquelyOwnable && itemData.data?.sid != '') {
    const isAlreadyOwned = [...actor.items.values()].some(
      (item) => item.data.data.sid === itemData.data.sid
    )
    if (isAlreadyOwned) {
      ui.notifications?.info('Item is already equiped!', {})
    }
    return !isAlreadyOwned
  }
})

export {}
