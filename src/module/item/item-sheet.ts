import { Attribute } from '../enums'
import { labeledEnumValues } from '../i18n'
import { getGame } from '../utils'

interface DsaItemDataSourceData {
  /* properties defined in the template.json for that particular type */
  [key: string]: any
}

interface DsaItemDataSource {
  type: any
  data: DsaItemDataSourceData
}

interface DsaItemDataProperties extends DsaItemDataSource {
  attributes: any
  [key: string]: any
}

type ItemDataSource = DsaItemDataSource
type ItemDataProperties = DsaItemDataProperties

declare global {
  interface SourceConfig {
    Item: ItemDataSource
  }

  interface DataConfig {
    Item: ItemDataProperties
  }
}

// interface DsaItemSheetData extends ItemSheet.Data<ItemSheet.Options> {
// }

export class DsaItemSheet extends ItemSheet<
  ItemSheet.Options,
  // DsaItemSheetData
  any
> {
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      width: 'auto',
      height: 'auto',
      classes: ['dsa-4.1', 'sheet', 'item'],
      resizable: true,
    })
  }

  activateListeners(html) {
    super.activateListeners(html)

    html.find('.item-delete').click(() => {
      if (this.item.actor && this.item.id) {
        this.item.actor.deleteEmbeddedDocuments('Item', [this.item.id])
        this.close()
      }
    })
  }

  get template() {
    return `systems/dsa-4.1/templates/items/${this.item.data.type}-sheet.html`
  }

  async getData(options?: Application.RenderOptions) {
    let data = (await super.getData()) as any

    const attributes = labeledEnumValues(Attribute)

    const game = getGame()
    const CombatTalentCompendium = game.settings.get(
      'dsa-4.1',
      'combat-talent-pack'
    )
    const combatTalentPack = await game.packs
      .get(CombatTalentCompendium)
      ?.getDocuments()

    const combatTalents = combatTalentPack?.map((talent) => ({
      name: talent.data.name,
      sid: talent.data.data.sid,
      type: talent.data.data.combat.category,
    }))

    data = {
      ...data.data,
      item: data.item,
      attributes,
    }

    data.meleeCombatTalents = combatTalents?.filter(
      (talent) => talent.type === 'melee'
    )
    data.rangedCombatTalents = combatTalents?.filter(
      (talent) => talent.type === 'ranged'
    )

    return data
  }
}
