import { ItemWithVariantsSheet } from './item-with-variants-sheet'
import { SpellModification, SpellTargetClass, RuleBook } from '../enums'
import { labeledEnumValues } from '../i18n'
import { createOpenPDFPageListener } from '../pdfoundry-integration'
import { showOptionsDialog } from './sheet-helper'

export class SpellSheet extends ItemWithVariantsSheet {
  static get variantCompendium() {
    return 'world.spellvariants'
  }

  static get variantItemType() {
    return 'spellVariant'
  }

  static get variantHTMLSelector() {
    return '.spell-variants'
  }

  activateListeners(html) {
    super.activateListeners(html)

    html.find('.modifications-list .item-create').click(() => {
      showOptionsDialog(SpellModification, this.item, 'modifications')
    })

    html.find('.target-classes-list .item-create').click(() => {
      showOptionsDialog(SpellTargetClass, this.item, 'targetClasses')
    })

    html
      .find('.open-lcd-page')
      .click(createOpenPDFPageListener(RuleBook.LiberCantionesDeluxe))
  }

  async getData() {
    const data = await super.getData()

    data.data.modifications = labeledEnumValues(SpellModification).filter(
      (mod) => data.data.modifications.includes(mod.value)
    )

    data.data.targetClasses = labeledEnumValues(SpellTargetClass).filter(
      (target_class) => data.data.targetClasses.includes(target_class.value)
    )

    return data
  }
}

ItemWithVariantsSheet.registerSheet('spell', SpellSheet)
