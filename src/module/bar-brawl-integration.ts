import { getGame } from './utils'

Hooks.on('preCreateToken', function (doc, data) {
  const game = getGame()
  if (game.actors) {
    const actor = game.actors.get(data.actorId)
    const hasAstralEnergy = actor?.data.data.base.resources.astralEnergy.max > 0
    const hasKarmicEnergy = actor?.data.data.base.resources.karmicEnergy.max > 0
    const resourceBars = {
      bar1: {
        id: 'bar1',
        mincolor: '#110000',
        maxcolor: '#FF0000',
        position: 'bottom-inner',
        attribute: 'base.resources.vitality',
        visibility: CONST.TOKEN_DISPLAY_MODES.OWNER,
      },
      enduranceBar: {
        id: 'enduranceBar',
        mincolor: '#111100',
        maxcolor: '#FFFF00',
        position: 'bottom-inner',
        attribute: 'base.resources.endurance',
        visibility: CONST.TOKEN_DISPLAY_MODES.OWNER,
      },
      bar2: {},
      karmaBar: {},
    }
    if (hasAstralEnergy) {
      resourceBars.bar2 = {
        id: 'bar2',
        mincolor: '#000011',
        maxcolor: '#0000FF',
        position: 'bottom-inner',
        attribute: 'base.resources.astralEnergy',
        visibility: CONST.TOKEN_DISPLAY_MODES.OWNER,
      }
    }
    if (hasKarmicEnergy) {
      resourceBars.karmaBar = {
        id: 'karmaBar',
        mincolor: '#001100',
        maxcolor: '#00FF00',
        position: 'bottom-inner',
        attribute: 'base.resources.karmicEnergy',
        visibility: CONST.TOKEN_DISPLAY_MODES.OWNER,
      }
    }
    doc.data.update({ 'flags.barbrawl.resourceBars': resourceBars })
  }
})
