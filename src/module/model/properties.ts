export const attributeNames = [
  'courage',
  'cleverness',
  'dexterity',
  'intuition',
  'agility',
  'charisma',
  'constitution',
  'strength',
] as const

export type AttributeName = typeof attributeNames[number]

export interface Attribute {
  value: number
  name: AttributeName
  roll(): void
  key: string
}

export interface BaseProperty {
  name: string
  identifier: string
}

export type TestAttributes = [AttributeName, AttributeName, AttributeName?]

export interface BaseTalent extends BaseProperty {
  value: number
}

export type SkillType = 'talent' | 'spell'

export type SkillDescriptor = BaseProperty & { skillType: SkillType }

export interface Skill extends BaseTalent {
  testAttributes: TestAttributes
  roll(options: any): void
  skillType: SkillType
}

type EffectiveEncumbaranceType = 'none' | 'special' | 'formula'

export interface EffectiveEncumbarance {
  type: EffectiveEncumbaranceType
  formula?: string
}

export interface Talent extends Skill {
  skillType: 'talent'
  effectiveEncumbarance: EffectiveEncumbarance
}

export interface Spell extends Skill {
  skillType: 'spell'
}

export interface CombatTalent extends BaseTalent {
  isUnarmed: boolean
  attack: number
  parry: number
  rangedAttack: number
  attackMod: number
  parryMod: number
  rangedAttackMod: number
}

export interface Ability extends BaseProperty {
  value?: string | number
}
