import type {
  Attribute,
  AttributeName,
  BaseProperty,
  Ability,
  Spell,
  CombatTalent,
} from './properties'
import type { BaseTalent } from './properties'
import type { Weapon, RollFormula, Shield, Armor } from './items'
import type {
  ManeuverDescriptor,
  ManeuverType,
  ModifierDescriptor,
} from './modifier'
import type {
  RangeClass,
  SizeClass,
} from '../ruleset/rules/derived-combat-attributes'
import { InitiativeFormula } from '../ruleset/rules/basic-combat'

export interface BaseCombatOptions {
  modifiers?: ModifierDescriptor[]
}

type Armed<CombatOptions extends BaseCombatOptions> = CombatOptions & {
  weapon: Weapon
}

type Unarmed<CombatOptions extends BaseCombatOptions> = CombatOptions & {
  talent: CombatTalent
}

interface ArmedMeleeCombatOptions extends Armed<BaseCombatOptions> {
  shield?: Shield
}

export type MeleeCombatOptions =
  | ArmedMeleeCombatOptions
  | Unarmed<BaseCombatOptions>

export interface RangedCombatOptions extends Armed<BaseCombatOptions> {
  sizeClass?: SizeClass
  rangeClass?: RangeClass
}

export interface CombatState {
  isArmed: boolean
  primaryHand: Weapon | undefined
  secondaryHand: Weapon | Shield | undefined
  unarmedTalent: CombatTalent | undefined
}

export interface Character {
  name: string
  attribute(name: AttributeName): Attribute
  baseAttack: number
  baseParry: number
  baseRangedAttack: number
  baseInitiative: number
  talent(sid: string): BaseTalent | undefined
  spell(sid: string): Spell | undefined
  attackValue(options: MeleeCombatOptions): number
  parryValue(options: MeleeCombatOptions): number
  rangedAttackValue(options: RangedCombatOptions): number
  dodgeValue(options: any): number
  damage(weapon: Weapon): RollFormula
  initiative(): InitiativeFormula
  abilities: Ability[]
  ability(sid: string): Ability | undefined
  has(property: BaseProperty): boolean
  attack(options: MeleeCombatOptions): void
  rangedAttack(options: RangedCombatOptions): void
  parry(options: MeleeCombatOptions): void
  dodge(options?: BaseCombatOptions): void
  maneuverList(
    maneuverType: ManeuverType,
    armed?: boolean
  ): ManeuverDescriptor[]
  armorItems: Armor[]
  armorClass: number
  encumbarance: number
  effectiveEncumbarance(formula: string): number
}
