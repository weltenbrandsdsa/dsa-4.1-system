export type ModifierType = 'maneuver' | 'other'

export interface ModifierDescriptor {
  name: string
  mod: number
  modifierType: ModifierType
}

export type ManeuverType = 'offensive' | 'defensive'

export interface ManeuverDescriptor extends ModifierDescriptor {
  type: ManeuverType
  minMod: number
}
