import App from '../../ui/ActorSheet.svelte'
import { FoundryCharacter } from '../character/character'

export class DsaSvelteActorSheet extends ActorSheet {
  component: App

  get template(): string {
    return 'systems/dsa-4.1/templates/actor-sheet-svelte.html'
  }

  async _renderInner(data): Promise<JQuery> {
    const inner = await super._renderInner(data)
    this.component = new App({
      target: $(inner).get(0),
      props: {
        character: new FoundryCharacter(this.actor),
        foundryApp: this,
        name: 'Test',
      },
    })
    return inner
  }
}
