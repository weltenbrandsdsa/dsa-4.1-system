import {
  RangeClass,
  SizeClass,
} from '../ruleset/rules/derived-combat-attributes'

import { labeledEnumValues } from '../i18n'
import { createOpenPDFPageListener } from '../pdfoundry-integration'
import { RuleBook } from '../enums'

import { ManeuverEditor } from '../maneuver-editor'
import { SkillRollDialog } from '../skill-roll-dialog'
import { FoundryCharacter } from '../character/character'
import { getGame } from '../utils'
import type { ManeuverDescriptor } from '../model/modifier'
import type { DsaFourOneActor } from './actor'
import { FoundrySkill } from '../character/skill'

interface RangedAttackConfig {
  modifier: number
  rangeClass?: string | number | string[]
  sizeClass?: string | number | string[]
}

export class DsaFourOneActorSheet extends ActorSheet {
  private rangedAttackConfig: RangedAttackConfig

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ['dsa-4.1', 'sheet', 'actor'],
      template: 'systems/dsa-4.1/templates/actor-sheet.html',
      width: 950,
      tabs: [
        {
          navSelector: '.sheet-tabs',
          contentSelector: '.sheet-body',
          initial: 'character',
        },
        {
          navSelector: '.talents-tabs',
          contentSelector: '.talents-body',
          initial: 'talents-combat',
        },
        {
          navSelector: '.inventory-tabs',
          contentSelector: '.inventory-body',
          initial: 'inventory-generic-item',
        },
      ],
    })
  }

  async getData() {
    let data: any = await super.getData()
    data = {
      ...data.data,
      actor: data.actor,
    }

    const itemTypes = [
      'talent',
      'combatTalent',
      'language',
      'scripture',
      'spell',
      'spellVariant',
      'liturgy',
      'advantage',
      'disadvantage',
      'specialAbility',
      'meleeWeapon',
      'rangedWeapon',
      'armor',
      'shield',
      'genericItem',
    ]

    data.data.owned = {}

    itemTypes.forEach((type) => {
      data.data.owned[`${type}s`] = data.items.filter(
        (item) => item.type === type
      )
    })

    data.data.owned.allMeleeWeapons = data.data.owned.meleeWeapons
    data.data.owned.meleeWeapons = data.data.owned.meleeWeapons.filter(
      (weapon) => this.actor.isEquipable(weapon)
    )
    for (const weapon of data.data.owned.meleeWeapons) {
      weapon.attack = this.actor.attack(weapon)
      weapon.parry = this.actor.parry(weapon)
      weapon.initiative = this.actor.initiative().formula
      weapon.damage = this.actor.damage(weapon)
    }

    data.data.owned.allRangedWeapons = data.data.owned.rangedWeapons
    data.data.owned.rangedWeapons = data.data.owned.rangedWeapons.filter(
      (weapon) => this.actor.isEquipable(weapon)
    )
    for (const rangedWeapon of data.data.owned.rangedWeapons) {
      rangedWeapon.rangedAttack = this.actor.rangedAttack(rangedWeapon)
      rangedWeapon.initiative = this.actor.initiative().formula
      rangedWeapon.damage = this.actor.damage(rangedWeapon)
    }

    data.data.owned.unarmedTalents = data.data.owned.combatTalents.filter(
      (talent) => talent.data.combat.category === 'unarmed'
    )

    for (const talent of data.data.owned.unarmedTalents) {
      talent.attack = this.actor.attack(talent)
      talent.parry = this.actor.parry(talent)
      talent.initiative = this.actor.initiative().formula
      talent.damage = this.actor.damage(talent)
    }

    data.armorClass = this.actor.getArmorClass()
    data.encumbarance = this.actor.getEncumbarance()

    data.itemsById = {}
    data.items.forEach((item) => {
      data.itemsById[item._id] = item
    })

    data.rangeClasses = labeledEnumValues(RangeClass, true)
    data.sizeClasses = labeledEnumValues(SizeClass, true)
    data.rangedAttackConfig = this.rangedAttackConfig
      ? this.rangedAttackConfig
      : {
          modifier: 0,
          rangeClass: RangeClass.Near,
          sizeClass: SizeClass.Big,
        }

    data.isArmed = data.data.base.combatState.isArmed

    return data
  }

  /** @override */
  async _updateObject(event, formData): Promise<undefined | DsaFourOneActor> {
    await super._updateObject(event, formData)

    let items = expandObject(formData).data.owned || []
    items = Object.entries(items).map(([id, data]) => ({
      _id: id,
      data: (data as any).data,
    }))
    await this.actor.updateEmbeddedDocuments('Item', items)

    return
  }

  _switchArmedEventListener(_event) {
    const isArmed = !this.actor.data.data.base.combatState.isArmed
    this.actor.update({
      data: { base: { combatState: { isArmed } } },
    })
    this.render(true)
  }

  activateListeners(html) {
    super.activateListeners(html)

    html.find('.switch-armed a').click((e) => this._switchArmedEventListener(e))

    html
      .find('.attribute-actions .item-roll')
      .click(createAttributeRollEventListener(this.actor, false))

    html
      .find('.attribute-label a')
      .click(createAttributeRollEventListener(this.actor, false))

    html
      .find('.trait-label a')
      .click(createAttributeRollEventListener(this.actor, true))

    html
      .find('.combat-attributes > a')
      .click(createCombatActionRollEventListener(this.actor))

    html.find('.damage-roll a').click(createDamageRollEventListener(this.actor))

    html.find('.talent-label a').click(createSkillRollEventListener(this.actor))
    html.find('.roll-spell').click(createSkillRollEventListener(this.actor))

    html
      .find('.open-lcd-page')
      .click(createOpenPDFPageListener(RuleBook.LiberCantionesDeluxe))

    html
      .find('.open-ll-page')
      .click(createOpenPDFPageListener(RuleBook.LiberLiturgium))

    html
      .find('.talent-label a')
      .contextmenu(createRenderItemSheetEventListener(this.actor))
    html
      .find('.spell-label a')
      .contextmenu(createRenderItemSheetEventListener(this.actor))
    html
      .find('.liturgy-label a')
      .contextmenu(createRenderItemSheetEventListener(this.actor))
    html
      .find('.item-edit')
      .click(createRenderItemSheetEventListener(this.actor))

    html.find('.item-info').click(createCollapseEventListener('trait'))
    html
      .find('.special-ability-label .mode')
      .click(createCollapseEventListener('special-ability'))

    html.find('.ranged_attack_config select').change((event) => {
      const weaponId = $(event.currentTarget)
        .parents('.weapon')
        ?.data('attribute')
      const weapon = this.actor.getOwnedItem(weaponId)?.data
      const configElement = $(event.currentTarget).parents(
        '.ranged_attack_config'
      )
      const options = {
        rangeClass: configElement.find('.range_class select').val(),
        sizeClass: configElement.find('.size_class select').val(),
      }
      const modificator = this.actor.rangedAttackModifier(weapon, options)
      //configElement.find('.modifier').text(modificator)
      this.rangedAttackConfig = {
        modifier: modificator || 0,
        rangeClass: options.rangeClass,
        sizeClass: options.sizeClass,
      }
    })

    html
      .find('.effect-edit')
      .click(createRenderActiveEffectConfigEventListener(this.actor))

    html
      .find('.effect-add')
      .click(createAddActiveEffectEventListener(this.actor))

    html
      .find('.effect-disable')
      .click(createDisableActiveEffectEventListener(this.actor))

    html
      .find('.effect-delete')
      .click(createDeleteActiveEffectEventListener(this.actor))
    html.find('.item-add').click(createAddItemEventListener(this.actor))
    html.find('.item-delete').click(createDeleteItemEventListener(this.actor))
    html.find('.item-name').click(createCollapseEventListener('item'))
  }
}

function createCollapseEventListener(type) {
  return (event) => {
    const typeElement = $(event.currentTarget)
      .parents(`.${type}`)
      .find(`.${type}-description`)

    const description = typeElement
    description.toggleClass('collapsed')

    if (description.hasClass('collapsed')) {
      description.slideUp()
    } else {
      description.slideDown()
    }
  }
}

function createRenderItemSheetEventListener(actor) {
  return (event) => {
    const item = actor.items.get(event.currentTarget.name)
    item.sheet.render(true)
  }
}

abstract class RollEventListener {
  protected _actor: DsaFourOneActor

  constructor(actor) {
    this._actor = actor
  }

  _rollTarget(event) {
    return event.currentTarget.name
  }

  abstract _rollMethod(target, isFastForward, options, event?)

  abstract _getOptions(action, event)

  async _renderDialog() {
    const template = 'systems/dsa-4.1/templates/roll-dialog.html'
    const dialogData = {}

    return renderTemplate(template, dialogData)
  }

  async _showDialog(name) {
    const html = this._renderDialog()

    return new Promise((resolve) => {
      const game = getGame()
      html.then((html) =>
        new Dialog({
          title: game.i18n.localize(`DSA.${name}`),
          content: html,
          buttons: {
            normal: {
              label: 'Roll',
              callback: (html) =>
                resolve(
                  parseInt(html[0].querySelector('form').modificator.value)
                ),
            },
          },
          default: 'normal',
          close: () => resolve(0),
        }).render(true)
      )
    })
  }

  handleEvent(event) {
    const target = this._rollTarget(event)
    // const fastForward = this._isFastForward(event)
    const options = this._getOptions(target, event) || {}

    const combatMode = $(event.currentTarget)
      .parents('.combat-mode')
      ?.data('combatMode')
    const isArmed = combatMode === 'armed'
    const maneuverType = {
      attack: 'offensive',
      parry: 'defensive',
      rangedAttack: '',
    }[target]
    const lastManeuver = this._actor.getFlag(
      'dsa-4.1',
      `last-${maneuverType}-maneuver`
    ) as ManeuverDescriptor[]
    const dialog = ManeuverEditor.create(
      maneuverType,
      lastManeuver,
      new FoundryCharacter(this._actor),
      isArmed
    ) //this._showDialog(target)
    dialog.then(([modifiers, mod]) => {
      options.mod = mod
      options.modifiers = modifiers
      this._actor.setFlag('dsa-4.1', `last-${maneuverType}-maneuver`, modifiers)
      this._rollMethod(target, true, options, event)
    })
  }

  _isFastForward(event) {
    return (
      event &&
      (event.shiftKey || event.altKey || event.ctrlKey || event.metaKey)
    )
  }
}

class AttributeRollEventListener extends RollEventListener {
  private _isNegative: boolean

  constructor(actor, isNegative) {
    super(actor)
    this._isNegative = isNegative
  }

  _getOptions(action, event) {
    return
  }

  _rollMethod(target, isFastForward, options) {
    this._actor.rollAttribute(target, isFastForward, this._isNegative, options)
  }
}

function createAttributeRollEventListener(actor, isNegative) {
  return (event) => {
    const eventListener = new AttributeRollEventListener(actor, isNegative)
    eventListener.handleEvent(event)
  }
}

export class SkillRollEventListener extends RollEventListener {
  _rollMethod(target, isFastForward, options) {
    this._actor.rollSkill(target, options)
  }

  _getOptions(action, event) {
    return undefined
  }

  handleEvent(event) {
    const target = this._rollTarget(event)

    const item = this._actor.items.find((i) => i.id === target)
    const options = this._getOptions(target, event) || ({} as any)
    const character = new FoundryCharacter(this._actor)
    const skill = FoundrySkill.create(item, character)
    if (skill) {
      const dialog = SkillRollDialog.create(skill, character)
      dialog.then(([modifiers, mod, selectedAttributes]) => {
        options.mod = mod
        options.modifiers = modifiers
        options.testAttributes = selectedAttributes
        this._rollMethod(target, true, options)
      })
    }
  }
}

function createSkillRollEventListener(actor) {
  return (event) => {
    const eventListener = new SkillRollEventListener(actor)
    eventListener.handleEvent(event)
  }
}

class CombatRollEventListener extends RollEventListener {
  getWeaponOrTalent(event) {
    const itemId = $(event.currentTarget)
      .parents('.weapon, .talent')
      ?.data('attribute')
    return this._actor.items.get(itemId)
  }

  _getOptions(action, event) {
    return
  }

  _rollMethod(target, isFastForward, options, event) {
    return
  }
}

class DamageRollEventListener extends CombatRollEventListener {
  _rollMethod(target, isFastForward, options, event) {
    const weaponOrTalent = this.getWeaponOrTalent(event)
    this._actor.rollDamage(weaponOrTalent, isFastForward, options)
  }
}

function createDamageRollEventListener(actor) {
  return (event) => {
    const eventListener = new DamageRollEventListener(actor)
    eventListener.handleEvent(event)
  }
}

class CombatActionRollEventListener extends CombatRollEventListener {
  _getOptions(action, event) {
    if (action == 'rangedAttack') {
      const configElement = $(event.currentTarget)
        .parents('.weapon')
        .find('.ranged_attack_config')
      return {
        rangeClass: configElement.find('.range_class select').val(),
        sizeClass: configElement.find('.size_class select').val(),
      }
    }
  }

  _rollMethod(action, isFastForward, options, event) {
    const weaponOrTalent = this.getWeaponOrTalent(event)
    // const actionDict = {
    //   attack: AttackAction,
    //   parry: ParryAction,
    //   rangedAttack: RangedAttackAction,
    //   dodge: DodgeAction,
    // }
    this._actor.rollCombatAction(action, weaponOrTalent, options)
  }
}

function createCombatActionRollEventListener(actor) {
  return (event) => {
    const eventListener = new CombatActionRollEventListener(actor)
    eventListener.handleEvent(event)
  }
}

function createRenderActiveEffectConfigEventListener(actor) {
  return (event) => {
    const effect = actor.effects.get(event.currentTarget.name)
    effect.sheet.render(true)
  }
}

function createAddActiveEffectEventListener(actor) {
  return async () => {
    const effectData = {
      label: 'New Effect',
      icon: 'icons/svg/blood.svg',
    }

    await actor.createEmbeddedDocuments('ActiveEffect', [effectData], {
      renderSheet: true,
    })
  }
}

function createDisableActiveEffectEventListener(actor) {
  return async (event) => {
    const effect = actor.effects.get(event.currentTarget.name)
    const updateData = {
      disabled: !effect.data.disabled,
    }
    await effect.update(updateData)
  }
}

function createDeleteActiveEffectEventListener(actor) {
  return async (event) => {
    await actor.deleteEmbeddedDocuments('ActiveEffect', [
      event.currentTarget.name,
    ])
  }
}

function createAddItemEventListener(actor) {
  return async (event) => {
    const itemData = {
      name: getGame().i18n.localize('DSA.newItem'),
      img: 'icons/svg/item-bag.svg',
      type: $(event.currentTarget).data('itemType'),
    }

    await actor.createEmbeddedDocuments('Item', [itemData], {
      renderSheet: true,
    })
  }
}

function createDeleteItemEventListener(actor) {
  return async (event) => {
    await actor.deleteEmbeddedDocuments('Item', [event.currentTarget.name])
  }
}
