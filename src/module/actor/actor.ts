import { FoundryCharacter } from '../character/character'
import {
  FoundryMeleeWeapon,
  FoundryRangedWeapon,
  FoundryWeapon,
} from '../character/weapon'
import { FoundryShield } from '../character/shield'
import { DsaRoll, critical1d20 } from '../dsa-roll'
import {
  ComputeAttack,
  ComputeDamageFormula,
  ComputeParry,
  ComputeRangedAttack,
} from '../ruleset/rules/derived-combat-attributes'
import { FoundrySkill } from '../character/skill'
import { getGame } from '../utils'
import { FoundryCombatTalent } from '../character/combat-talent'
import { CombatState } from '../model/character'
import { InitiativeFormula } from '../ruleset/rules/basic-combat'

class DsaItem {
  protected _item: Item

  constructor(item) {
    this._item = item
  }

  static create(item) {
    switch (item?.type) {
      case 'combatTalent':
        return new CombatTalent(item)
      case 'meleeWeapon':
        return new FoundryMeleeWeapon(item)
      case 'rangedWeapon':
        return new FoundryRangedWeapon(item)
      case 'shield':
        return new FoundryShield(item)
    }
  }
}

class CombatTalent extends DsaItem {
  get attack() {
    return this._item.data.data.combat.attack
  }

  get rangedAttack() {
    return this._item.data.data.combat.rangedAttack
  }

  get parry() {
    return this._item.data.data.combat.parry
  }
}

declare global {
  interface DocumentClassConfig {
    Actor: typeof DsaFourOneActor
  }
}

interface DsaActorDataSourceData {
  /* properties defined in the template.json for that particular type */
  [key: string]: any
}

interface DsaActorDataSource {
  type: 'character'
  data: DsaActorDataSourceData
}
type DsaActorDataProperties = DsaActorDataSource

type ActorDataSource = DsaActorDataSource
type ActorDataProperties = DsaActorDataProperties

declare global {
  interface SourceConfig {
    Actor: ActorDataSource
  }

  interface DataConfig {
    Actor: ActorDataProperties
  }
}

export class DsaFourOneActor extends Actor {
  prepareData() {
    super.prepareData()
  }

  get combatState(): CombatState {
    const primaryHandId = this.data.data.base.combatState['primaryHand']
    const primaryHandItem = this.items.get(primaryHandId)
    const primaryHand = primaryHandItem
      ? FoundryWeapon.create(primaryHandItem)
      : undefined

    const secondaryHandId = this.data.data.base.combatState['secondaryHand']
    const secondaryHandItem = this.items.get(secondaryHandId)
    const secondaryHand = secondaryHandItem
      ? secondaryHandItem.data.type === 'shield'
        ? new FoundryShield(secondaryHandItem)
        : FoundryWeapon.create(secondaryHandItem)
      : undefined

    const character = new FoundryCharacter(this)

    const unarmedTalentId = this.data.data.base.combatState['unarmedTalent']
    const unarmedTalentItem = this.items.get(unarmedTalentId)
    const unarmedTalent = unarmedTalentItem
      ? new FoundryCombatTalent(unarmedTalentItem, character)
      : undefined

    return {
      isArmed: this.data.data.base.combatState.isArmed,
      primaryHand,
      secondaryHand,
      unarmedTalent,
    }
  }

  attack(itemData) {
    const item = this.items.find((item) => item.id == itemData._id)
    const secondaryWeaponData = this._getSecondaryWeapon()
    let shield
    if (secondaryWeaponData?.type === 'shield') {
      shield = this.items.find((item) => item.id == secondaryWeaponData?._id)
    }

    if (item) {
      const game = getGame()
      const character = new FoundryCharacter(this)
      return game.ruleset.compute(ComputeAttack, {
        character,
        weapon: ['meleeWeapon', 'rangedWeapon'].includes(item.type)
          ? FoundryWeapon.create(item)
          : undefined,
        talent:
          item.type === 'combatTalent'
            ? new FoundryCombatTalent(item, character)
            : undefined,
        shield: shield ? new FoundryShield(shield) : undefined,
      }).value
    }
  }

  parry(itemData) {
    const item = this.items.find((item) => item.id == itemData._id)
    const secondaryWeaponData = this._getSecondaryWeapon()
    let shield
    if (secondaryWeaponData?.type === 'shield') {
      shield = this.items.find((item) => item.id == secondaryWeaponData?._id)
    }

    if (item) {
      const game = getGame()
      const character = new FoundryCharacter(this)
      return game.ruleset.compute(ComputeParry, {
        character,
        weapon: ['meleeWeapon', 'rangedWeapon'].includes(item.type)
          ? FoundryWeapon.create(item)
          : undefined,
        talent:
          item.type === 'combatTalent'
            ? new FoundryCombatTalent(item, character)
            : undefined,
        shield: shield ? new FoundryShield(shield) : undefined,
      }).value
    }
  }

  // initiative(itemData) {
  initiative(): InitiativeFormula {
    return new FoundryCharacter(this).initiative()
    // const item = this.items.find((item) => item.id == itemData._id)
    // const secondaryWeaponData = this._getSecondaryWeapon()
    // let shield
    // if (secondaryWeaponData?.type === 'shield') {
    //   shield = this.items.find((item) => item.id == secondaryWeaponData?._id)
    // }

    // if (item) {
    //   const game = getGame()
    //   const character = new FoundryCharacter(this)
    //   return game.ruleset.compute(ComputeInitiativeFormula, {
    //     character,
    //     weapon: ['meleeWeapon', 'rangedWeapon'].includes(item.type)
    //       ? FoundryWeapon.create(item)
    //       : undefined,
    //     talent:
    //       item.type === 'combatTalent'
    //         ? new FoundryCombatTalent(item, character)
    //         : undefined,
    //     shield: shield ? new FoundryShield(shield) : undefined,
    //   })
    // }
  }

  damage(itemData) {
    const item = this.items.find((item) => item.id == itemData._id)
    // if (weapon.data.type === 'rangedWeapon') return this.getDamage(weapon.data)
    const secondaryWeaponData = this._getSecondaryWeapon()
    let shield
    if (secondaryWeaponData?.type === 'shield') {
      shield = this.items.find((item) => item.id == secondaryWeaponData?._id)
    }

    if (item) {
      const game = getGame()
      const character = new FoundryCharacter(this)
      return game.ruleset.compute(ComputeDamageFormula, {
        character,
        weapon: ['meleeWeapon', 'rangedWeapon'].includes(item.type)
          ? FoundryWeapon.create(item)
          : undefined,
        talent:
          item.type === 'combatTalent'
            ? new FoundryCombatTalent(item, character)
            : undefined,
        shield: shield ? new FoundryShield(shield) : undefined,
      }).formula
    }
  }

  dodge() {
    return new FoundryCharacter(this).dodgeValue()
  }

  rangedAttack(weaponData) {
    const weapon = this.items.find((item) => item.id == weaponData._id)

    if (weapon) {
      const game = getGame()
      return game.ruleset.compute(ComputeRangedAttack, {
        character: new FoundryCharacter(this),
        weapon: FoundryWeapon.create(weapon),
      }).value
    }
  }

  rangedAttackModifier(weaponData, options) {
    const weapon = this.items.find((item) => item.id == weaponData._id)

    if (weapon) {
      const game = getGame()
      return game.ruleset.compute(ComputeRangedAttack, {
        character: new FoundryCharacter(this),
        weapon: FoundryWeapon.create(weapon),
        ...options,
      }).mod
    }
  }

  talent(name) {
    return DsaItem.create(this._itemByName(name))
  }

  _getEquippedWeapon(hand) {
    const weaponId = this.data.data.base.combatState[hand + 'Hand']
    return this.items.get(weaponId)?.data
  }

  _getPrimaryWeapon() {
    return this._getEquippedWeapon('primary')
  }

  _getSecondaryWeapon() {
    return this._getEquippedWeapon('secondary')
  }

  _getWeaponAttribute(attribute, weapon = this._getPrimaryWeapon()) {
    if (attribute === 'dodge') {
      return this.dodge()
    }

    if (weapon === undefined) {
      return 0
    }

    const attrTalentName = weapon.data.talent
    const attrTalent =
      this._itemByName(attrTalentName) || this._itemBySID(attrTalentName)

    let attr = attrTalent?.data.data.combat[attribute]

    const secondaryWeapon = this._getSecondaryWeapon()

    if (secondaryWeapon && secondaryWeapon.type === 'shield') {
      if (attribute === 'attack') {
        attr = attr + secondaryWeapon.data.weaponMod.attack
      }

      if (attribute === 'parry') {
        const baseParry =
          this.data.data.base.combatAttributes.active['baseParry'].value

        attr = baseParry + secondaryWeapon.data.weaponMod.parry
      }
    }

    return attr
  }

  getAttack(weapon = undefined) {
    return this._getWeaponAttribute('attack', weapon)
  }

  getParry(weapon = undefined) {
    return this._getWeaponAttribute('parry', weapon)
  }

  getRangedAttack(weapon = undefined) {
    return this._getWeaponAttribute('rangedAttack', weapon)
  }

  getDamage(weapon) {
    const damageFormula = weapon.data.damage
    return damageFormula
  }

  _calculateArmorAttribute(attribute) {
    const equippedArmors = this.data.items.filter(
      (item) => item.type === 'armor' && item.data.equiped
    )

    return equippedArmors.reduce(
      (curr, armor) => curr + armor.data[attribute],
      0
    )
  }

  getArmorClass() {
    return new FoundryCharacter(this).armorClass
  }

  getEncumbarance() {
    return new FoundryCharacter(this).encumbarance
  }

  isEquipable(weapon) {
    return this.items.some((item) => item.data.data.sid === weapon.data.talent)
  }

  _itemByName(name) {
    return this.items.find((item) => item.data.name === name)
  }

  _itemBySID(sid) {
    return this.items.find((item) => item.data.data.sid === sid)
  }

  rollDamage(weapon, isFastForward = false, options) {
    const game = getGame()
    const name = game.i18n.localize('DSA.damage')
    const parts = [this.damage(weapon.data), options.mod]

    return new DsaRoll(parts, { name }).roll(isFastForward)
  }

  async rollAttribute(
    abilityId,
    isFastForward = false,
    isNegativeAttribute = false,
    options
  ) {
    let name = ''
    let targetValue = 0

    if (isNegativeAttribute) {
      const attribute = this.items.get(abilityId)?.data
      name = attribute?.name || ''
      targetValue = attribute?.data.value || 0
    } else {
      const game = getGame()
      name = game.i18n.localize('DSA.' + abilityId)
      targetValue = this.data.data.base.basicAttributes[abilityId].value
    }

    const parts = ['1d20', options.mod]

    const rollOptions = {
      successChecker: (roll) => roll.total <= targetValue,
      criticalChecker: critical1d20,
      name,
    }

    return new DsaRoll(parts, rollOptions).roll(isFastForward)
  }

  rollCombatAction(action, itemData, options) {
    // const result = game.ruleset.execute(action, {
    //   character: new FoundryCharacter(this),
    //   weapon: new FoundryMeleeWeapon(weapon),
    //   ...options,
    // })
    const character = new FoundryCharacter(this)
    const weapon = ['meleeWeapon', 'rangedWeapon'].includes(itemData?.type)
      ? FoundryWeapon.create(itemData)
      : undefined
    const talent =
      itemData?.type === 'combatTalent'
        ? new FoundryCombatTalent(itemData, character)
        : undefined
    // if (action === 'dodge') {
    //   character.dodge(options)
    // } else {
    character[action]({ ...options, weapon, talent })
    // }
    // const name = game.i18n.localize('DSA.' + action);
    // const targetValue = this._getWeaponAttribute(action, weapon);
    // const parts = ['1d20', '@bonus'];
    // let rollOptions = {
    // 	successChecker: roll => (roll.total <= targetValue),
    // 	criticalChecker: critical1d20,
    // 	confirmCritical: true,
    // 	name: name,
    // };
    // const dsaRoll = new DsaRoll(parts, rollOptions)
    // const roll = dsaRoll.roll(event);
    // result.roll.then(roll => roll.toMessage(roll))
    // return result.roll
  }

  rollSkill(skillId, options) {
    const items = this.items.filter((i) => i.id === skillId)

    if (items.length === 0) return

    const skillData = items[0]
    FoundrySkill.create(skillData, new FoundryCharacter(this))?.roll(options)
    // const skill = {
    //   name: skillData.name,
    //   identifier: skillData.data.sid,
    // }
    // const result = game.ruleset.execute(TalentAction, {
    //   character: new FoundryCharacter(this),
    //   ...options,
    //   skill,
    // })
    // return result.roll
    // const items = this.items.filter((i) => i.id === skillId)

    // if (items.length === 0) return

    // const skill = items[0].data
    // const skillValue = skill.data.value

    // const parts = []
    // const skillMap = ['firstAttribute', 'secondAttribute', 'thirdAttribute']
    // const skillCheckData = []

    // for (let i = 0; i < 3; i++) {
    //   const attributeName = attributeFromAbbr(
    //     skill.data.test[skillMap[i]].trim()
    //   )
    //   const attribute = this.data.data.base.basicAttributes[attributeName].value

    //   skillCheckData.push({
    //     name: attributeName,
    //     value: attribute,
    //   })
    //   parts.push('{(' + attribute + '-1d20+{@skillValue - @bonus,0}kl),0}kl')
    // }

    // parts.push('{@skillValue - @bonus ,0}kh')

    // const rollOptions = {
    //   successChecker: (roll) => roll.total >= 0,
    //   criticalChecker: critical3d20,
    //   name: skill.name,
    //   maxTotal: skillValue,
    // }

    // return new DsaRoll(parts, rollOptions, {
    //   skillValue,
    //   bonus: options.mod,
    //   is3d20Roll: true,
    //   skillCheckData,
    //   skillType: determineSkillType(skill),
    // }).roll(isFastForward)
  }

  async importItem(data) {
    const game = getGame()

    if (data.pack) {
      const pack = game.packs.get(data.pack)
      if (pack) {
        const item = await pack.get(data.id)
        return await this.createEmbeddedDocuments('Item', [item?.data])
      }
    } else if (data.data) {
      return this.createEmbeddedDocuments('Item', [data.data])
    } else {
      const game = getGame()
      const item = game.items?.get(data.id)
      if (item) {
        return this.createEmbeddedDocuments('Item', [item.data])
      }
    }
    return undefined
  }
}
