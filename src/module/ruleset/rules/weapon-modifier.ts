import type { MeleeWeapon } from '../../model/items'
import { DescribeRule } from '../rule'
import type { Ruleset } from '../ruleset'
import { usesMeleeWeapon } from './basic-combat'
import { ComputeAttack, ComputeParry } from './derived-combat-attributes'

export const WeaponModifierRule = DescribeRule(
  'basic-shield-combat-rule',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeAttack)
      .when(usesMeleeWeapon)
      .do((options, result) => ({
        ...result,
        value: result.value + (<MeleeWeapon>options.weapon).weaponMod.attack,
      }))

    ruleset
      .after(ComputeParry)
      .when(usesMeleeWeapon)
      .do((options, result) => {
        if (!options.shield) {
          result.value += (<MeleeWeapon>options.weapon).weaponMod.parry
        }
        return result
      })
  }
)
