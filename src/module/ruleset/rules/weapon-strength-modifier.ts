import type { MeleeWeapon } from '../../model/items'
import { DescribeRule } from '../rule'
import type { Ruleset } from '../ruleset'
import { usesMeleeWeapon } from './basic-combat'
import { ComputeDamageFormula } from './derived-combat-attributes'

export const WeaponStrengthModifierRule = DescribeRule(
  'basic-shield-combat-rule',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeDamageFormula)
      .when(usesMeleeWeapon)
      .do((options, result) => {
        const strengthDiff =
          options.character.attribute('strength').value -
          (<MeleeWeapon>options.weapon).strengthMod.threshold
        const damageMod =
          Math.sign(strengthDiff) *
          Math.floor(
            Math.abs(strengthDiff) /
              (<MeleeWeapon>options.weapon).strengthMod.hitPointStep
          )
        result.bonusDamage += damageMod
        return result
      })
  }
)
