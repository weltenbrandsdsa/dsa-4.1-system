import { Maneuver } from '../../../character/maneuver'
import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import type { Ruleset } from '../../ruleset'
import {
  AddManeuver,
  CharacterHas,
  ComputeManeuverList,
  IsArmedManeuver,
  IsOffensiveManeuver,
} from './basic-maneuver'

export const Downthrow: Ability = {
  name: 'Niederwerfen',
  identifier: 'ability-niederwerfen',
}

export const DownthrowManeuver = new Maneuver('downthrow', 'offensive', {
  minMod: 4,
})

export const DownthrowRule = DescribeRule(
  'downthrow',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeManeuverList)
      .when(IsOffensiveManeuver)
      .when(IsArmedManeuver)
      .when(CharacterHas(Downthrow))
      .do(AddManeuver(DownthrowManeuver))
  }
)
