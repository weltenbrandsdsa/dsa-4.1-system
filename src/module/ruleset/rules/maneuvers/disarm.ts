import { Maneuver } from '../../../character/maneuver'
import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import type { Ruleset } from '../../ruleset'
import {
  AddManeuver,
  CharacterHas,
  ComputeManeuverList,
  IsArmedManeuver,
  IsDefensiveManeuver,
} from './basic-maneuver'

export const Disarm: Ability = {
  name: 'Entwaffnen',
  identifier: 'ability-entwaffnen',
}

export const DisarmManeuver = new Maneuver('disarm', 'defensive', {
  minMod: 8,
})

export const DisarmRule = DescribeRule(
  'disarm',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeManeuverList)
      .when(IsDefensiveManeuver)
      .when(IsArmedManeuver)
      .when(CharacterHas(Disarm))
      .do(AddManeuver(DisarmManeuver))
  }
)
