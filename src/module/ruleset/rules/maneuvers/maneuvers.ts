import { BindRule } from './bind'
import { ClearanceRule } from './clearance'
import { DefensiveStyleRule } from './defensive-style'
import { DisarmRule } from './disarm'
import { DownthrowRule } from './downthrow'
import { FeintRule } from './feint'
import { HammerStrikeRule } from './hammer-strike'
import { HoldupRule } from './holdup'
import { MasterparryRule } from './masterparry'
import { MightyStrikeRule } from './mighty-strike'
import { SallyRule } from './sally'

export const ManeuverRules = [
  BindRule,
  ClearanceRule,
  DefensiveStyleRule,
  DisarmRule,
  DownthrowRule,
  FeintRule,
  HammerStrikeRule,
  HoldupRule,
  MasterparryRule,
  MightyStrikeRule,
  SallyRule,
]
