import { Maneuver } from '../../../character/maneuver'
import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import type { Ruleset } from '../../ruleset'
import {
  AddManeuver,
  CharacterHas,
  ComputeManeuverList,
  IsArmedManeuver,
  IsDefensiveManeuver,
} from './basic-maneuver'

export const DefensiveStyle: Ability = {
  name: 'Defensiver Kampfstile',
  identifier: 'ability-defensiver-kampfstil',
}

export const DefensiveStyleManeuver = new Maneuver(
  'defensiveStyle',
  'defensive'
)

export const DefensiveStyleRule = DescribeRule(
  'defensiveStyle',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeManeuverList)
      .when(IsDefensiveManeuver)
      .when(IsArmedManeuver)
      .when(CharacterHas(DefensiveStyle))
      .do(AddManeuver(DefensiveStyleManeuver))
  }
)
