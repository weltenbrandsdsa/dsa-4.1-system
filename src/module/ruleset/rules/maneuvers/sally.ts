import { Maneuver } from '../../../character/maneuver'
import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import type { Ruleset } from '../../ruleset'
import {
  AddManeuver,
  CharacterHas,
  ComputeManeuverList,
  IsArmedManeuver,
  IsOffensiveManeuver,
} from './basic-maneuver'

export const Sally: Ability = {
  name: 'Ausfall',
  identifier: 'ability-ausfall',
}

export const SallyManeuver = new Maneuver('sally', 'offensive')

export const SallyRule = DescribeRule(
  'sally',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeManeuverList)
      .when(IsOffensiveManeuver)
      .when(IsArmedManeuver)
      .when(CharacterHas(Sally))
      .do(AddManeuver(SallyManeuver))
  }
)
