import { Maneuver } from '../../../character/maneuver'
import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import type { Ruleset } from '../../ruleset'
import {
  AddManeuver,
  CharacterHas,
  ComputeManeuverList,
  IsArmedManeuver,
  IsOffensiveManeuver,
} from './basic-maneuver'

export const HammerStrike: Ability = {
  name: 'Hammerschlag',
  identifier: 'ability-hammerschlag',
}

export const HammerStrikeManeuver = new Maneuver('hammerStrike', 'offensive', {
  minMod: 8,
})

export const HammerStrikeRule = DescribeRule(
  'hammerStrike',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeManeuverList)
      .when(IsOffensiveManeuver)
      .when(IsArmedManeuver)
      .when(CharacterHas(HammerStrike))
      .do(AddManeuver(HammerStrikeManeuver))
  }
)
