import { Maneuver } from '../../../character/maneuver'
import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import type { Ruleset } from '../../ruleset'
import {
  AddManeuver,
  CharacterHas,
  ComputeManeuverList,
  IsArmedManeuver,
  IsDefensiveManeuver,
} from './basic-maneuver'

export const Bind: Ability = {
  name: 'Binden',
  identifier: 'ability-binden',
}

export const BindManeuver = new Maneuver('bind', 'defensive', {
  minMod: 4,
})

export const BindRule = DescribeRule(
  'bind',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeManeuverList)
      .when(IsDefensiveManeuver)
      .when(IsArmedManeuver)
      .when(CharacterHas(Bind))
      .do(AddManeuver(BindManeuver))
  }
)
