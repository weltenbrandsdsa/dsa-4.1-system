import type {
  ManeuverDescriptor,
  ManeuverType,
  ModifierDescriptor,
} from '../../../model/modifier'
import type { BaseProperty } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import {
  BaseComputationOptionData,
  CreateComputationIdentifier,
} from '../../rule-components'
import type { Ruleset } from '../../ruleset'
import {
  AttackAction,
  CombatActionData,
  CombatActionResult,
  DodgeAction,
  ParryAction,
} from '../basic-combat'
import { RangedAttackAction } from '../basic-ranged-combat'

export interface ManeuverListOptions extends BaseComputationOptionData {
  isArmed?: boolean
  maneuverType: ManeuverType
}
export interface ManeuverListResult {
  maneuvers: ManeuverDescriptor[]
}

export const ComputeManeuverList = CreateComputationIdentifier<
  ManeuverListOptions,
  ManeuverListResult
>('maneuverList')

function CreateManeuverList(options: ManeuverListOptions): ManeuverListResult {
  return {
    maneuvers: [],
  }
}

function IsFailed(_options: CombatActionData, result: CombatActionResult) {
  return result.success === false
}

export function IsOffensiveManeuver(options: ManeuverListOptions): boolean {
  return options.maneuverType === 'offensive'
}

export function IsDefensiveManeuver(options: ManeuverListOptions): boolean {
  return options.maneuverType === 'defensive'
}

export function IsArmedManeuver(options: ManeuverListOptions): boolean {
  return options.isArmed === undefined ? true : options.isArmed
}

export function CharacterHas<OptionData extends BaseComputationOptionData>(
  property: BaseProperty
) {
  return (options: OptionData): boolean => options.character.has(property)
}

export function AddManeuver(maneuver: ManeuverDescriptor) {
  return (
    _options: ManeuverListOptions,
    result: ManeuverListResult
  ): ManeuverListResult => {
    result.maneuvers.push(maneuver)
    return result
  }
}

function DeterminePenality(
  options: CombatActionData,
  result: CombatActionResult
) {
  const maneuvers: ManeuverDescriptor[] = (options.modifiers || [])
    .filter(
      (modifier: ModifierDescriptor) => modifier.modifierType === 'maneuver'
    )
    .map((modifier) => modifier as ManeuverDescriptor)

  if (maneuvers.length > 0) {
    result.penality = maneuvers.reduce(
      (previous: number, current) => previous + current.mod,
      0
    )
  }

  return result
}

function ComputeTotalModifier<
  OptionData extends CombatActionData = CombatActionData
>(options: OptionData) {
  const totalMod = options.modifiers
    ? options.modifiers.reduce((prev, current) => prev + current.mod, 0)
    : 0
  return {
    ...options,
    mod: totalMod,
  }
}

export const BasicManeuverRule = DescribeRule(
  'basic-maneuver',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset.before(AttackAction).do(ComputeTotalModifier)
    ruleset.before(RangedAttackAction).do(ComputeTotalModifier)
    ruleset.before(ParryAction).do(ComputeTotalModifier)
    ruleset.before(DodgeAction).do(ComputeTotalModifier)
    ruleset.after(AttackAction).when(IsFailed).do(DeterminePenality)
    ruleset.after(ParryAction).when(IsFailed).do(DeterminePenality)

    ruleset.on(ComputeManeuverList).do(CreateManeuverList)
  }
)
