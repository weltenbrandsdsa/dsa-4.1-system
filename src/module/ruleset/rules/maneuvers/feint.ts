import { Maneuver } from '../../../character/maneuver'
import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import type { Ruleset } from '../../ruleset'
import {
  AddManeuver,
  ComputeManeuverList,
  IsArmedManeuver,
  IsOffensiveManeuver,
} from './basic-maneuver'

export const Feint: Ability = {
  name: 'Finte',
  identifier: 'ability-finte',
}

export const FeintManeuver = new Maneuver('feint', 'offensive')

export const FeintRule = DescribeRule(
  'feint',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeManeuverList)
      .when(IsOffensiveManeuver)
      .when(IsArmedManeuver)
      .do(AddManeuver(FeintManeuver))
  }
)
