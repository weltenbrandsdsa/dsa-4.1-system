import { Maneuver } from '../../../character/maneuver'
import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import type { Ruleset } from '../../ruleset'
import {
  AddManeuver,
  CharacterHas,
  ComputeManeuverList,
  IsArmedManeuver,
  IsDefensiveManeuver,
} from './basic-maneuver'

export const Masterparry: Ability = {
  name: 'Meisterparade',
  identifier: 'ability-meisterparade',
}

export const MasterparryManeuver = new Maneuver('masterparry', 'defensive')

export const MasterparryRule = DescribeRule(
  'masterparry',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeManeuverList)
      .when(IsDefensiveManeuver)
      .when(IsArmedManeuver)
      .when(CharacterHas(Masterparry))
      .do(AddManeuver(MasterparryManeuver))
  }
)
