import { Maneuver } from '../../../character/maneuver'
import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import type { Ruleset } from '../../ruleset'
import {
  CombatDamageComputationData,
  ComputeDamageFormula,
  DamageFormula,
} from '../derived-combat-attributes'
import {
  AddManeuver,
  ComputeManeuverList,
  IsArmedManeuver,
  IsOffensiveManeuver,
} from './basic-maneuver'

export const MightyStrike: Ability = {
  name: 'Wuchtschlag',
  identifier: 'ability-wuchtschlag',
}

export const MightyStrikeManeuver = new Maneuver('mightyStrike', 'offensive')

export const MightyStrikeRule = DescribeRule(
  'mightyStrike',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeDamageFormula)
      .do((options: CombatDamageComputationData, result: DamageFormula) => {
        const mightyStrike = options.modifiers?.find(
          (maneuver) => maneuver.name === MightyStrikeManeuver.name
        )
        if (mightyStrike) {
          result.bonusDamage = mightyStrike.mod
          if (!options.character.has(MightyStrike)) {
            result.bonusDamage = Math.ceil(result.bonusDamage / 2)
          }
        }

        return result
      })

    ruleset
      .after(ComputeManeuverList)
      .when(IsOffensiveManeuver)
      .when(IsArmedManeuver)
      .do(AddManeuver(MightyStrikeManeuver))
  }
)
