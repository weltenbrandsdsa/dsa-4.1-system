import { Maneuver } from '../../../character/maneuver'
import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import type { Ruleset } from '../../ruleset'
import {
  AddManeuver,
  CharacterHas,
  ComputeManeuverList,
  IsArmedManeuver,
  IsDefensiveManeuver,
} from './basic-maneuver'

export const Holdup: Ability = {
  name: 'Entwaffnen',
  identifier: 'ability-entwaffnen',
}

export const HoldupManeuver = new Maneuver('holdup', 'defensive')

export const HoldupRule = DescribeRule(
  'holdup',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeManeuverList)
      .when(IsDefensiveManeuver)
      .when(IsArmedManeuver)
      .when(CharacterHas(Holdup))
      .do(AddManeuver(HoldupManeuver))
  }
)
