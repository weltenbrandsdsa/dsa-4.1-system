import { Maneuver } from '../../../character/maneuver'
import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import type { Ruleset } from '../../ruleset'
import {
  AddManeuver,
  CharacterHas,
  ComputeManeuverList,
  IsArmedManeuver,
  IsOffensiveManeuver,
} from './basic-maneuver'

export const Clearance: Ability = {
  name: 'Befreiungsschlag',
  identifier: 'ability-befreiungsschlag',
}

export const ClearanceManeuver = new Maneuver('clearance', 'offensive', {
  minMod: 4,
})

export const ClearanceRule = DescribeRule(
  'clearance',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeManeuverList)
      .when(IsOffensiveManeuver)
      .when(IsArmedManeuver)
      .when(CharacterHas(Clearance))
      .do(AddManeuver(ClearanceManeuver))
  }
)
