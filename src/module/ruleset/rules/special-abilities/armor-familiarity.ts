import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import { BaseComputationOptionData } from '../../rule-components'
import { And, Not, Or, Ruleset } from '../../ruleset'
import { ComputeInitiative } from '../basic-combat'
import { ComputeEncumbarance } from '../derived-combat-attributes'
import { CharacterHas } from '../maneuvers/basic-maneuver'

export const ArmorFamiliarityI: Ability = {
  name: 'Rüstungsgewöhnung 1',
  identifier: 'ability-ruestungsgewoehnung-i',
}
export const ArmorFamiliarityII: Ability = {
  name: 'Rüstungsgewöhnung 2',
  identifier: 'ability-ruestungsgewoehnung-ii',
}
export const ArmorFamiliarityIII: Ability = {
  name: 'Rüstungsgewöhnung 3',
  identifier: 'ability-ruestungsgewoehnung-iii',
}
export const ArmorFamiliarityRule = DescribeRule(
  'armorFamiliarity',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeEncumbarance)
      .when(
        And(
          Or(CharacterHas(ArmorFamiliarityI), CharacterHas(ArmorFamiliarityII)),
          Not(CharacterHas(ArmorFamiliarityIII))
        )
      )
      .do((options: BaseComputationOptionData, result: number) => {
        return Math.max(0, result - 1)
      })

    ruleset
      .after(ComputeEncumbarance)
      .when(CharacterHas(ArmorFamiliarityIII))
      .do((options: BaseComputationOptionData, result: number) => {
        return Math.max(0, result - 2)
      })

    ruleset
      .after(ComputeInitiative)
      .when(CharacterHas(ArmorFamiliarityIII))
      .do((options: BaseComputationOptionData, result: number) => {
        return result + Math.floor((options.character.encumbarance + 2) / 2)
      })
  }
)
