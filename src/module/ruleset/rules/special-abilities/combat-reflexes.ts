import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import { Ruleset } from '../../ruleset'
import { ComputeInitiative } from '../basic-combat'
import { CombatComputationData } from '../derived-combat-attributes'
import { CharacterHas } from '../maneuvers/basic-maneuver'

export const CombatReflexes: Ability = {
  name: 'Kampfreflexe',
  identifier: 'ability-kampfreflexe',
}
export const CombatReflexesRule = DescribeRule(
  'combatReflexes',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeInitiative)
      .when(CharacterHas(CombatReflexes))
      .do((_options: CombatComputationData, result: number) => result + 4)
  }
)
