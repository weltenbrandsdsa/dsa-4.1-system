import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import { Ruleset } from '../../ruleset'
import {
  ComputeInitiativeFormula,
  ComputeInitiativeFormulaData,
} from '../basic-combat'
import { CharacterHas } from '../maneuvers/basic-maneuver'

export const BladeDancer: Ability = {
  name: 'Klingentänzer',
  identifier: 'ability-klingentaenzer',
}
export const BladeDancerRule = DescribeRule(
  'bladeDancer',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .before(ComputeInitiativeFormula)
      .when(CharacterHas(BladeDancer))
      .do((options: ComputeInitiativeFormulaData) => ({
        ...options,
        dices: 2,
      }))
  }
)
