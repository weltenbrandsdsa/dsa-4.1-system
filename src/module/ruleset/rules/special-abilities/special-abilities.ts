import { ArmorFamiliarityRule } from './armor-familiarity'
import { BladeDancerRule } from './blade-dancer'
import { CombatIntuitionRule } from './combat-intuition'
import { CombatReflexesRule } from './combat-reflexes'

export const SpecialAbilityRules = [
  ArmorFamiliarityRule,
  BladeDancerRule,
  CombatIntuitionRule,
  CombatReflexesRule,
]
