import type { Ability } from '../../../model/properties'
import { DescribeRule } from '../../rule'
import { Ruleset } from '../../ruleset'
import { ComputeInitiative } from '../basic-combat'
import { CombatComputationData } from '../derived-combat-attributes'
import { CharacterHas } from '../maneuvers/basic-maneuver'

export const CombatIntuition: Ability = {
  name: 'Kampfgespür',
  identifier: 'ability-kampfgespuer',
}
export const CombatIntuitionRule = DescribeRule(
  'combatIntuition',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeInitiative)
      .when(CharacterHas(CombatIntuition))
      .do((_options: CombatComputationData, result: number) => result + 2)
  }
)
