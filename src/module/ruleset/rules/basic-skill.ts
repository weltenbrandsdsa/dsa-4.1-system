import type { Character } from '../../model/character'
import type { SkillDescriptor, TestAttributes } from '../../model/properties'
import { DescribeRule } from '../rule'
import {
  Action,
  BaseActionOptionData,
  BaseActionResultType,
  CreateActionIdentifier,
} from '../rule-components'
import type { Ruleset } from '../ruleset'
import {
  Messagable,
  Rollable,
  RollSkill,
  RollSkillToChatEffect,
  SkillRollActionData,
} from './basic-roll-mechanic'

export interface SkillActionData extends BaseActionOptionData {
  skill: SkillDescriptor
  character: Character
  mod?: number
  testAttributes?: TestAttributes
}

export interface SkillActionResult
  extends BaseActionResultType<SkillActionData & SkillRollActionData> {
  mod: number
  roll: Rollable & Messagable
  success: boolean
  damage?: string
}

export const TalentAction = CreateActionIdentifier<
  SkillActionData,
  SkillActionResult
>('talent')
export const SpellAction = CreateActionIdentifier<
  SkillActionData,
  SkillActionResult
>('spell')

export class SkillAction extends Action<SkillActionData, SkillActionResult> {
  async _execute<OptionData extends SkillActionData>(
    options: OptionData
  ): Promise<SkillActionResult> {
    let skill
    switch (options.skill.skillType) {
      case 'talent':
        skill = options.character.talent(options.skill.identifier)
        break
      case 'spell':
        skill = options.character.spell(options.skill.identifier)
        break
    }
    const testAttributes = options.testAttributes
      ? options.testAttributes
      : skill.testAttributes
    const testAttributeData = testAttributes.map((attributeName) => {
      const attribute = options.character.attribute(attributeName)
      return { name: attribute.name, value: attribute.value }
    })
    const rollResultPromise = this.ruleset.execute(RollSkill, {
      ...options,
      skillName: options.skill.name,
      skillType: options.skill.skillType,
      skillValue: skill.value,
      testAttributeData,
      mod: options.mod || 0,
    })
    return rollResultPromise.then((rollResult) => ({
      mod: options.mod || 0,
      roll: rollResult.roll,
      success: rollResult.success,
      options: {
        ...options,
        ...rollResult.options,
      },
    }))
  }
}

export const BasicSkillRule = DescribeRule(
  'basic-skill-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset.on(TalentAction).do(SkillAction)
    ruleset.after(TalentAction).trigger(RollSkillToChatEffect)
    ruleset.on(SpellAction).do(SkillAction)
    ruleset.after(SpellAction).trigger(RollSkillToChatEffect)
  }
)
