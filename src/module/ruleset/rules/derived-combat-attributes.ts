import type { Ruleset } from '../ruleset'
import {
  BaseComputationOptionData,
  CreateComputationIdentifier,
} from '../rule-components'
import type { CombatTalent } from '../../model/properties'
import type { Shield, Weapon } from '../../model/items'
import { DescribeRule } from '../rule'
import type { ModifierDescriptor } from '../../model/modifier'

function ExtractTalentFromWeapon(options) {
  if (options.talent) return options
  if (options.weapon) {
    options.talent = options.character.talent(options.weapon.talent)
    return options
  }
  return options
}

export function IsUnarmed(options: CombatComputationData) {
  return (
    options.weapon === undefined &&
    options.talent !== undefined &&
    options.talent.isUnarmed
  )
}

export function IsArmed(options: CombatComputationData) {
  return !IsUnarmed(options)
}

export interface DamageFormula {
  baseDamage: string
  multiplier: number
  bonusDamage: number
  formula: string
}

interface DamageFormulaOptions {
  multiplier?: number
  bonusDamage?: number
}

export function createDamageFormula(
  baseDamage: string,
  options: DamageFormulaOptions = {}
): DamageFormula {
  return {
    baseDamage,
    multiplier: options.multiplier || 1,
    bonusDamage: options.bonusDamage || 0,
    get formula() {
      let damageFormula = this.baseDamage
      if (this.multiplier > 1) {
        damageFormula = `${this.multiplier}*(${damageFormula})`
      }
      if (this.bonusDamage !== 0) {
        damageFormula = `${damageFormula} + ${this.bonusDamage}`
      }
      return damageFormula
    },
  }
}

export interface CombatComputationData extends BaseComputationOptionData {
  talent?: CombatTalent
  weapon?: Weapon
  shield?: Shield
  modifiers?: ModifierDescriptor[]
}

export interface CombatComputationResult {
  value: number
  mod?: number
}

export interface CombatDamageComputationData extends CombatComputationData {
  bonusDamage?: number
  damageMultiplier?: number
}

export enum RangeClass {
  VeryNear,
  Near,
  Medium,
  Far,
  VeryFar,
}

export enum SizeClass {
  Tiny,
  VerySmall,
  Small,
  Medium,
  Big,
  VeryBig,
}

export interface RangedCombatComputationData extends CombatComputationData {
  rangeClass?: RangeClass
  sizeClass?: SizeClass
  mod?: number
}

export const ComputeAttack = CreateComputationIdentifier<
  CombatComputationData,
  CombatComputationResult
>('attack')

export const ComputeParry = CreateComputationIdentifier<
  CombatComputationData,
  CombatComputationResult
>('parry')

export const ComputeRangedAttack = CreateComputationIdentifier<
  RangedCombatComputationData,
  CombatComputationResult
>('rangedAttack')

export const ComputeDamageFormula = CreateComputationIdentifier<
  CombatDamageComputationData,
  DamageFormula
>('damageFormula')

export const ComputeDodge = CreateComputationIdentifier<
  CombatComputationData,
  CombatComputationResult
>('dodge')

export const ComputeArmorClass = CreateComputationIdentifier<
  BaseComputationOptionData,
  number
>('armorClass')

export const ComputeEncumbarance = CreateComputationIdentifier<
  BaseComputationOptionData,
  number
>('encumbarance')

interface EffectiveEncumbaranceOptionData extends BaseComputationOptionData {
  formula: string
}

export const ComputeEffectiveEncumbarance = CreateComputationIdentifier<
  EffectiveEncumbaranceOptionData,
  number
>('effectiveEncumbarance')

export const DerivedCombatAttributesRule = DescribeRule(
  'derived-combat-attributes-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .on(ComputeAttack)
      .do((options: CombatComputationData): CombatComputationResult => {
        const value = options.talent
          ? options.character.baseAttack + options.talent.attackMod
          : 0
        return {
          value,
          mod: 0,
        }
      })

    ruleset
      .on(ComputeParry)
      .do((options: CombatComputationData): CombatComputationResult => {
        const value = options.talent
          ? options.character.baseParry + options.talent.parryMod
          : 0
        return {
          value,
          mod: 0,
        }
      })

    ruleset
      .on(ComputeRangedAttack)
      .do((options: CombatComputationData): CombatComputationResult => {
        const value = options.talent
          ? options.character.baseRangedAttack + options.talent.rangedAttackMod
          : 0
        return {
          value,
          mod: 0,
        }
      })

    ruleset.before(ComputeAttack).do(ExtractTalentFromWeapon)
    ruleset.before(ComputeParry).do(ExtractTalentFromWeapon)
    ruleset.before(ComputeRangedAttack).do(ExtractTalentFromWeapon)

    ruleset.on(ComputeDodge).do(
      (options: CombatComputationData): CombatComputationResult => ({
        value: options.character.dodgeValue({}),
        mod: 0,
      })
    )

    ruleset
      .on(ComputeDamageFormula)
      .do((options: CombatDamageComputationData): DamageFormula => {
        return createDamageFormula(options.weapon?.damage || '0', {
          bonusDamage: options.bonusDamage,
          multiplier: options.damageMultiplier,
        })
      })

    ruleset
      .after(ComputeDamageFormula)
      .when(IsUnarmed)
      .do((options, result) => {
        result.baseDamage = '1d6'
        return result
      })

    ruleset
      .on(ComputeEncumbarance)
      .do((options: BaseComputationOptionData): number => {
        return options.character.armorItems
          ? options.character.armorItems.reduce(
              (prev, current) => current.encumbarance + prev,
              0
            )
          : 0
      })

    ruleset
      .on(ComputeEffectiveEncumbarance)
      .do((options: EffectiveEncumbaranceOptionData): number => {
        if (options.formula === undefined || options.formula === null) {
          return 0
        }
        let encumbarance = options.character.encumbarance
        const formula = options.formula?.replace('–', '-')
        const operators = ['+', '-', 'x']
        const formulaParts =
          formula?.split(
            new RegExp('([' + operators.map((op) => '\\' + op).join('') + '])+')
          ) || []
        if (formulaParts?.length === 3) {
          switch (formulaParts[1]) {
            case '+':
              encumbarance = encumbarance + parseInt(formulaParts[2])
              break
            case '-':
              encumbarance = Math.max(
                encumbarance - parseInt(formulaParts[2]),
                0
              )
              break
            case 'x':
              encumbarance = encumbarance * parseInt(formulaParts[2])
              break
          }
        }
        return encumbarance
      })

    ruleset
      .on(ComputeArmorClass)
      .do((options: BaseComputationOptionData): number => {
        return options.character.armorItems
          ? options.character.armorItems.reduce(
              (prev, current) => current.armorClass + prev,
              0
            )
          : 0
      })
  }
)
