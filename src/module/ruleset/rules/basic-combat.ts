import type { Character } from '../../model/character'
import type { MeleeWeapon, Shield, Weapon } from '../../model/items'
import type { ModifierDescriptor } from '../../model/modifier'
import type { CombatTalent } from '../../model/properties'
import { DescribeRule } from '../rule'
import {
  Action,
  CreateActionIdentifier,
  CreateComputationIdentifier,
  BaseActionOptionData,
} from '../rule-components'
import type { Ruleset } from '../ruleset'
import {
  AttributeActionResult,
  AttributeRollActionData,
  RollAttributeToChatEffect,
  RollCombatAttribute,
} from './basic-roll-mechanic'
import {
  CombatComputationData,
  ComputeDamageFormula,
} from './derived-combat-attributes'

export function usesMeleeWeapon<
  OptionData extends { weapon?: Weapon },
  ResultType
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
>(options: OptionData, _result?: ResultType): boolean {
  return options.weapon?.type === 'melee'
}

export interface CombatActionData extends BaseActionOptionData {
  character: Character
  talent?: CombatTalent
  weapon?: Weapon
  shield?: Shield
  mod?: number
  modifiers?: ModifierDescriptor[]
}

export interface CombatActionResult<
  OptionData extends CombatActionData = CombatActionData
> extends AttributeActionResult<OptionData & AttributeRollActionData> {
  success: boolean
  penality?: number
}

interface AttackActionResult extends CombatActionResult {
  bonusDamage?: number
  damageMultiplier?: number
}

export const AttackAction = CreateActionIdentifier<
  CombatActionData,
  AttackActionResult
>('attack')
export const ParryAction = CreateActionIdentifier<
  CombatActionData,
  CombatActionResult
>('parry')
export const DodgeAction = CreateActionIdentifier<
  CombatActionData,
  CombatActionResult
>('dodge')

function IsSuccess(_options, result): boolean {
  return result?.success || false
}

export const ComputeInitiative = CreateComputationIdentifier<
  CombatComputationData,
  number
>('initiative')

export interface ComputeInitiativeFormulaData extends CombatComputationData {
  dices?: number
}

export interface InitiativeFormula {
  formula: string
  initiative: (modifer: number) => number
  rollFormula: string
}

export const ComputeInitiativeFormula = CreateComputationIdentifier<
  ComputeInitiativeFormulaData,
  InitiativeFormula
>('initiativeFormula')

export function IsAttackAction(result: CombatActionResult): boolean {
  return result.options.action?.name === AttackAction.name
}

export class CombatAction<
  ActionOptionData extends CombatActionData = CombatActionData
> extends Action<ActionOptionData, CombatActionResult<ActionOptionData>> {
  async _execute<OptionData extends ActionOptionData>(
    options: OptionData
  ): Promise<CombatActionResult<ActionOptionData>> {
    const valueKey = `${this.identifier.name}Value`
    const targetValue = options.character[valueKey]({
      weapon: options.weapon,
      talent: options.talent,
    })
    const attributeName = options.action?.name || ''
    const rollResult = await this.ruleset.execute(RollCombatAttribute, {
      ...options,
      targetValue,
      mod: options.mod || 0,
      attributeName,
    })
    return {
      ...rollResult,
      options: {
        ...rollResult.options,
        ...options,
      },
    }
  }
}

export const BasicCombatRule = DescribeRule(
  'basic-combat-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .on(ComputeInitiative)
      .do((options) => options.character.baseInitiative)

    ruleset
      .after(ComputeInitiative)
      .when(usesMeleeWeapon)
      .do(
        (options, result) =>
          result +
          ((<MeleeWeapon>options.weapon)?.initiativeMod
            ? (<MeleeWeapon>options.weapon).initiativeMod
            : 0)
      )

    ruleset
      .after(ComputeInitiative)
      .do(
        (options, result) =>
          result + (options.shield ? options.shield.initiativeMod : 0)
      )

    ruleset
      .after(ComputeInitiative)
      .do((options, result) => result - (options.character.encumbarance || 0))

    ruleset
      .on(ComputeInitiativeFormula)
      .do((options: ComputeInitiativeFormulaData) => {
        const initiative =
          ruleset.compute(ComputeInitiative, options) -
          options.character.baseInitiative
        const dices = options.dices || 1
        return {
          character: options.character,
          get formula() {
            return (
              `${dices}d6 + ${initiative + this.character.baseInitiative}.` +
              `${this.character.baseInitiative}`.padStart(2, '0')
            )
          },
          initiative(modifier: number) {
            return (
              modifier +
              initiative +
              this.character.baseInitiative +
              this.character.baseInitiative / 100
            )
          },
          rollFormula: `${dices}d6`, //TODO Test
        }
      })

    ruleset.on(AttackAction).do(CombatAction)
    ruleset.after(AttackAction).trigger(RollAttributeToChatEffect)
    ruleset.on(ParryAction).do(CombatAction)
    ruleset.after(ParryAction).trigger(RollAttributeToChatEffect)
    ruleset.on(DodgeAction).do(CombatAction)
    ruleset.after(DodgeAction).trigger(RollAttributeToChatEffect)

    ruleset
      .after(AttackAction)
      .when(IsSuccess)
      .do((_options, result) => ({
        ...result,
        damage: ruleset.compute(ComputeDamageFormula, {
          character: result.options.character,
          weapon: result.options.weapon,
          talent: result.options.talent,
          modifiers: result.options.modifiers,
          bonusDamage: result.bonusDamage,
          damageMultiplier: result.damageMultiplier,
        }),
      }))
  }
)
