import type { Ruleset } from './ruleset'

type RuleLoader = (ruleset: Ruleset) => void

export interface RuleConfigManager {
  register(module: string, key: string, data: any): void
  get(module: string, key: string): any
}

interface RuleConfigData {
  enabled?: boolean
  changeable?: boolean
}

export class Rule {
  public isEnabled = false
  private isChangeable = true
  private loader: RuleLoader
  private ruleId: string
  private systemId = 'dsa-4.1'

  constructor(ruleId: string, config?: RuleConfigData) {
    this.ruleId = ruleId
    this.isEnabled =
      config?.enabled !== undefined ? config.enabled : this.isEnabled
    this.isChangeable =
      config?.changeable !== undefined ? config.changeable : this.isChangeable
  }

  describe(loader: RuleLoader): void {
    this.loader = loader
  }

  loadInto(ruleset: Ruleset): void {
    if (this.isEnabled) {
      this.loader(ruleset)
    }
  }

  createConfig(configManager: RuleConfigManager): void {
    configManager.register(this.systemId, this.ruleId, {
      name: `${this.systemId}.settings.${this.ruleId}`,
      hint: `${this.systemId}.settings.${this.ruleId}Hint`,
      scope: 'world',
      config: this.isChangeable,
      type: Boolean,
      default: this.isEnabled,
    })
  }

  loadConfig(configManager: RuleConfigManager): void {
    const isEnabled = configManager.get(this.systemId, this.ruleId)
    this.isEnabled = isEnabled
  }
}

export interface RuleDescriptor {
  ruleId: string
  config: RuleConfigData
  loader: RuleLoader
}

export interface RuleCreator {
  (descriptor: RuleDescriptor): Rule
}

export function CreateRule(descriptor: RuleDescriptor): Rule {
  const rule = new Rule(descriptor.ruleId, descriptor.config)
  rule.describe(descriptor.loader)
  return rule
}

export function DescribeRule(
  ruleId: string,
  config: RuleConfigData,
  loader: RuleLoader
): RuleDescriptor {
  return {
    ruleId,
    config,
    loader,
  }
}
