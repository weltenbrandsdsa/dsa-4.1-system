import type { Ruleset } from './ruleset/ruleset'

declare global {
  class DSAGame extends Game {
    ruleset: Ruleset
  }
}

export function getGame(): DSAGame {
  if (!(game instanceof Game)) {
    throw new Error('game is not initialized yet!')
  }
  return game as DSAGame
}

export function getCanvas(): Canvas {
  if (!(canvas instanceof Canvas)) {
    throw new Error('canvas is not initialized yet!')
  }
  return canvas
}
