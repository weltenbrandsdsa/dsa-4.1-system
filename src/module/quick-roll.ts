import { SkillRollEventListener } from './actor/actor-sheet'
import { getCanvas, getGame } from './utils'

Hooks.on('renderSidebarTab', (_0, html) => {
  const chat = html[0]?.querySelector('#chat-message')
  if (!chat) return

  chat.addEventListener('drop', (event) => handleChatItemDrop(chat, event))
})

async function handleChatItemDrop(chat, event) {
  let data
  try {
    data = JSON.parse(event.dataTransfer.getData('text/plain'))
    if (data.type !== 'Item') return
  } catch (err) {
    return false
  }
  const game = getGame()
  const pack = await game.packs.get(data.pack)
  const item = await pack?.get(data.id)?.data
  if (item?.type === 'talent') {
    ChatMessage.create({
      content: `<button class="talent-from-chat" data-sid="${item.data.sid}">${item.name}</button>`,
    })
  }
}

function onCustomButtonClick(event) {
  const button = event.currentTarget
  const sid = $(button).data('sid')

  rollSkillBySid(sid)
}

Hooks.on('renderChatMessage', (app, html) => {
  html.find('button.talent-from-chat').click(onCustomButtonClick)
})

function rollSkillBySid(sid) {
  const game = getGame()
  const actor = game.user?.character as Actor
  if (actor === undefined) {
    ui.notifications?.warn('No Actor selected!')
  }

  const item = actor?.items?.filter((i) => i.data.data.sid === sid)[0]
  if (item) {
    // actor.rollSkill(item.id)
    const eventHandler = new SkillRollEventListener(actor)
    const event = {
      currentTarget: {
        name: item.id,
      },
    }
    eventHandler.handleEvent(event)
  } else {
    ui.notifications?.info('Actor does not own this skill!')
  }
}

function matchBoundKeyEvent(event) {
  const keySetting = 'Shift +  '
  const canvas = getCanvas()

  const key = (<any>window).Azzu.SettingsTypes.KeyBinding.parse(keySetting)
  if (key.key === ' ' && (canvas.controls?.ruler?.waypoints?.length || 0) > 0) {
    return false
  }

  document.activeElement?.closest('.app.window-app')

  return (
    (<any>window).Azzu.SettingsTypes.KeyBinding.eventIsForBinding(event, key) &&
    !$(document.activeElement as any)
      .closest('.app.window-app')
      .is('#client-settings')
  )
}

declare class QuickInsert {
  static open(options: any)
}

declare class KeybindLib {
  static register(module, setting, options)
}

Hooks.once('init', function () {
  const game = getGame()
  if ('QuickInsert' in window && QuickInsert) {
    game.modules.set('dsa-4.1', {} as any)
    // eslint-disable-next-line no-undef
    KeybindLib.register('dsa-4.1', 'QuickRoll', {
      name: 'Open Quick Insert',
      hint: 'Opens Quick Insert anywhere in Foundry',
      default: 'Shift + Space',
      config: true,
      onKeyDown: callQuickInsert,
    })
  }
})

function callQuickInsert() {
  const game = getGame()

  QuickInsert.open({
    filter: 'actor.items',
    onSubmit: async (searchItem) => {
      const pack = await game.packs.get(searchItem.package)
      const item = await pack?.getDocument(searchItem.id)

      if (item) {
        rollSkillBySid(item.data.data.sid)
      }
    },
  })
}
