export const Attribute = Object.freeze({
  Courage: 'courage',
  Cleverness: 'cleverness',
  Intuition: 'intuition',
  Charisma: 'charisma',
  Dexterity: 'dexterity',
  Agility: 'agility',
  Constitution: 'constitution',
  Strength: 'strength',
})

export const CombatAttribute = Object.freeze({
  BaseAttack: 'baseAttack',
  BaseParry: 'baseParry',
  BaseRangedAttack: 'baseRangedAttack',
  BaseInitiative: 'baseInitiative',
})

export const SpellModification = Object.freeze({
  spellCastTime: 'spellCastTime',
  ForceEffect: 'forceEffect',
  SaveCosts: 'saveCosts',
  MultipleTargets: 'multipleTargets',
  Range: 'range',
})

export const SpellTargetClass = Object.freeze({
  Object: 'object',
  MultipleObjects: 'multipleObjects',
  Person: 'person',
  MultiplePersons: 'multiplePersons',
  Zone: 'zone',
  Creature: 'creature',
  MultipleCreatures: 'multipleCreatures',
})

export const LiturgyType = Object.freeze({
  Basic: 'basic',
  Special: 'special',
})

export const LiturgyTargetClass = Object.freeze({
  G: 'G',
  P: 'P',
  PP: 'PP',
  PPP: 'PPP',
  PPPP: 'PPPP',
  Z: 'Z',
  ZZ: 'ZZ',
  ZZZ: 'ZZZ',
  ZZZZ: 'ZZZZ',
})

export const LiturgyCastType = Object.freeze({
  BumpPrayer: 'bumpPrayer',
  Prayer: 'prayer',
  Devotion: 'devotion',
  Ceremony: 'ceremony',
  Cycle: 'cycle',
})

export const TalentType = Object.freeze({
  Basic: 'basic',
  Special: 'special',
})

export const TalentCategory = Object.freeze({
  Combat: 'combat',
  Physical: 'physical',
  Social: 'social',
  Nature: 'nature',
  Knowledge: 'knowledge',
  Language: 'language',
  Crafting: 'crafting',
  Karma: 'karma',
  Gift: 'gift',
})

export const CombatTalentCategory = Object.freeze({
  Melee: 'melee',
  Ranged: 'ranged',
  Special: 'special',
})

export const EffectiveEncumbaranceType = Object.freeze({
  None: 'none',
  Special: 'special',
  Formula: 'formula',
})

export const RuleBook = Object.freeze({
  LiberCantionesDeluxe: 'lcd',
  LiberLiturgium: 'll',
})

export const SkillType = Object.freeze({
  Talent: 'talent',
  Spell: 'spell',
  Karma: 'karma',
  Ritual: 'ritual',
})
