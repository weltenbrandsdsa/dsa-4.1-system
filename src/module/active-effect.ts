import { Attribute, CombatAttribute } from './enums'
import { labeledEnumValues } from './i18n'

interface DsaActiveEffectConfigData extends ActiveEffectConfig.Data {
  changeableAttributes: Record<string, string>
}

// eslint-disable-next-line no-undef
export class DsaActiveEffectConfig extends ActiveEffectConfig<
  DocumentSheet.Options,
  DsaActiveEffectConfigData
> {
  get template(): string {
    return `systems/dsa-4.1/templates/active-effect.html`
  }

  async getData(
    options: Application.RenderOptions | undefined
  ): Promise<DsaActiveEffectConfigData> {
    const data = await super.getData(options)

    const attributes = labeledEnumValues(Attribute, false)
    const combatAttributes = labeledEnumValues(CombatAttribute, false)

    data.changeableAttributes = attributes.reduce((prev, labeledValue) => {
      const key = `data.base.basicAttributes.${labeledValue.value}.value`
      return { ...prev, [key]: labeledValue.label }
    }, {})

    data.changeableAttributes = combatAttributes.reduce(
      (prev, labeledValue) => {
        const key = `data.base.combatAttributes.active.${labeledValue.value}.value`
        return { ...prev, [key]: labeledValue.label }
      },
      data.changeableAttributes
    )

    return data
  }
}
