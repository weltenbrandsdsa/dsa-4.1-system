import App from '../ui/skill-roll/SkillRoll.svelte'
import type { Character } from './model/character'
import type { ModifierDescriptor } from './model/modifier'
import { Skill, TestAttributes } from './model/properties'
import { getGame } from './utils'

export type SkillDialogCallback = (
  modifiers: ModifierDescriptor[],
  totalModifier: number,
  selectedAttributes: TestAttributes
) => void

export class SkillRollDialog extends Application {
  private callback: SkillDialogCallback
  private skill: Skill
  private character: Character

  constructor(
    options: Partial<Application.Options> | undefined,
    callback: SkillDialogCallback,
    skill: Skill,
    character: Character
  ) {
    super(options)
    this.callback = callback
    this.skill = skill
    this.character = character
  }

  component: App

  async _renderInner(_data: any): Promise<JQuery<HTMLElement>> {
    const inner = $('<div class="svelte-app"></div>')
    this.component = new App({
      target: $(inner).get(0),
      props: {
        foundryApp: this,
        skill: this.skill,
        character: this.character,
        callback: this.callback,
        localize: (key: string) => getGame().i18n.localize('DSA.' + key),
      },
    })
    return inner
  }

  static create(skill: Skill, character: Character): Promise<any> {
    return new Promise((resolve) =>
      new SkillRollDialog(
        {
          title: getGame().i18n.localize('DSA.skillRollDialog'),
        },
        (
          modifiers: ModifierDescriptor[],
          totalModifier: number,
          selectedAttributes: TestAttributes
        ) => resolve([modifiers, totalModifier, selectedAttributes]),
        skill,
        character
      ).render(true)
    )
  }
}
