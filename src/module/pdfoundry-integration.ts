import { RuleBook } from './enums'

const BookCodes = {
  [RuleBook.LiberCantionesDeluxe]: 'LCD',
  [RuleBook.LiberLiturgium]: 'LL',
}

const PageAttributes = {
  [RuleBook.LiberCantionesDeluxe]: 'lcdPage',
  [RuleBook.LiberLiturgium]: 'llPage',
}

function openPDFPage(book, page) {
  if ((<any>ui).PDFoundry) {
    return (<any>ui).PDFoundry.openPDFByCode(BookCodes[book], { page })
  } else {
    ui.notifications?.warn('PDFoundry must be installed to use source links.')
  }
}

export function createOpenPDFPageListener(book) {
  return (event) => {
    const page = parseInt($(event.currentTarget).data(PageAttributes[book]))
    openPDFPage(book, page)
  }
}
