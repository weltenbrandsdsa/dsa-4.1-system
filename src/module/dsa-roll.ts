import { getGame } from './utils'

interface DsaRollOptions {
  successChecker?: (roll?: any) => boolean
  criticalChecker?: (
    roll?: any,
    confirmCritical?: any,
    successChecker?: any
  ) => boolean
  name: string
  confirmCritical?: boolean
}

interface DsaRollData {
  is3d20Roll: boolean
  mod: number
}

export class DsaRoll {
  parts: any
  options: any
  data: any
  createMessage: any
  __roll: any

  constructor(
    parts,
    options: DsaRollOptions = {
      successChecker: () => false,
      criticalChecker: () => false,
      name: '',
      confirmCritical: false,
    },
    data: DsaRollData = {
      is3d20Roll: false,
      mod: 0,
    },
    createMessage = true
  ) {
    this.parts = parts
    this.options = options
    this.data = data
    this.createMessage = createMessage
  }

  _roll(form: any = undefined) {
    if (form !== undefined) this.data['bonus'] = form?.bonus.value

    if (!this.data['bonus'] || this.data['bonus'] == 0) {
      this.parts = this.parts.filter((part) => part != '@bonus')
    }

    if (!this.data['mod'] || this.data['mod'] == 0) {
      this.parts = this.parts.filter((part) => part != '@mod')
    }

    const rollClass = CONFIG.Dice.rolls[0]
    this.__roll = new rollClass(this.parts.join(' + '), this.data).roll({
      async: false,
    })

    if (this.createMessage) {
      this.toMessage()
    }

    return this
  }

  toMessage() {
    if (this.options.maxTotal && this.__roll.total > this.options.maxTotal) {
      this.__roll._total = this.options.maxTotal
    }

    if (this.options.criticalChecker) {
      this.__roll.critical = this.options.criticalChecker(
        this.__roll,
        this.options.confirmCritical,
        this.options.successChecker
      )
    }

    if (this.options.successChecker) {
      this.__roll.success = this.options.successChecker(this.__roll)
    }

    this._toMessage(this.__roll)
  }

  _toMessage(roll) {
    let result = ''
    if (roll.success != undefined) {
      result = roll.success ? 'success' : 'failed'
    }

    let critical = ''
    if (roll.success != undefined) {
      critical = roll.critical
    }

    let flavor = this.options.name + ' '
    if (this.data.skillValue) {
      flavor += `(${this.data.skillValue}) `
    }
    if (this.data.bonus && this.data.bonus != 0) {
      flavor += this.data.bonus > 0 ? '+ ' : '- '
      flavor += `${Math.abs(this.data.bonus)} `
    }

    const game = getGame()

    if (critical) {
      const criticalText = game.i18n.localize('DSA.critical')
      flavor += criticalText + ' '
    }

    if (result) {
      flavor += game.i18n.localize('DSA.' + result)
    }

    const rollMode = game.settings.get('core', 'rollMode')

    roll.toMessage(
      {
        speaker: ChatMessage.getSpeaker(),
        flavor,
      },
      { rollMode }
    )
  }

  async _renderDialog() {
    // Render modal dialog
    const template = 'systems/dsa-4.1/templates/roll-dialog.html'
    const dialogData = {
      formula: this.parts.join(' + '),
    }

    return renderTemplate(template, dialogData)
  }

  async _showDialog() {
    const html = await this._renderDialog()
    let roll

    return new Promise((resolve) => {
      new Dialog({
        title: this.options.name,
        content: html,
        buttons: {
          normal: {
            label: 'Roll',
            callback: (html) =>
              (roll = this._roll(html[0].querySelector('form'))),
          },
        },
        default: 'normal',
        close: () => resolve(roll ? roll : false),
      }).render(true)
    })
  }

  roll(isFastForward = false) {
    if (isFastForward) {
      return this._roll()
    }

    return this._showDialog()
  }

  static executeRoll(parts, data, rollOptions, isFastForward = false) {
    return new DsaRoll(parts, rollOptions, data, false).roll(isFastForward)
  }
}

export function critical1d20(roll, confirmCritical, successChecker) {
  const dieResult = roll.dice[0].results[0]
  let isCritical = dieResult === 1 || dieResult === 20

  if (isCritical && confirmCritical) {
    const reroll = roll.reroll()
    isCritical = successChecker(roll) == successChecker(reroll)
    roll.dice[1] = reroll.dice[0]
    roll.dice[0].results[1] = reroll.dice[0].results[0]
    roll.dice[0].results[0].exploded = true
  }

  return isCritical
}

export function critical3d20(roll) {
  let ones = 0
  let twenties = 0

  for (const d of roll.dice) {
    if (d.results[0] == 1) {
      ones++
    }

    if (d.results[0] == 20) {
      twenties++
    }
  }

  return ones >= 2 || twenties >= 2
}
