import { getGame } from './utils'

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace ClientSettings {
    interface Values {
      'dsa-4.1.combat-talent-pack': string
    }
  }
}

export const registerSettings = function (): void {
  const game = getGame()

  const packChoices = {}
  game.packs.forEach((p) => {
    packChoices[p.collection] = `${p.metadata.label} (${p.metadata.package})`
  })

  game.settings.register('dsa-4.1', 'combat-talent-pack', {
    name: `dsa-4.1.settings.combatTalentPack`,
    hint: `dsa-4.1.settings.combatTalentPackHint`,
    scope: 'world',
    config: true,
    type: String,
    choices: packChoices,
    default: 'dsa-4.1.combattalent',
  })
}
