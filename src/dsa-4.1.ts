import './styles/dsa-4.1.css'

// Import Modules
import { DsaFourOneActor } from './module/actor/actor'
import { DsaFourOneActorSheet } from './module/actor/actor-sheet'
import { DsaSvelteActorSheet } from './module/actor/actor-sheet-svelte'
import { preloadHandlebarsTemplates } from './module/preload-templates'
import './module/item/item'
import { DsaItemSheet } from './module/item/item-sheet'
import { SpellSheet } from './module/item/spell-sheet'
import { LiturgySheet } from './module/item/liturgy-sheet'
import {
  TalentSheet,
  LinguisticTalentSheet,
  CombatTalentSheet,
} from './module/item/talent-sheet'
import './module/item/talent'
import { DsaCombatant } from './module/combat'
import { DsaActiveEffectConfig } from './module/active-effect'

import './module/bar-brawl-integration'
import './module/quick-roll'
import './module/nicer-dsa-roll'

import { Ruleset } from './module/ruleset/ruleset'
import { BasicCombatRule } from './module/ruleset/rules/basic-combat'
import { BasicRangedCombatRule } from './module/ruleset/rules/basic-ranged-combat'
import { DerivedCombatAttributesRule } from './module/ruleset/rules/derived-combat-attributes'
import { BasicShieldCombatRule } from './module/ruleset/rules/basic-shield-combat'
import { WeaponModifierRule } from './module/ruleset/rules/weapon-modifier'
import { WeaponStrengthModifierRule } from './module/ruleset/rules/weapon-strength-modifier'
import { BasicRollMechanicRule } from './module/ruleset/rules/basic-roll-mechanic'
import { BasicManeuverRule } from './module/ruleset/rules/maneuvers/basic-maneuver'
import { ManeuverRules } from './module/ruleset/rules/maneuvers/maneuvers'
import { SpecialAbilityRules } from './module/ruleset/rules/special-abilities/special-abilities'

import { DSADie } from './module/dsa-die'

import { BasicSkillRule } from './module/ruleset/rules/basic-skill'
import { FulminictusRule } from './module/ruleset/rules/spells/fulminictus'
import { getGame } from './module/utils'
import { migrateSystem } from './migrations'
import { registerSettings } from './module/settings'

Hooks.once('init', async function () {
  console.log('Initialisiere DSA 4.1 System')

  CONFIG.Actor.documentClass = DsaFourOneActor
  CONFIG.Combatant.documentClass = DsaCombatant

  CONFIG.ActiveEffect.sheetClass = DsaActiveEffectConfig

  Actors.unregisterSheet('core', ActorSheet)
  Actors.registerSheet('dsa-4.1', DsaFourOneActorSheet, { makeDefault: true })
  Actors.registerSheet('dsa-4.1', DsaSvelteActorSheet)

  Items.registerSheet('dsa-4.1', DsaItemSheet, {
    types: [
      'meleeWeapon',
      'rangedWeapon',
      'armor',
      'shield',
      'advantage',
      'disadvantage',
      'specialAbility',
      'skill',
      'spellVariant',
      'liturgyVariant',
      'genericItem',
    ],
    makeDefault: true,
  })
  Items.registerSheet('dsa-4.1', SpellSheet, {
    types: ['spell'],
    makeDefault: true,
  })
  Items.registerSheet('dsa-4.1', LiturgySheet, {
    types: ['liturgy'],
    makeDefault: true,
  })
  Items.registerSheet('dsa-4.1', TalentSheet, {
    types: ['talent'],
    makeDefault: true,
  })
  Items.registerSheet('dsa-4.1', CombatTalentSheet, {
    types: ['combatTalent'],
    makeDefault: true,
  })
  Items.registerSheet('dsa-4.1', LinguisticTalentSheet, {
    types: ['language', 'scripture'],
    makeDefault: true,
  })

  Handlebars.registerHelper('concat', function (...args) {
    let outStr = ''

    for (const arg of args) {
      if (typeof arg !== 'object') {
        outStr += arg
      }
    }

    return outStr
  })

  Handlebars.registerHelper('toLowerCase', function (str) {
    return str.toLowerCase()
  })

  await preloadHandlebarsTemplates()
})

Hooks.once('init', async function () {
  const game = getGame()
  game.ruleset = new Ruleset(game.settings)
  // game.ruleset.add(BasicRollMechanicRule)
  BasicRollMechanicRule(game.ruleset)
  game.ruleset.add(BasicSkillRule)
  game.ruleset.add(BasicCombatRule)
  game.ruleset.add(DerivedCombatAttributesRule)
  game.ruleset.add(BasicRangedCombatRule)
  game.ruleset.add(BasicShieldCombatRule)
})

Hooks.once('setup', async function () {
  const game = getGame()
  game.ruleset.add(WeaponModifierRule)
  game.ruleset.add(WeaponStrengthModifierRule)

  game.ruleset.add(BasicManeuverRule)
  ManeuverRules.forEach((maneuverRule) => game.ruleset.add(maneuverRule))

  game.ruleset.add(FulminictusRule)

  SpecialAbilityRules.forEach((specialAbility) =>
    game.ruleset.add(specialAbility)
  )
})

Hooks.once('ready', async function () {
  await migrateSystem()

  const game = getGame()
  game.ruleset.compileRules()

  registerSettings()
})

Hooks.once('init', async function () {
  CONFIG.Dice.terms['z'] = DSADie
})
