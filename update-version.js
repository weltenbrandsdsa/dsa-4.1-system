// const yargs = require('yargs')
const fs = require('fs')

const manifestFile = 'src/system.json'
const gitlabPath = 'foundry-vtt-dsa/dsa-4.1-core/dsa-4.1-system'

let releaseType = 'patch'

if (
  process.argv[2] == 'patch' ||
  process.argv[2] == 'minor' ||
  process.argv[2] == 'major'
) {
  releaseType = process.argv[2]
}

const manifestRaw = fs.readFileSync(manifestFile)
let manifest = JSON.parse(manifestRaw)

let [major, minor, patch] = manifest.version.split('.')

const incrementVersion = (version) => (parseInt(version) + 1).toString()

switch (releaseType) {
  case 'patch':
    patch = incrementVersion(patch)
    break
  case 'minor':
    minor = incrementVersion(minor)
    break
  case 'major':
    major = incrementVersion(major)
    break
}

const version = `${major}.${minor}.${patch}`

console.log(version)

manifest.version = version
manifest.download = `https://gitlab.com/${gitlabPath}/-/jobs/artifacts/v${version}/raw/${manifest.name}.zip?job=deploy`

fs.writeFileSync(manifestFile, JSON.stringify(manifest, null, 2))
