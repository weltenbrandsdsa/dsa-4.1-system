---

# DSA 4.1 System

Das Schwarze Auge 4.1 (The Dark Eye) for Foundry Virtual Tabletop System

**PLEASE NOTE** This system is currently in an alpha stadium. It is possible, that future versions are incompatible. This means, that there is a possibility, that characters and items must be newly created after an update.

**BITTE BEACHTEN** Dieses System befindet sich noch in einem Alpha Stadium. Daher kann es gut, dass zukünfitge Versionen nicht mehr kompatibel zu dieser Version sind. Dies bedeutet, dass möglicherweise Charaktere und Gegenstände neu angelegt werden müssen!

## How to install

In the FoundryVTT Game Systems Menu, click Install System and enter the following Manifest URL:

`https://gitlab.com/foundry-vtt-dsa/dsa-4.1-core/dsa-4.1-system/-/raw/master/src/system.json`

## [How To Play (in German)](https://gitlab.com/foundry-vtt-dsa/dsa-4.1-core/dsa-4.1-system/-/wikis/How-To-Play)

## Development Status

The current state is descriped in the how to play tutorial. The plans for the upcoming versions are:

- More and better integration of rules
- Support for active effects
- Support for rituals
- More compendias
- Complete UI overhaul of actor sheet
- Initiative Tracker with multiple action support

## How to get in Contact

[![Join our Discord server!](https://invidget.switchblade.xyz/emtzDGumax)](https://discord.gg/emtzDGumax)
